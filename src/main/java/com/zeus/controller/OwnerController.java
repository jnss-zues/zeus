package com.zeus.controller;

import com.alibaba.fastjson.JSON;
import com.zeus.common.Properties;
import com.zeus.common.StringUtil;
import com.zeus.common.enums.ErrorEnum;
import com.zeus.common.utils.ExcelUtil;
import com.zeus.common.utils.ExcelUtils;
import com.zeus.common.utils.HttpClientManager;
import com.zeus.common.utils.RedisUtil;
import com.zeus.domain.SmsKeyEnum;
import com.zeus.domain.dto.WxInfoDTO;
import com.zeus.domain.entity.HouseVote;
import com.zeus.domain.entity.Owner;
import com.zeus.domain.entity.WxInfo;
import com.zeus.domain.param.FeedbackHandleParams;
import com.zeus.domain.param.OwnerInfoEditParams;
import com.zeus.domain.param.OwnerInfoQueryParams;
import com.zeus.domain.param.VoteRecordQueryParams;
import com.zeus.domain.vo.BaseVo;
import com.zeus.service.HouseVoteService;
import com.zeus.service.OwnerService;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.zeus.service.WxInfoService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.Mapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author yishan@dian.so
 * @version 1.0.0
 * @data 2019/3/3
 */
@RestController
@Slf4j
public class OwnerController {

    @Resource
    private OwnerService ownerService;

    @Resource
    private WxInfoService wxInfoService;

    @Resource
    private Properties properties;

    @Resource
    private RedisUtil redisUtil;

    @Resource
    private HouseVoteService houseVoteService;

    /**
     * 业主信息列表
     * @param
     * @return
     */
    @GetMapping("/owner/info/list")
    public BaseVo listOwnerInfo(OwnerInfoQueryParams ownerInfoQuery){
        return BaseVo.create(ownerService.listOwnerInfo(ownerInfoQuery));
    }

    /**
     * 业主详情
     * @param
     * @return
     */
    @GetMapping("/owner/info/get")
    public BaseVo getOwnerInfo(@RequestParam("houseId") String houseId){
        return BaseVo.create(ownerService.queryById(houseId).convert());
    }

    /**
     * 业主信息编辑
     * @param
     * @return
     */
    @PostMapping("/owner/info/edit")
    public BaseVo editOwnerInfo(@RequestBody OwnerInfoEditParams ownerInfoEditParams){
        return BaseVo.create(ownerService.editOwnerInfo(ownerInfoEditParams));
    }

    /**
     * 业主信息导入
     * @param
     * @return
     */
    @RequestMapping("/owner/info/import")
    public BaseVo importOwnerInfo(@RequestParam("communityId")Integer communityId,@RequestParam("fileName")String fileName,@RequestParam("file") MultipartFile file){
        return BaseVo.create(ownerService.importOwnerInfo(communityId,fileName,file));
    }

    /**
     * 业主导入错误信息下载
     * @param
     * @return
     */
    @RequestMapping("/owner/info/error/import")
    public void importErrorInfo(@RequestParam("fileName")String fileName,HttpServletRequest request,HttpServletResponse response){
        ExcelUtils.getExcel("/Users/yishan/temp",fileName,response,request);

    }

    /**
     * 小区区域列表
     * @param
     * @return
     */
    @GetMapping("/owner/district/{communityId}")
    public BaseVo listDistrict(@PathVariable Integer communityId){
        return BaseVo.create(ownerService.listDistrict(communityId));
    }

    /**
     * 小区幢列表
     * @param
     * @return
     */
    @GetMapping("/owner/building/{communityId}")
    public BaseVo listBuilding(@PathVariable Integer communityId){
        return BaseVo.create(ownerService.listBuilding(communityId));
    }

    /**
     * 小区单元列表
     * @param
     * @return
     */
    @GetMapping("/owner/unit/{communityId}")
    public BaseVo listUnit(@PathVariable Integer communityId){
        return BaseVo.create(ownerService.listUnit(communityId));
    }

    /**
     * 小区反馈列表
     * @param
     * @return
     */
    @GetMapping("/owner/feedback/list")
    public BaseVo listFeedback(OwnerInfoQueryParams ownerInfoQuery){
        return BaseVo.create(ownerService.listFeedback(ownerInfoQuery));
    }

    /**
     * 小区反馈处理
     * @param
     * @return
     */
    @GetMapping("/owner/feedback/handle")
    public BaseVo handleOwnerFeedback(FeedbackHandleParams feedbackHandleParams){
        return BaseVo.create(ownerService.handleOwnerFeedback(feedbackHandleParams));
    }

    /**
     * 新增反馈
     * @param
     * @return
     */
    @PostMapping("/owner/feedback/add")
    public BaseVo addOwnerFeedback(@RequestBody FeedbackHandleParams feedbackHandleParams){
        return BaseVo.create(ownerService.addOwnerFeedback(feedbackHandleParams));
    }

    /**
     * 确权
     * @param mobile
     * @param name
     * @param openId
     * @param validCode
     * @return
     * @throws Exception
     */
    @PostMapping("/owner/right")
    public BaseVo right(@RequestParam String mobile,@RequestParam String name,@RequestParam String openId
            ,@RequestParam String validCode,@RequestParam Integer voteId) throws Exception {

        if(StringUtils.isBlank(openId) || StringUtils.isBlank(mobile)
                || StringUtils.isBlank(name) || StringUtils.isBlank(validCode)
                || voteId == null){
            return BaseVo.errorCreate(ErrorEnum.PARAMS_NULL);
        }
        Integer checkCode = (Integer) redisUtil.get(mobile+SmsKeyEnum.RIGHT.getDesc());
        log.info("确权checkCode:"+checkCode);
        log.info("确权validCode:"+validCode);
        if(!StringUtil.equals(validCode,String.valueOf(properties.commonCode))){
            if(checkCode == null || !StringUtil.equals(validCode,String.valueOf(checkCode))){
                return BaseVo.errorCreate(ErrorEnum.VALID_CODE_INVALID);
            }
        }
        VoteRecordQueryParams voteRecordQueryParams = new VoteRecordQueryParams();
        voteRecordQueryParams.setMobiles(mobile);
        voteRecordQueryParams.setName(name);
        voteRecordQueryParams.setVoteId(voteId);
        List<HouseVote> houseVoteList = houseVoteService.ListAllByParam(voteRecordQueryParams);
        if(CollectionUtils.isEmpty(houseVoteList)){
            return BaseVo.errorCreate(ErrorEnum.NONE_RIGHT);
        }

        String token = wxInfoService.getAccessToken();
        log.info("确权openid:"+openId);
        log.info("确权token:"+token);
        WxInfoDTO wxInfoDTO = wxInfoService.getWxInfo(openId,token);
        String openIdResult = dealWxInfo(mobile,name,wxInfoDTO);
        redisUtil.del(mobile +SmsKeyEnum.RIGHT.getDesc());
        return BaseVo.create(openIdResult);
    }


    /**
     * 检查是否需要确权
     * @param code
     * @return
     * @throws Exception
     */
    @GetMapping("/checkOpenId")
    public BaseVo checkOpenId(String code,String openId) throws Exception {
        Map<String,Object> result = new HashMap<>();
        log.info("检查code:" + code);
        log.info("检查openId:" + openId);
        if(StringUtil.isBlank(openId)){
            if(StringUtils.isBlank(code) ){
                return BaseVo.errorCreate(ErrorEnum.PARAMS_NULL);
            }
            Map<String,Object> openIdInfo = wxInfoService.getOpenId(code);
            if(openIdInfo == null || openIdInfo.get("openid") == null){
                return BaseVo.errorCreate(ErrorEnum.NONE_OPENID);
            }
            openId = openIdInfo.get("openid").toString();
        }
        WxInfo wxInfo = wxInfoService.getByOpenid(openId);
        log.info(JSON.toJSONString(wxInfo));
        result.put("openId",openId);
        if(wxInfo == null || StringUtil.isBlank(wxInfo.getMobile())){
            result.put("needRight",true);
        }else{
            result.put("needRight",false);
        }
        log.info(JSON.toJSONString(result));
        return BaseVo.create(result);
    }



    public String dealWxInfo(String mobile,String name,WxInfoDTO wxInfoDTO){
        log.info("mobile:"+mobile);
        log.info("wxInfoDTO:"+JSON.toJSONString(wxInfoDTO));
        if(wxInfoDTO == null || StringUtil.isBlank(wxInfoDTO.getOpenId())){
            return null;
        }
        WxInfo wxInfo = wxInfoService.getByMobile(mobile);
        log.info("wxInfo:"+JSON.toJSONString(wxInfo));
        WxInfo newWxInfo = wxInfoDTO.toDO();
        log.info("newWxInfo:"+JSON.toJSONString(newWxInfo));
        if(wxInfo != null){
            newWxInfo.setId(wxInfo.getId());
            wxInfoService.updateWxInfo(newWxInfo);
        }else{
            newWxInfo.setMobile(mobile);
            newWxInfo.setName(name);
            wxInfoService.saveWxInfo(newWxInfo);
        }
        return wxInfoDTO.getOpenId();
    }

    @GetMapping("/owner/info")
    public BaseVo getOwnerInfo(@RequestParam  String openId,@RequestParam Integer voteId){
        if(StringUtil.isBlank(openId) || voteId == null){
            return BaseVo.errorCreate(ErrorEnum.PARAMS_NULL);
        }
        WxInfo wxInfo = wxInfoService.getByOpenid(openId);
        String mobile = wxInfo.getMobile();
        VoteRecordQueryParams voteRecordQueryParams = new VoteRecordQueryParams();
        voteRecordQueryParams.setVoteId(voteId);
        voteRecordQueryParams.setMobiles(mobile);
        List<HouseVote> houseVotes = houseVoteService.ListAllByParam(voteRecordQueryParams);
        return BaseVo.create(houseVotes);
    }

}
