package com.zeus.controller;

import com.zeus.domain.param.CommunityParams;
import com.zeus.domain.vo.BaseVo;
import com.zeus.service.CommunityService;
import javax.annotation.Resource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.Mapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author yishan@dian.so
 * @version 1.0.0
 * @data 2019/3/2
 */
@RestController
public class CommunityController {

    @Resource
    private CommunityService communityService;

    /**
     * 小区编辑
     * @param communityParams
     * @return
     */
    @PostMapping("/community/modify")
    public BaseVo modifyCommunity(@RequestBody CommunityParams communityParams){
        return BaseVo.create(communityService.modify(communityParams));
    }

    /**
     * 小区详情
     * @param communityId
     * @return
     */
    @GetMapping("/community/detail/{communityId}")
    public BaseVo detail(@PathVariable Integer communityId){
        return BaseVo.create(communityService.detail(communityId));
    }

    @GetMapping("/communities")
    public BaseVo ListAll(){
        return BaseVo.create(communityService.listAll());
    }
}
