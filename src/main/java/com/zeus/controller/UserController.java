package com.zeus.controller;

import com.zeus.domain.param.UserParams;
import com.zeus.domain.param.UserQueryParams;
import com.zeus.domain.vo.BaseVo;
import com.zeus.service.UserService;
import javax.annotation.Resource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author yishan@dian.so
 * @version 1.0.0
 * @data 2019/3/2
 */
@RestController
public class UserController {

    @Resource
    private UserService userService;

    /**
     * 小区管理人员列表
     * @param userQueryParams
     * @return
     */
    @GetMapping("/user/list")
    public BaseVo listUser(UserQueryParams userQueryParams){
        return BaseVo.create(userService.listUser(userQueryParams));
    }

    /**
     * 添加小区管理人员
     * @param userParams
     * @return
     */
    @PostMapping("/user/add")
    public BaseVo addUser(@RequestBody UserParams userParams){
        return BaseVo.create(userService.add(userParams));
    }

    /**
     * 移除小区管理人员
     * @param userId
     * @return
     */
    @GetMapping("/user/move/{userId}")
    public BaseVo moveUser(@PathVariable Integer userId){
        return BaseVo.create(userService.delete(userId));
    }
}
