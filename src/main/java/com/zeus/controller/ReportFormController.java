package com.zeus.controller;

import com.zeus.common.utils.ExcelUtil;
import com.zeus.common.utils.ExcelUtils;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author yishan@dian.so
 * @version 1.0.0
 * @data 2019/3/18
 */
@Controller
@RequestMapping(value = "/report")
@Slf4j
public class ReportFormController {

    //发送响应流方法
    public void setResponseHeader(HttpServletResponse response, String fileName) {
        try {
            try {
                fileName = new String(fileName.getBytes(), "ISO8859-1");
            } catch (UnsupportedEncodingException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            response.setContentType("application/octet-stream;charset=ISO8859-1");
            response.setHeader("Content-Disposition", "attachment;filename=" + fileName);
            response.addHeader("Pargam", "no-cache");
            response.addHeader("Cache-Control", "no-cache");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * @Title: getExcelTemplate
     * @Description: 生成Excel模板并导出
     * @param @param uuid
     * @param @param request
     * @param @param response
     * @param @return
     * @return Data
     * @throws
     */
    @RequestMapping("/export")
    public void getExcelTemplate(HttpServletRequest request, HttpServletResponse response){

        String fileName = "ownerInfo.xls"; //模板名称
        String[] handers = {"序号","区","幢","单元","室","面积","房屋类型","业主姓名（多个用;隔开）","业主电话（多个用;隔开）"}; //列标题

        //下拉框数据
        List<String[]> downData = new ArrayList();
        String[] str1 = {"住宅","非住宅"};
        downData.add(str1);
        String [] downRows = {"6"}; //下拉的列序号数组(序号从0开始)

        //创建HSSFWorkbook
        HSSFWorkbook wb = ExcelUtils.createExcelTemplate(handers,downData, downRows );

        //响应到客户端
        try {
            this.setResponseHeader(response, fileName);
            OutputStream os = response.getOutputStream();
            wb.write(os);
            os.flush();
            os.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
