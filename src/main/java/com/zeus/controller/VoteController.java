package com.zeus.controller;

import com.github.pagehelper.PageInfo;
import com.zeus.common.StringUtil;
import com.zeus.common.enums.ErrorEnum;
import com.zeus.common.utils.HtmlToPdfUtil;
import com.zeus.common.utils.WordUtil;
import com.zeus.dao.HouseVoteIssueDao;
import com.zeus.dao.VoteIssueDao;
import com.zeus.domain.HouseTypeEnum;
import com.zeus.domain.VoteRecordStatusEnum;
import com.zeus.domain.VoteResultEnum;
import com.zeus.domain.VoteStatusEnum;
import com.zeus.domain.dto.HouseVoteDTO;
import com.zeus.domain.dto.VoteDetailDTO;
import com.zeus.domain.dto.VoteResultDTO;
import com.zeus.domain.entity.*;
import com.zeus.domain.param.VoteParams;
import com.zeus.domain.param.VoteRecordParams;
import com.zeus.domain.param.VoteRecordQueryParams;
import com.zeus.domain.vo.BaseVo;
import com.zeus.domain.vo.VoteInfoDTO;
import com.zeus.service.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.SimpleDateFormat;
import java.util.*;

@RestController
public class VoteController extends BaseController {

    @Resource
    private VoteService voteService;

    @Resource
    private WxInfoService wxInfoService;

    @Resource
    private HouseVoteService houseVoteService;

    @Resource
    private HouseService houseService;

    @Resource
    private VoteIssueDao voteIssueDao;

    @Resource
    private HouseVoteIssueDao houseVoteIssueDao;

    /**
     * 投票列表
     * @param voteParams
     * @return
     */
    @GetMapping("/votes")
    public BaseVo listVote(VoteParams voteParams,HttpServletRequest request){
        Integer communityId = this.getCommunityId(request);
        if(communityId == null){
            return BaseVo.errorCreate(ErrorEnum.UN_LOGIN);
        }
        voteParams.setCommunityId(communityId);
        return BaseVo.create("查询投票信息成功",voteService.listVote(voteParams));
    }

    /**
     * 投票列表
     * @param voteParams
     * @param request
     * @return
     */
    @GetMapping("/votesWx")
    public BaseVo listVoteWx(VoteParams voteParams,HttpServletRequest request){
        voteParams.setStatus(VoteStatusEnum.UNDERWAY.getCode());
        return BaseVo.create("查询投票信息成功",voteService.listVote(voteParams));
    }

    /**
     * 新建投票
     * @return
     */
    @PostMapping("/vote")
    public BaseVo createVote(@RequestBody VoteParams voteParams, HttpServletRequest request){
        Integer userId = this.getUserId(request);
        voteParams.setOperator(userId);
        Integer communityId = this.getCommunityId(request);
        voteParams.setCommunityId(communityId);
        voteParams.setStatus(VoteStatusEnum.NOT_START.getCode());
        VoteParams voteParamsQuery = new VoteParams();
        voteParamsQuery.setStatus(VoteStatusEnum.UNDERWAY.getCode());
        voteParamsQuery.setCommunityId(communityId);
        List<VoteInfoDTO> voteList = voteService.listVote(voteParamsQuery).getList();
        if(!CollectionUtils.isEmpty(voteList)){
            return BaseVo.errorCreate(ErrorEnum.HAS_VOTE_RUNNING);
        }
        voteService.create(voteParams);
        return BaseVo.create(null);
    }

    /**
     * 编辑投票
     * @return
     */
    @PatchMapping("/vote")
    public BaseVo updateVote(HttpServletRequest request ,@RequestBody VoteParams voteParams){
        Integer userId = this.getUserId(request);
        voteParams.setOperator(userId);
        Vote vote = voteService.getVoteDetail(voteParams.getId());
        if(vote == null){
            return BaseVo.errorCreate(ErrorEnum.NONE_VOTE);
        }
        if(VoteStatusEnum.TERMINATION.getCode().equals(vote.getStatus())){
            return BaseVo.errorCreate(ErrorEnum.CAN_NOT_UPDATE);
        }
        if(VoteStatusEnum.UNDERWAY.getCode().equals(vote.getStatus())
                && voteParams.getStartTime() != null
                && !vote.getStartTime().equals(voteParams.getStartTime())){
            return BaseVo.errorCreate(ErrorEnum.CAN_NOT_UPDATE_START_TIME);
        }
        voteService.update(voteParams);
        return BaseVo.create(null);
    }

    /**
     * 投票详情
     * @param id
     * @return
     */
    @GetMapping("/vote/{id}")
    public BaseVo getVoteInfo(@PathVariable Integer id,String openId){
        if(id == null){
            return BaseVo.errorCreate(ErrorEnum.PARAMS_NULL);
        }
        VoteDetailDTO voteDetailDTO = voteService.getVoteDetail(id);
        if(StringUtil.isNotBlank(openId) ){
            WxInfo wxInfo = wxInfoService.getByOpenid(openId);
            if(wxInfo == null){
                return BaseVo.errorCreate(ErrorEnum.NONE_PERMISSION);
            }
            String mobile = wxInfo.getMobile();
            List<HouseVote> houseVotes = houseVoteService.ListAllMobile(mobile,id);
            if(!CollectionUtils.isEmpty(houseVotes)){
                voteDetailDTO.setCanVote(false);
                //最后随机投票结果
                Integer result = null;
                List<VoteResultDTO> resultList = new ArrayList<>();
                //自己的投票结果
                Integer resultImp = null;
                for(HouseVote houseVote : houseVotes){
                    if(!VoteRecordStatusEnum.HAS_VOTE.getCode().equals(houseVote.getStatus())){
                        voteDetailDTO.setCanVote(true);
                        continue;
                    }
                    if(mobile.equals(houseVote.getMobiles())){
                        List<HouseVoteIssue> voteIssues = houseVoteService.listIssues(houseVote.getId());
                        if(!CollectionUtils.isEmpty(voteIssues)){
                            for(HouseVoteIssue houseVoteIssue : voteIssues){
                                VoteResultDTO voteResultDTO = new VoteResultDTO();
                                BeanUtils.copyProperties(houseVoteIssue,voteResultDTO);
                                voteResultDTO.setResultStr(VoteResultEnum.getDesc(voteResultDTO.getResult()));
                                resultList.add(voteResultDTO);

                            }
                        }
                    }
                }
                voteDetailDTO.setResults(resultList);
            }
        }
        return BaseVo.create(voteDetailDTO);
    }

    /**
     * 获取投票详情
     * @param id
     * @return
     */
    @GetMapping("/voteDetail/{id}")
    public BaseVo getVoteInfo(@PathVariable Integer id){
        if(id == null){
            return BaseVo.errorCreate(ErrorEnum.PARAMS_NULL);
        }
        HouseVote houseVote = houseVoteService.getById(id);
        if(houseVote == null) {
            return BaseVo.errorCreate(ErrorEnum.NONE_VOTE);
        }
        VoteDetailDTO voteDetailDTO = voteService.getVoteDetail(houseVote.getVoteId());
        List<HouseVoteIssue> voteIssues = houseVoteService.listIssues(houseVote.getId());
        List<VoteResultDTO> resultList = new ArrayList<>();
        if(!CollectionUtils.isEmpty(voteIssues)){
            for(HouseVoteIssue houseVoteIssue : voteIssues){
                VoteResultDTO voteResultDTO = new VoteResultDTO();
                BeanUtils.copyProperties(houseVoteIssue,voteResultDTO);
                voteResultDTO.setResultStr(VoteResultEnum.getDesc(voteResultDTO.getResult()));
                resultList.add(voteResultDTO);
            }
        }
        voteDetailDTO.setResults(resultList);
        return BaseVo.create(voteDetailDTO);
    }

    /**
     * 获取投票基本信息
     * @param voteId
     * @return
     */
    @GetMapping("/getById")
    public BaseVo getById(@RequestParam Integer voteId){
        return BaseVo.create(voteService.getVoteDetail(voteId));
    }

    /**
     * 投票
     * @param voteRecordParams
     * @return
     */
    @PostMapping("/voteRecord")
    public BaseVo vote(@RequestBody  VoteRecordParams voteRecordParams){
        String openId = voteRecordParams.getOpenId();
        Integer voteId = voteRecordParams.getVoteId();
        List<HouseVoteIssue> results = voteRecordParams.getResults();
        VoteDetailDTO voteDetailDTO = voteService.getVoteDetail(voteId);
        //投票状态校验
        if(VoteStatusEnum.NOT_START.getCode().equals(voteDetailDTO.getStatus())){
            return BaseVo.errorCreate(ErrorEnum.VOTE_NOT_START);
        }
        if(!VoteStatusEnum.UNDERWAY.getCode().equals(voteDetailDTO.getStatus())){
            return BaseVo.errorCreate(ErrorEnum.VOTE_FINISHED);
        }
        if(StringUtils.isBlank(openId) || voteId == null || CollectionUtils.isEmpty(results)){
            return BaseVo.errorCreate(ErrorEnum.PARAMS_NULL);
        }

        //用户信息校验
        WxInfo wxInfo = wxInfoService.getByOpenid(openId);
        if(wxInfo == null){
            return BaseVo.errorCreate(ErrorEnum.NONE_PERMISSION);
        }

        return vote(voteId,wxInfo.getMobile(),wxInfo.getName(),voteDetailDTO.getVoteUnits(),results,"WL");
    }


    /**
     * 手动投票
     * @param houseVoteParam
     * @return
     */
    @PostMapping("/voteRecord/add")
    public BaseVo voteAdd(@RequestBody HouseVoteDTO houseVoteParam){
        String name = houseVoteParam.getNames();
        String mobile = houseVoteParam.getMobiles();
        Integer voteId = houseVoteParam.getVoteId();
        List<HouseVoteIssue> results = houseVoteParam.getResults();
        VoteDetailDTO voteDetailDTO = voteService.getVoteDetail(voteId);
        if(voteId == null || StringUtil.isBlank(mobile) ||
                StringUtil.isBlank(name) || CollectionUtils.isEmpty(results)){
            return BaseVo.errorCreate(ErrorEnum.PARAMS_NULL);
        }
        //投票状态校验
        if(VoteStatusEnum.NOT_START.getCode().equals(voteDetailDTO.getStatus())){
            return BaseVo.errorCreate(ErrorEnum.VOTE_NOT_START);
        }
        if(!VoteStatusEnum.UNDERWAY.getCode().equals(voteDetailDTO.getStatus())){
            return BaseVo.errorCreate(ErrorEnum.VOTE_FINISHED);
        }
        return vote(voteId,mobile,name,voteDetailDTO.getVoteUnits(),results,"DH");

    }

    private BaseVo vote(Integer voteId,String mobile,String name,String district,List<HouseVoteIssue> results,String type){
        VoteRecordQueryParams voteRecordQueryParams = new VoteRecordQueryParams();
        voteRecordQueryParams.setVoteId(voteId);
        voteRecordQueryParams.setMobiles(mobile);
        voteRecordQueryParams.setName(name);
        //参与投票单位校验
        if(!StringUtil.equals(district,"0") ){
            voteRecordQueryParams.setDistrict(district);
        }
        //用户信息校验
        List<HouseVote> houseVotes = houseVoteService.ListAllByParam(voteRecordQueryParams);
        if(CollectionUtils.isEmpty(houseVotes)){
            return BaseVo.errorCreate(ErrorEnum.NONE_PERMISSION);
        }

        boolean flag = false;
        List<HouseVote> canVoteList = new ArrayList<>();
        for(HouseVote houseVote : houseVotes){
            if(!VoteRecordStatusEnum.HAS_VOTE.getCode().equals(houseVote.getStatus())){
                flag = true;
                canVoteList.add(houseVote);
            }
        }
        if(!flag){
            return BaseVo.errorCreate(ErrorEnum.HAS_RECORD);
        }
        List<VoteIssue> voteIssues = voteService.listIssues(voteId);
        if(CollectionUtils.isEmpty(voteIssues)
                || voteIssues.size() != results.size()){
            return BaseVo.errorCreate(ErrorEnum.NOT_FINISH_VOTE);
        }

        //进行投票
        for(HouseVote houseVote : canVoteList){
            houseVote.setVoteMobile(mobile);
            houseVote.setStatus(VoteRecordStatusEnum.HAS_VOTE.getCode());
            houseVote.setVoteNo(type + houseVote.getVoteNo());
            houseVote.setUpdateTime(new Date());
            houseVoteService.vote(houseVote,results);

        }
        return BaseVo.create(null);
    }



    /**
     * 投票记录
     * @param voteRecordQueryParams
     * @return
     */
    @GetMapping("/voteRecords")
    public BaseVo ListVotRecord(VoteRecordQueryParams voteRecordQueryParams,HttpServletRequest request){
        if(voteRecordQueryParams.getVoteId() == null){
            return BaseVo.errorCreate(ErrorEnum.PARAMS_NULL);
        }
        return BaseVo.create(voteService.listVoteRecord(voteRecordQueryParams));
    }

    /**
     * 投票记录导出
     * @param voteRecordQueryParams
     * @param response
     * @return
     */
    @GetMapping("/voteRecords/export")
    public void voteRecordExport(VoteRecordQueryParams voteRecordQueryParams, HttpServletResponse response){
        if(voteRecordQueryParams.getVoteId() == null){
            return;
        }
        voteService.voteRecordExport(voteRecordQueryParams,response);

    }

    //获取投票分享二维码
    @GetMapping("/vote/qrcode")
    public void getVoteQrCode(VoteRecordQueryParams voteRecordQueryParams, HttpServletResponse response){
        if(voteRecordQueryParams.getVoteId() == null){
            BaseVo.errorCreate(ErrorEnum.PARAMS_NULL);
            return;
        }
        voteService.getVoteQrCodeImg(voteRecordQueryParams.getVoteId(),response);
    }

    /**
     * 投票统计
     * @param voteRecordQueryParams
     * @param response
     */
    @GetMapping("/vote/count")
    public BaseVo countVote(VoteRecordQueryParams voteRecordQueryParams, HttpServletResponse response){
        return BaseVo.create(voteService.countVote(voteRecordQueryParams.getCommunityId(),voteRecordQueryParams.getVoteId()));
    }

    /**
     * 投票记录
     * @param voteRecordQueryParams
     * @param response
     * @return
     */
    @RequestMapping("/vote/records")
    public BaseVo recordList(VoteRecordQueryParams voteRecordQueryParams, HttpServletResponse response){
        return BaseVo.create(houseVoteService.ListByParam(voteRecordQueryParams));
//        return BaseVo.create(voteService.listVoteRecord(voteRecordQueryParams));
    }

    /**
     * 投票确认接收
     * @param openId
     * @param voteId
     * @return
     */
    @RequestMapping("/vote/affirm")
    public BaseVo affirm(@RequestParam String openId,@RequestParam Integer voteId){
        if(StringUtil.isBlank(openId) || voteId == null){
            return BaseVo.errorCreate(ErrorEnum.PARAMS_NULL);
        }
        VoteDetailDTO voteDetailDTO = voteService.getVoteDetail(voteId);
        if(voteDetailDTO == null){
            return BaseVo.errorCreate(ErrorEnum.NONE_VOTE);
        }
        if(VoteStatusEnum.NOT_START.getCode().equals(voteDetailDTO.getStatus())){
            return BaseVo.errorCreate(ErrorEnum.VOTE_NOT_START);
        }
        if(VoteStatusEnum.TERMINATION.getCode().equals(voteDetailDTO.getStatus())){
            return BaseVo.errorCreate(ErrorEnum.VOTE_FINISHED);
        }
        WxInfo wxInfo = wxInfoService.getByOpenid(openId);
        String mobile = wxInfo.getMobile();
        houseVoteService.affirm(mobile,voteId);
        return  BaseVo.create(null);
    }

    /**
     * 修改投票记录信息
     * @param houseVote
     * @return
     */
    @PatchMapping("/vote/record")
    public BaseVo updateRecord(@RequestBody HouseVote houseVote ){
        if(houseVote.getId() == null){
            return BaseVo.errorCreate(ErrorEnum.PARAMS_NULL);
        }
        houseVoteService.update(houseVote);
        return BaseVo.create(null);

    }

    /**
     * 获取投票记录详情
     * @param id
     * @return
     */
    @GetMapping("/vote/record/{id}")
    public BaseVo getInfo(@PathVariable  Integer id){
        if(id == null){
            return BaseVo.create(ErrorEnum.PARAMS_NULL);
        }
        return BaseVo.create(houseVoteService.getById(id));
    }

    @RequestMapping("/vote/export")
    public BaseVo voteExport(HttpServletResponse response,Integer id){
        HouseVote houseVote = houseVoteService.getById(id);
        List<VoteIssue> voteIssues = voteService.listIssues(houseVote.getVoteId());
        List<HouseVoteIssue> issueLIst =  houseVoteService.listIssues(houseVote.getId());
        SimpleDateFormat ff = new SimpleDateFormat("yyyy年MM月dd日");
        if(houseVote == null){
            return BaseVo.create(ErrorEnum.NONE_VOTE);
        }
        Map map=new HashMap();
        map.put("name",houseVote.getNames());
        map.put("mobile",houseVote.getMobiles());
        map.put("house",houseVote.getDistrict() + houseVote.getBuildingNum() + "幢" + houseVote.getUnit() +"单元" + houseVote.getRoomNum());
        map.put("houseType",HouseTypeEnum.getDesc(houseVote.getHouseType()));
        map.put("area",String.valueOf(houseVote.getArea()));
        map.put("num",houseVote.getVoteNo());
        if(!VoteRecordStatusEnum.HAS_VOTE.getCode().equals(houseVote.getStatus())){
            map.put("voteTime","");
            map.put("result","");
        }else{
            map.put("voteTime",ff.format(houseVote.getUpdateTime()));
            map.put("result",VoteResultEnum.getDesc(houseVote.getResult()));
        }
        if(!CollectionUtils.isEmpty(issueLIst)){
            for(int i = 0;i< issueLIst.size();i++){
                int index = i + 1;
                HouseVoteIssue houseVoteIssue = issueLIst.get(i);
                map.put("result_"+index+"_1","  ");
                map.put("result_"+index+"_2","  ");
                map.put("result_"+index+"_3","  ");
                map.put("result_"+index+"_"+houseVoteIssue.getResult()," √️");
            }
        }else if(!CollectionUtils.isEmpty(voteIssues)){
            for(int i = 0;i< voteIssues.size();i++){
                int index = i + 1;
                map.put("result_"+index+"_1","  ");
                map.put("result_"+index+"_2","  ");
                map.put("result_"+index+"_3","  ");
            }
        }

        WordUtil.getBuild("static/doc/新明半岛表决票样.doc",map,"业主大会会议表决票-"+ houseVote.getNames(),response);
        return BaseVo.create(true);
    }

    /**
     * 批量导出投票结果
     * @param response
     * @param params
     * @return
     * @throws Exception
     */
    @RequestMapping("/votes/export")
    public BaseVo votesExport(HttpServletResponse response,VoteRecordQueryParams params) throws Exception {
        if(params.getVoteId() == null){
            return BaseVo.errorCreate(ErrorEnum.PARAMS_NULL);
        }
        PageInfo result = houseVoteService.ListByParam(params);
        List<HouseVote> houseVotes = result.getList();
        SimpleDateFormat ff = new SimpleDateFormat("yyyy年MM月dd日");
        if(CollectionUtils.isEmpty(houseVotes)){
            return BaseVo.create(ErrorEnum.NONE_VOTE);
        }
        List<HouseVoteIssue> houseVoteIssues = houseVoteIssueDao.listByVote(params.getVoteId());
        Map<Integer,Map<Integer,Integer>> houseVoteIssuesMap = new HashMap<>();
        for(HouseVoteIssue houseVoteIssue : houseVoteIssues){
            Map<Integer,Integer> houseVoteIssuesTemp = houseVoteIssuesMap.get(houseVoteIssue.getHouseVoteId());
            if(CollectionUtils.isEmpty(houseVoteIssuesTemp)){
                houseVoteIssuesTemp = new HashMap<>();
            }
            houseVoteIssuesTemp.put(houseVoteIssue.getVoteIssueId(),houseVoteIssue.getResult());
            houseVoteIssuesMap.put(houseVoteIssue.getHouseVoteId(),houseVoteIssuesTemp);
        }

        List<VoteIssue> voteIssues = voteIssueDao.listByVote(params.getVoteId());
        List<Map<String,String>> mapList = new ArrayList<>();
        for(HouseVote houseVote : houseVotes){
            Map map=new HashMap();
            map.put("${name}",houseVote.getNames());
            map.put("${mobile}",houseVote.getMobiles());
            map.put("${house}",houseVote.getDistrict() + houseVote.getBuildingNum() + "幢" + houseVote.getUnit() +"单元" + houseVote.getRoomNum());
            map.put("${area}",String.valueOf(houseVote.getArea()));
            map.put("${num}",houseVote.getVoteNo());
            map.put("${houseType}",HouseTypeEnum.getDesc(houseVote.getHouseType()));
            if(!VoteRecordStatusEnum.HAS_VOTE.getCode().equals(houseVote.getStatus())){
                map.put("${voteTime}","");
                map.put("${result}","");
            }else{
                map.put("${voteTime}",ff.format(houseVote.getUpdateTime()));
                map.put("${result}",VoteResultEnum.getDesc(houseVote.getResult()));
            }

            if(!CollectionUtils.isEmpty(voteIssues)){
                Map<Integer,Integer> resultMap = houseVoteIssuesMap.get(houseVote.getId());
                for(int i = 0;i< voteIssues.size();i++){
                    int index = i + 1;
                    VoteIssue voteIssue = voteIssues.get(i);
                    map.put("${result_"+index+"_1}","  ");
                    map.put("${result_"+index+"_2}","  ");
                    map.put("${result_"+index+"_3}","  ");
                    if(resultMap == null){
                        continue;
                    }
                    map.put("${result_"+index+"_"+resultMap.get(voteIssue.getId())+"}"," √️");
                }
            }
            mapList.add(map);
        }
        String endNum = params.getPageNum()*params.getPageSize() + "";
        if(params.getPageNum() >= result.getPages()){
            endNum = result.getTotal() + "";
        }
//        String fileUrl = "/Users/songzuowei/Desktop/temp.docx";
        String fileUrl = "/usr/local/static/doc/temp.docx";
        WordUtil.getBuilds(fileUrl,mapList,"业主大会会议表决票"+
                (params.getPageNum()-1)*params.getPageSize() + "-" + endNum,response);
        return BaseVo.create(true);
    }

    @GetMapping("/vote/record/result/{recordId}")
    public BaseVo listIssues(HttpServletResponse response,@PathVariable Integer recordId){
        if(recordId == null){
            return BaseVo.errorCreate(ErrorEnum.PARAMS_NULL);
        }
        return BaseVo.create(houseVoteService.listIssues(recordId));
    }

    /**
     * 添加投票业主
     * @param houseNo
     * @return
     */
    @PostMapping("/vote/addVoteHouse")
    public BaseVo addVoteHouse(Integer houseNo,HttpServletRequest request,Integer voteId){
        if(houseNo == null || voteId == null){
            return BaseVo.errorCreate(ErrorEnum.PARAMS_NULL);
        }
        Integer communityId = this.getCommunityId(request);
        House house = houseService.getByNo(houseNo,communityId);
        if(house == null){
            return BaseVo.errorCreate(ErrorEnum.NONE_OWNER);
        }
        VoteDetailDTO voteDetailDTO = voteService.getVoteDetail(voteId);
        if(voteDetailDTO == null){
            return BaseVo.errorCreate(ErrorEnum.NONE_VOTE);
        }
        if(houseVoteService.getByNo(house.getNo(),voteDetailDTO.getId()) != null){
            return BaseVo.errorCreate(ErrorEnum.HAS_HOUSE_VOTE);
        }

        //校验该业主是否有投票权限
        if(!"0".equals(voteDetailDTO.getVoteUnits())
                && !StringUtil.equals(house.getDistrict(),voteDetailDTO.getVoteUnits())){
            return BaseVo.errorCreate(ErrorEnum.NONE_PERMISSION);
        }

        houseVoteService.insert(house,voteDetailDTO.getId());
        return BaseVo.create(null);
    }

    /**
     * 导出pdf
     * @param response
     * @param id
     * @return
     */
    @RequestMapping("/vote/pdf/export")
    public BaseVo votePdfExport(HttpServletResponse response,Integer id){
        HouseVote houseVote = houseVoteService.getById(id);
        List<VoteIssue> voteIssues = voteService.listIssues(houseVote.getVoteId());
        List<HouseVoteIssue> issueLIst =  houseVoteService.listIssues(houseVote.getId());
        SimpleDateFormat ff = new SimpleDateFormat("yyyy年MM月dd日");
        if(houseVote == null){
            return BaseVo.create(ErrorEnum.NONE_VOTE);
        }
        Map map=new HashMap();
        map.put("name",houseVote.getNames());
        map.put("mobile",houseVote.getMobiles());
        map.put("house",houseVote.getDistrict() + houseVote.getBuildingNum() + "幢" + houseVote.getUnit() +"单元" + houseVote.getRoomNum());
        map.put("houseType",HouseTypeEnum.getDesc(houseVote.getHouseType()));
        map.put("area",String.valueOf(houseVote.getArea()));
        map.put("num",houseVote.getVoteNo());
        if(!VoteRecordStatusEnum.HAS_VOTE.getCode().equals(houseVote.getStatus())){
            map.put("voteTime","");
            map.put("result","");
        }else{
            map.put("voteTime",ff.format(houseVote.getUpdateTime()));
            map.put("result",VoteResultEnum.getDesc(houseVote.getResult()));
        }
        if(!CollectionUtils.isEmpty(issueLIst)){
            for(int i = 0;i< issueLIst.size();i++){
                int index = i + 1;
                HouseVoteIssue houseVoteIssue = issueLIst.get(i);
                map.put("result_"+index+"_1","&nbsp;&nbsp;");
                map.put("result_"+index+"_2","&nbsp;&nbsp;");
                map.put("result_"+index+"_3","&nbsp;&nbsp;");
                map.put("result_"+index+"_"+houseVoteIssue.getResult()," √️");
            }
        }else if(!CollectionUtils.isEmpty(voteIssues)){
            for(int i = 0;i< voteIssues.size();i++){
                int index = i + 1;
                map.put("result_"+index+"_1","&nbsp;&nbsp;");
                map.put("result_"+index+"_2","&nbsp;&nbsp;");
                map.put("result_"+index+"_3","&nbsp;&nbsp;");
            }
        }

        HtmlToPdfUtil.html2Pdf("static/pdf-xmbd.html","业主大会会议表决票-"+ houseVote.getNames(),map,response);
        return BaseVo.create(true);
    }


    /**
     * 批量导出投票结果PDF
     * @param response
     * @param params
     * @return
     * @throws Exception
     */
    @RequestMapping("/votes/pdf/export")
    public BaseVo votesPdfExport(HttpServletResponse response,VoteRecordQueryParams params) throws Exception {
        if(params.getVoteId() == null){
            return BaseVo.errorCreate(ErrorEnum.PARAMS_NULL);
        }
        PageInfo result = houseVoteService.ListByParam(params);
        List<HouseVote> houseVotes = result.getList();
        SimpleDateFormat ff = new SimpleDateFormat("yyyy年MM月dd日");
        if(CollectionUtils.isEmpty(houseVotes)){
            return BaseVo.create(ErrorEnum.NONE_VOTE);
        }
        List<HouseVoteIssue> houseVoteIssues = houseVoteIssueDao.listByVote(params.getVoteId());
        Map<Integer,Map<Integer,Integer>> houseVoteIssuesMap = new HashMap<>();
        for(HouseVoteIssue houseVoteIssue : houseVoteIssues){
            Map<Integer,Integer> houseVoteIssuesTemp = houseVoteIssuesMap.get(houseVoteIssue.getHouseVoteId());
            if(CollectionUtils.isEmpty(houseVoteIssuesTemp)){
                houseVoteIssuesTemp = new HashMap<>();
            }
            houseVoteIssuesTemp.put(houseVoteIssue.getVoteIssueId(),houseVoteIssue.getResult());
            houseVoteIssuesMap.put(houseVoteIssue.getHouseVoteId(),houseVoteIssuesTemp);
        }

        List<VoteIssue> voteIssues = voteIssueDao.listByVote(params.getVoteId());
        List<Map<String,String>> mapList = new ArrayList<>();
        for(HouseVote houseVote : houseVotes){
            Map map=new HashMap();
            map.put("${name}",houseVote.getNames());
            map.put("${mobile}",houseVote.getMobiles());
            map.put("${house}",houseVote.getDistrict() + houseVote.getBuildingNum() + "幢" + houseVote.getUnit() +"单元" + houseVote.getRoomNum());
            map.put("${area}",String.valueOf(houseVote.getArea()));
            map.put("${num}",houseVote.getVoteNo());
            map.put("${houseType}",HouseTypeEnum.getDesc(houseVote.getHouseType()));
            if(!VoteRecordStatusEnum.HAS_VOTE.getCode().equals(houseVote.getStatus())){
                map.put("${voteTime}","");
                map.put("${result}","");
            }else{
                map.put("${voteTime}",ff.format(houseVote.getUpdateTime()));
                map.put("${result}",VoteResultEnum.getDesc(houseVote.getResult()));
            }

            if(!CollectionUtils.isEmpty(voteIssues)){
                Map<Integer,Integer> resultMap = houseVoteIssuesMap.get(houseVote.getId());
                for(int i = 0;i< voteIssues.size();i++){
                    int index = i + 1;
                    VoteIssue voteIssue = voteIssues.get(i);
                    map.put("${result_"+index+"_1}","&nbsp;&nbsp;");
                    map.put("${result_"+index+"_2}","&nbsp;&nbsp;");
                    map.put("${result_"+index+"_3}","&nbsp;&nbsp;");
                    if(resultMap == null){
                        continue;
                    }
                    map.put("${result_"+index+"_"+resultMap.get(voteIssue.getId())+"}"," √️");
                }
            }
            mapList.add(map);
        }
        String endNum = params.getPageNum()*params.getPageSize() + "";
        if(params.getPageNum() >= result.getPages()){
            endNum = result.getTotal() + "";
        }
        //解决xml过大报错，JDK限制为64000个，设置xml引用实体数为99999999
        System.setProperty("entityExpansionLimit", "99999999");
        HtmlToPdfUtil.html2PdfBacth("static/pdf-xmbd.html","业主大会会议表决票",mapList,response);
        return BaseVo.create(true);
    }


}
