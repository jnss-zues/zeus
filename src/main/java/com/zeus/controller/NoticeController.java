package com.zeus.controller;

import com.github.pagehelper.PageInfo;
import com.zeus.common.enums.ErrorEnum;
import com.zeus.domain.entity.Notice;
import com.zeus.domain.param.NoticeParams;
import com.zeus.domain.param.VoteRecordQueryParams;
import com.zeus.domain.vo.BaseVo;
import com.zeus.service.ImgsService;
import com.zeus.service.NoticeService;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Date;
import java.util.List;

@RestController
public class NoticeController extends  BaseController{

    @Resource
    private NoticeService noticeService;

    @Resource
    private ImgsService imgsService;

    /**
     * 穿件公告
     * @param noticeParams
     * @return
     */
    @PostMapping("/notice")
    public BaseVo create(HttpServletRequest request, @RequestBody NoticeParams noticeParams){
        Integer userId = this.getUserId(request);
        Integer communityId = this.getCommunityId(request);
        noticeParams.setPublisher(userId);
        noticeParams.setCommunityId(communityId);
        noticeService.create(noticeParams);
        return BaseVo.create(null);
    }

    /**
     * 编辑公告
     * @param noticeParams
     * @return
     */
    @PatchMapping("/notice")
    public BaseVo update(@RequestBody NoticeParams noticeParams){
        if(noticeParams.getId() == null){
            return BaseVo.errorCreate(ErrorEnum.PARAMS_NULL);
        }
        noticeParams.setUpdateTime(new Date());
        noticeService.update(noticeParams);
        return BaseVo.create(null);
    }

    /**
     * 删除公告
     * @param noticeParams
     * @return
     */
    @DeleteMapping("/notice")
    public BaseVo delete(NoticeParams noticeParams,Integer id){
        if(noticeParams.getId() == null){
            return BaseVo.errorCreate(ErrorEnum.PARAMS_NULL);
        }
        noticeParams.setUpdateTime(new Date());
        noticeService.delete(noticeParams);
        return BaseVo.create(null);
    }



    /**
     * 公告列表
     * @param noticeParams
     * @return
     */
    @GetMapping("/notices")
    public BaseVo ListNotice(NoticeParams noticeParams,HttpServletRequest request){
        Integer communityId = this.getCommunityId(request);
        noticeParams.setCommunityId(communityId);
        String community = (String) request.getSession().getAttribute("community");
        PageInfo pageInfo = noticeService.listNotice(noticeParams);
        List<Notice> notices = pageInfo.getList();
        pageInfo.setList(noticeService.setPublisherName(notices,community));
        return BaseVo.create(pageInfo);
    }

    /**
     * 获取公告详情
     * @param id
     * @return
     */
    @GetMapping("/notice/{id}")
    public BaseVo getInfo(@PathVariable Integer id,HttpServletRequest request){
        return BaseVo.create(noticeService.getDetail(id,request));
    }

    @PostMapping("/notice/uploadImg")
    public BaseVo imgUpload(@RequestParam MultipartFile[] txt_file, HttpServletRequest request,
                            @RequestParam String userId , @RequestParam Integer type,
                            String uuid) throws IOException {
        return imgsService.uploadImgs(txt_file,type,userId,uuid);
    }

    //获取投票分享二维码
    @GetMapping("/notice/qrcode")
    public void getVoteQrCode(NoticeParams noticeParams, HttpServletResponse response){
        if(noticeParams.getId() == null){
            BaseVo.errorCreate(ErrorEnum.PARAMS_NULL);
            return;
        }
        noticeService.getQrCodeImg(noticeParams.getId(),response);
    }
}
