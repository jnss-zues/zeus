package com.zeus.controller;

import cn.hutool.core.collection.CollectionUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.zeus.common.Properties;
import com.zeus.common.StringUtil;
import com.zeus.common.enums.ErrorEnum;
import com.zeus.common.utils.HttpClientManager;
import com.zeus.common.utils.MessageUtils;
import com.zeus.common.utils.RedisUtil;
import com.zeus.domain.SmsKeyEnum;
import com.zeus.domain.dto.UserInfoDTO;
import com.zeus.domain.dto.WxInfoDTO;
import com.zeus.domain.entity.*;
import com.zeus.domain.param.VoteRecordQueryParams;
import com.zeus.domain.vo.BaseVo;
import com.zeus.service.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Slf4j
@Controller
public class LoginController {

    @Resource
    private UserService userService;

    @Resource
    private Properties properties;

    @Resource
    private CommunityService communityService;

    @Resource
    private WxInfoService wxInfoService;

    @Resource
    private OwnerService ownerService;

    @Resource
    private RedisUtil redisUtil;

    @Resource
    private HouseVoteService houseVoteService;

    ExecutorService executorService = Executors.newSingleThreadExecutor();

    /**
     * 手机登录
     * @param mobile
     * @param code
     * @param request
     * @param communityId
     * @return
     */
    @ResponseBody
    @PostMapping("/login")
    public BaseVo login(@RequestParam String mobile, @RequestParam String code, HttpServletRequest request,@RequestParam Integer communityId) {
        User user = userService.login(mobile, code,communityId);
        if (user == null) {
            return BaseVo.errorCreate(ErrorEnum.LOGIN_FAILED);
        }
        Community community = communityService.getById(user.getCommunityId());
        UserInfoDTO userInfo = new UserInfoDTO();
        BeanUtils.copyProperties(user, userInfo);
        userInfo.setCommunity(community.getName() == null ? "未知" : community.getName());
        userInfo.setSignature(community.getSignature() == null ? "未知" : community.getSignature());
        HttpSession session = request.getSession();
        session.setAttribute("userInfo", user);
        session.setAttribute("userId", user.getId());
        session.setAttribute("userName", user.getName());
        session.setAttribute("wxAvatarUrl", user.getWxAvatarUrl());
        session.setAttribute("communityId", community.getId());
        session.setAttribute("community", community.getName());
        session.setAttribute("signature", community.getSignature());

        return BaseVo.create(userInfo);
    }

    /**
     * 登出
     * @param request
     * @return
     */
    @ResponseBody
    @PostMapping("/logout")
    public BaseVo logout(HttpServletRequest request) {
        HttpSession session = request.getSession();
        if(session == null){
            return BaseVo.create(null);
        }
        session.invalidate();
        return BaseVo.create(null);
    }
    /**
     * 发送短信验证码
     *
     * @param mobile
     * @param key
     * @return
     */
    @RequestMapping("/sendSms")
    @ResponseBody
    public BaseVo sendMsg(@RequestParam String mobile, @RequestParam Integer key,
                           Integer communityId) {
        if(SmsKeyEnum.LOGIN.getCode().equals(key)){
            User user = userService.getByMobile(mobile,communityId);
            if(user == null){
                return BaseVo.errorCreate(ErrorEnum.INVALID_MOBILE);
            }
        }else{
            VoteRecordQueryParams voteRecordQueryParams = new VoteRecordQueryParams();
            voteRecordQueryParams.setMobiles(mobile);
            List<HouseVote> houseVoteList = houseVoteService.ListAllByParam(voteRecordQueryParams);
            if(CollectionUtil.isEmpty(houseVoteList)){
                return BaseVo.errorCreate(ErrorEnum.INVALID_MOBILE);
            }
        }

        executorService.execute(() -> {
            Long code = null;
            try {
                code = MessageUtils.sendMsg(mobile);
            } catch (Exception e) {
                e.printStackTrace();
            }
            if(code != null){
                redisUtil.set(mobile + SmsKeyEnum.getDesc(key), code,300);
            }
        });
        return BaseVo.create(true);

    }

    /**
     * 微信扫码登录
     * @param request
     * @param response
     * @param communityId
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping("wxLogin")
    public BaseVo wxLogin(HttpServletRequest request, HttpServletResponse response,@RequestParam Integer communityId) throws Exception {
        if(communityId == null){
            return BaseVo.errorCreate(ErrorEnum.NONE_COMMUNITY);
        }
        String token = request.getParameter("token");
        log.info("token:" + token);
        String openId = (String) redisUtil.get(token);
        log.info("openId:" + openId);
        if (StringUtil.isBlank(openId)) {
            return BaseVo.errorCreate(ErrorEnum.NONE_OPENID);
        }
        HttpSession session = request.getSession();
        Integer userId = (Integer) session.getAttribute("userId");
        log.info("当前登录人:" + userId);
        User user = null;
        //若已经登录，再次扫码进行微信绑定
        if (userId != null) {
            log.info("已经密码登录,再次扫码绑定");
            user = userService.getById(userId);
            WxInfoDTO wxInfo = getWxInfo(openId);
            if(wxInfo != null){
                user.setWxGender(String.valueOf(wxInfo.getSex()));
                user.setWxAvatarUrl(wxInfo.getHeadimgurl());
                user.setWxCity(wxInfo.getCity());
                user.setWxCountry(wxInfo.getCountry());
                user.setWxProvince(wxInfo.getProvince());
                user.setWxName(wxInfo.getNickname());
            }
            user.setWxOpenid(openId);
            userService.update(user);
        } else {
            log.info("直接扫码登录");
            user = userService.getByOpenId(openId,communityId);
        }
        if (user != null) {
            log.info("用户信息:" +JSON.toJSONString(user));
            Community community = communityService.getById(user.getCommunityId());
            UserInfoDTO userInfo = new UserInfoDTO();
            BeanUtils.copyProperties(user, userInfo);
            userInfo.setCommunity(community.getName() == null ? "未知" : community.getName());
            userInfo.setSignature(community.getSignature());
            session.setAttribute("userInfo", user);
            session.setAttribute("userId", user.getId());
            session.setAttribute("wxAvatarUrl", user.getWxAvatarUrl());
            session.setAttribute("userName", user.getName());
            session.setAttribute("communityId", community.getId());
            session.setAttribute("community", community.getName());
            session.setAttribute("signature", community.getSignature());
            //用掉就及时清除
            redisUtil.del(token);
            return BaseVo.create(userInfo);
        }
        log.info("用户信息为空");
        redisUtil.del(token);
        return BaseVo.errorCreate(ErrorEnum.WX_LOGIN_ERROR);
    }

    /**
     * 通过opendId获取微信信息
     * @param openId
     * @return
     * @throws Exception
     */
    public WxInfoDTO getWxInfo(String openId) throws Exception {
        String token = wxInfoService.getAccessToken();
        return wxInfoService.getWxInfo(openId,token);
    }

    /**
     * 获取二维码ticket
     * @param token
     * @return
     */
    @ResponseBody
    @PostMapping("getTicket")
    public BaseVo getTicket(@RequestParam String token) {
        try {
            String accessToken = wxInfoService.getAccessToken();
            String ticketUrl = "https://api.weixin.qq.com/cgi-bin/qrcode/create?access_token=" + accessToken;
            String ticketParam = "{\"action_name\": \"QR_LIMIT_STR_SCENE\", \"action_info\": {\"scene\": {\"scene_str\": \"" + token + "\"}}}";
            String ticketResult = HttpClientManager.postUrlData(ticketUrl, ticketParam);
            JSONObject ticketObject = JSON.parseObject(ticketResult);
            if (ticketObject.get("ticket") != null) {
                String ticket = (String) ticketObject.get("ticket");
                return BaseVo.create(ticket);
            }
        } catch (Exception e) {
            return BaseVo.errorCreate(ErrorEnum.INVOKE_ERROR);
        }
        return BaseVo.errorCreate(ErrorEnum.INVOKE_ERROR);
    }
}
