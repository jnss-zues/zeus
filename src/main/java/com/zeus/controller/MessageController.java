package com.zeus.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.zeus.common.enums.ErrorEnum;
import com.zeus.common.utils.MessageUtil;
import com.zeus.common.utils.MessageUtils;
import com.zeus.common.utils.RedisUtil;
import com.zeus.common.utils.SignUtil;
import com.zeus.domain.dto.TextMessage;
import com.zeus.domain.vo.BaseVo;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Controller
@Slf4j
@RequestMapping("/message/receive")
public class MessageController {

    @Resource
    private RedisUtil redisUtil;


    @RequestMapping(method = RequestMethod.GET)
//    @ResponseBody
    public String checkToken(HttpServletRequest request, HttpServletResponse response) throws IOException {
//        if (StringUtils.isBlank(request.getParameter("signature"))) {
//            return;
//        }
        String signature = request.getParameter("signature");
        String timestamp = request.getParameter("timestamp");
        String nonce = request.getParameter("nonce");
        String echostr = request.getParameter("echostr");
        log.info("signature[{}], timestamp[{}], nonce[{}], echostr[{}]", signature, timestamp, nonce, echostr);
        PrintWriter out = response.getWriter();
        //校验不通过
        if (SignUtil.checkSignature(signature, timestamp, nonce)) {
            log.info("数据源为微信后台，将echostr[{}]返回！", echostr);
            out.write(echostr);
        }
        out.close();
        return null;
    }



    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    public void dealMessage(HttpServletRequest request, HttpServletResponse response) {
        String respMessage = "异常消息！";
        try {
            respMessage = this.weixinPost(request);
            PrintWriter out = response.getWriter();
            out.write(respMessage);
            log.info("The request completed successfully");
            log.info("to weixin server " + respMessage);
        } catch (Exception e) {
            log.error("Failed to convert the message from weixin!");
        }
    }

    /**
     * 处理微信发来的请求
     *
     * @param request
     * @return
     */
    public String weixinPost(HttpServletRequest request) {
        String respMessage = null;
        try {

            // xml请求解析
            Map<String, String> requestMap = MessageUtil.xmlToMap(request);

            // 发送方帐号（open_id）
            String fromUserName = requestMap.get("FromUserName");
            // 公众帐号
            String toUserName = requestMap.get("ToUserName");
            // 消息类型
            String msgType = requestMap.get("MsgType");
            // 消息内容
            String content = requestMap.get("Content");

            String EventKey = requestMap.get("EventKey");

            String Ticket = requestMap.get("Ticket");

            log.info("FromUserName is:" + fromUserName + ", ToUserName is:" + toUserName + ", MsgType is:" + msgType);
            log.info("content is : " + content + "，EventKey is : " + EventKey + "，Ticket : " + Ticket);

            //目前只拦截扫描带参二维码事件,其他事件可根据情况进行处理
            if (StringUtils.isNoneBlank(EventKey) && StringUtils.isNoneBlank(fromUserName)) {
                redisUtil.set(EventKey, fromUserName,3600);
                return null;
            }
            if (msgType.equals(MessageUtil.REQ_MESSAGE_TYPE_EVENT)) {
                String eventType = requestMap.get("Event");// 事件类型
                // 订阅
                if (eventType.equals(MessageUtil.EVENT_TYPE_SUBSCRIBE)) {

                    TextMessage text = new TextMessage();
                    text.setContent("欢迎关注，xxx");
                    text.setToUserName(fromUserName);
                    text.setFromUserName(toUserName);
                    text.setCreateTime(new Date().getTime() + "");
                    text.setMsgType(MessageUtil.RESP_MESSAGE_TYPE_TEXT);

                    respMessage = MessageUtil.textMessageToXml(text);
                } else if (eventType.equals(MessageUtil.EVENT_TYPE_UNSUBSCRIBE)) {// 取消订阅


                }
            }
        } catch (Exception e) {
            log.error("error......");
        }
        return respMessage;
    }
}
