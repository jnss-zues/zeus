package com.zeus.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class BaseController {

    public Integer getUserId(HttpServletRequest request){
        HttpSession session = request.getSession();
        Object userId = session.getAttribute("userId");
        if(userId == null){
            return null;
        }
        return (Integer)userId;
    }

    public Integer getCommunityId(HttpServletRequest request){
        HttpSession session = request.getSession();
        Object communityId = session.getAttribute("communityId");
        if(communityId == null){
            return null;
        }
        return (Integer)communityId;
    }
}
