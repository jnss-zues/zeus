package com.zeus.common.job;

import com.zeus.common.Properties;
import com.zeus.dao.VoteDao;
import com.zeus.service.VoteService;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
public class VoteStatusJob {

    @Resource
    private Properties properties;

    @Resource
    private VoteDao voteDao;


    /**
     * 一分钟调一次,更新投票状态
     */
    @Scheduled(cron="1 * * * * ?")
    public void updateStatus(){
        voteDao.start();
        voteDao.end();
    }
}
