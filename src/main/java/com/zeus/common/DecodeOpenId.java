package com.zeus.common;

import com.zeus.common.utils.Pkcs7Encoder;
import com.zeus.common.utils.UUIDs;
import com.zeus.domain.entity.User;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.Base64Utils;

import java.io.UnsupportedEncodingException;

public class DecodeOpenId {

	public static User getUser(String sessionKey, String encryptedData, String iv) {
		String result = "";
        byte[] sessionKeyBy = Base64Utils.decode(sessionKey.getBytes());
        byte[] encryptedDataBy = Base64Utils.decode(encryptedData.getBytes());
        byte[] ivBy = Base64Utils.decode(iv.getBytes());
        byte[] dec = Pkcs7Encoder.decryptOfDiyIV(encryptedDataBy, sessionKeyBy, ivBy);
        JSONObject h = null;
        User user = new User();
        try {
            h = JSONObject.parseObject(new String(dec, "utf-8"));
//            user.setId(UUIDs.getUUID());
            user.setWxOpenid((String)h.get("openId"));
            user.setWxAvatarUrl((String)h.get("avatarUrl"));
            user.setWxName((String)h.get("nickName"));
            user.setWxCity((String)h.get("city"));
            user.setWxProvince((String)h.get("province"));
            user.setWxCountry((String)h.get("country"));
//            user.setWxGender((Integer)h.get("gender"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        if (h.containsKey("openId")) {
            result = h.toString();
            System.out.println("解密后的内容：" + h.toString());
        }
        return user;
	}
}
