package com.zeus.common.utils;

import com.lowagie.text.DocumentException;
import com.lowagie.text.pdf.BaseFont;
import com.zeus.common.StringUtil;
import com.zeus.controller.PdfDemoTest;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.csv.CSVFormat;
import org.springframework.util.CollectionUtils;
import org.xhtmlrenderer.pdf.ITextFontResolver;
import org.xhtmlrenderer.pdf.ITextRenderer;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

@Slf4j
public class HtmlToPdfUtil {

    public static final String ttcPath = "/usr/local/static/fonts/simsun.ttc";

    public static void html2Pdf(String htmlTemplate,String fileName ,
                                Map<String, String> contentMap,
                                HttpServletResponse response) {
        try {
            //读取html的流
            InputStream inputStream = HtmlToPdfUtil.class.getClassLoader().getResourceAsStream(htmlTemplate);

            //流转换成字符串
            StringBuffer out = new StringBuffer();
            byte[] b = new byte[4096];
            for (int n; (n = inputStream.read(b)) != -1; ) {
                out.append(new String(b, 0, n));
            }

            String html = out.toString();
            html = replaceContent(html,contentMap);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            ITextRenderer renderer = new ITextRenderer();
            renderer.setDocumentFromString(html);
            // writer.setPageEvent(header);
            ITextFontResolver fontResolver = renderer.getFontResolver();
            // 添加字体支持,路径可以自身项目的实际情况设置，我这里是本地项目，而且为了方便测试，就写成固定的了
            // 实际项目中，可以获取改字体所在真实的服务器的路径,这个方法是本地地址和网络地址都支持的
            // 这里面添加的是宋体

            fontResolver.addFont(ttcPath,
                    BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
            renderer.layout();
            renderer.createPDF(byteArrayOutputStream);
            response.setContentType(response.getContentType());
            response.setHeader("Content-Disposition",
                    "attachment;filename=" + new String(fileName.getBytes("UTF8"), "ISO8859-1") + ".pdf");
            CSVFormat format = CSVFormat.DEFAULT.withRecordSeparator("\n");
            byte[] bytes = byteArrayOutputStream.toByteArray();
            response.setHeader("Content-Length", String.valueOf(bytes.length));
            BufferedOutputStream bos = null;
            bos = new BufferedOutputStream(response.getOutputStream());
            bos.write(bytes);
            bos.close();
            byteArrayOutputStream.close();
        }catch (Exception e){
            e.printStackTrace();
            log.error("导出pdf异常",e);
        }
    }

    public static String replaceContent(String html,Map<String, String> contentMap){
        if(!CollectionUtils.isEmpty(contentMap)){
            Iterator<Map.Entry<String,String>> it = contentMap.entrySet().iterator();
            while (it.hasNext()){
                Map.Entry<String,String> entry = it.next();
                html = StringUtil.replace(html,"${"+entry.getKey()+"}",entry.getValue());
            }
        }
        return html;

    }

    public static String replaceContentBacth(String html,List<Map<String, String>> contentMapList){
        StringBuilder sb = new StringBuilder();
        if(!CollectionUtils.isEmpty(contentMapList)){
            for(Map<String, String> map : contentMapList){
                Iterator<Map.Entry<String,String>> it = map.entrySet().iterator();
                String htmlTemp = html;
                while (it.hasNext()){
                    Map.Entry<String,String> entry = it.next();
                    htmlTemp = StringUtil.replace(htmlTemp,entry.getKey(),entry.getValue());
                }
                sb.append(htmlTemp);
            }
        }
        return sb.toString();

    }

    public static void html2PdfBacth(String htmlTemplate,String fileName ,
                                List<Map<String, String>> contentMapList,
                                HttpServletResponse response) {
        try {
            //读取html的流
            InputStream inputStream = HtmlToPdfUtil.class.getClassLoader().getResourceAsStream(htmlTemplate);
            //流转换成字符串
            StringBuffer out = new StringBuffer();
            byte[] b = new byte[4096];
            for (int n; (n = inputStream.read(b)) != -1; ) {
                out.append(new String(b, 0, n));
            }
            String html = out.toString();
            int starIndex = html.indexOf("<body>") + "<body>".length();
            int endIndex = html.indexOf("</body>");
            String content  = html.substring(starIndex,endIndex);

            StringBuilder sb = new StringBuilder(html);
            String contents = replaceContentBacth(content,contentMapList);
            sb.replace(starIndex,endIndex,"");
            sb.insert(starIndex,contents);

            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            ITextRenderer renderer = new ITextRenderer();
            renderer.setDocumentFromString(sb.toString());
            // writer.setPageEvent(header);
            ITextFontResolver fontResolver = renderer.getFontResolver();
            // 添加字体支持,路径可以自身项目的实际情况设置，我这里是本地项目，而且为了方便测试，就写成固定的了
            // 实际项目中，可以获取改字体所在真实的服务器的路径,这个方法是本地地址和网络地址都支持的
            // 这里面添加的是宋体
            fontResolver.addFont(ttcPath,
                    BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
            renderer.layout();
            renderer.createPDF(byteArrayOutputStream);
            response.setContentType(response.getContentType());
            response.setHeader("Content-Disposition",
                    "attachment;filename=" + new String(fileName.getBytes("UTF8"), "ISO8859-1") + ".pdf");
            CSVFormat format = CSVFormat.DEFAULT.withRecordSeparator("\n");
            byte[] bytes = byteArrayOutputStream.toByteArray();
            response.setHeader("Content-Length", String.valueOf(bytes.length));
            BufferedOutputStream bos = null;
            bos = new BufferedOutputStream(response.getOutputStream());
            bos.write(bytes);
            bos.close();
            byteArrayOutputStream.close();
        }catch (Exception e){
            e.printStackTrace();
            log.error("批量导出pdf异常",e);

        }
    }
}
