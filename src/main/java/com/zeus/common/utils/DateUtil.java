package com.zeus.common.utils;

import com.google.common.base.Strings;
import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Time;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by damao on 2017/4/19.
 */
public class DateUtil {

    private static Logger logger = LoggerFactory.getLogger(DateUtil.class);

    private DateUtil() {
    }

    public static final SimpleDateFormat formater1 = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
    public static final SimpleDateFormat formater2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    public static final SimpleDateFormat formater3 = new SimpleDateFormat("yyyy:MM:dd hh:mm:ss");
    public static final SimpleDateFormat formater4 = new SimpleDateFormat("yyyyMMdd");
    public static final SimpleDateFormat formater5 = new SimpleDateFormat("MM-dd HH:mm");
    public static final SimpleDateFormat formater_yyyyMMdd = new SimpleDateFormat("yyyy-MM-dd");
    public static final SimpleDateFormat formater_yyyyMMdd_readable = new SimpleDateFormat("yyyy年MM月dd日");
    public static final SimpleDateFormat formater_yyyyMMdd_dot = new SimpleDateFormat("yyyy.MM.dd");
    public static final SimpleDateFormat formater_yyyyMMdd_slash = new SimpleDateFormat("yyyy/MM/dd");
    public static final String format_yyyyMMdd = "yyyy-MM-dd";
    public static final String format_yyyyMMdd_HHmmss = "yyyy-MM-dd HH:mm:ss";
    public static final SimpleDateFormat formater_MMddyyyy = new SimpleDateFormat("MM/dd/yyyy");
    public static final SimpleDateFormat formater_ALL = new SimpleDateFormat("ssyyyySSShhddmmMM");
    public static final SimpleDateFormat Formater_ALL_NORMAL = new SimpleDateFormat("yyyyMMddHHmmssSSS");
    public static final SimpleDateFormat Formater_ALL_NORMAL2 = new SimpleDateFormat("yyyyMMddHHmmss");
    public static final SimpleDateFormat formater_hhmm = new SimpleDateFormat("hhmm");
    public static final SimpleDateFormat formater_hhmmss = new SimpleDateFormat("HH:mm:ss");


    /**
     * @param str 格式要求 yyyy,m(mm),d(dd) 如：2002-01-02，2002-1-2，2002-2-15
     * @Title stringToDate
     * @Description 字符串转化为Date
     */
    public static Date stringToDate(String str) {
        if (str == null) {
            return null;
        }
        String strFormat = "yyyy-MM-dd HH:mm";
        if (str != null && (str.length() == 10 || str.length() == 8 || str.length() == 9)) {
            strFormat = "yyyy-MM-dd";
        }
        SimpleDateFormat sdFormat = new SimpleDateFormat(strFormat);
        Date date;
        try {
            date = sdFormat.parse(str);
        } catch (ParseException e) {
            logger.error("", e);
            try {
                sdFormat = new SimpleDateFormat("yyyy:MM:dd HH:mm:ss");
                date = sdFormat.parse(str);
            } catch (Exception e1) {
                logger.error("Error=", e1);
                return null;
            }
        } catch (NullPointerException e2) {
            logger.error("Error=", e2);
            return null;
        }
        return date;
    }

    /**
     * @Title stringToDate
     * @@Description str 字符串以指定格式转化为Date
     */
    public static Date stringToDate(String str, String strFormat) {
        SimpleDateFormat sdFormat = new SimpleDateFormat(strFormat);
        Date date;
        try {
            date = sdFormat.parse(str);
        } catch (Exception e) {
            return null;
        }
        return date;
    }

    /**
     * @Title dateToYMD
     * @@Description 字符串以指定格式转化为Date
     */
    public static String dateToYMD(Date dt) {
        SimpleDateFormat sdFormat = new SimpleDateFormat("yyyy-MM-dd");
        String str;
        try {
            str = sdFormat.format(dt);
        } catch (Exception e) {
            return "";
        }
        if (str.equals("1900-01-01")) {
            str = "";
        }
        return str;
    }

    /**
     * @Title dateToString
     * @@Description Date转换为字符串 yyyy-MM-dd HH:mm
     */
    public static String dateToString(Date dt) {
        SimpleDateFormat sdFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        String str;
        try {
            str = sdFormat.format(dt);
        } catch (Exception e) {
            return "";
        }
        if (str.equals("1900-01-01 00:00")) {
            str = "";
        }
        return str;
    }

    /**
     * @Title dateToString
     * @@Description Date按指定格式转换为字符串
     */
    public static String dateToString(Date dt, String strFormat) {
        SimpleDateFormat sdFormat = new SimpleDateFormat(strFormat);
        String str;
        try {
            str = sdFormat.format(dt);
        } catch (Exception e) {
            return "";
        }
        if (str.equals("1900-01-01 00:00")) {
            str = "";
        }
        return str;
    }

    /**
     * @Title dateToString
     * @@Description 根据格式获得当前日期字符串
     */
    public static String getDateStr(String sFormat) {
        Calendar tCal = Calendar.getInstance();
        Timestamp ts = new Timestamp(tCal.getTime().getTime());
        Date date = new Date(ts.getTime());
        SimpleDateFormat formatter = new SimpleDateFormat(sFormat);
        return formatter.format(date);
    }

    /**
     * @Title tsToString
     * @@Description 根据给定格式获取特定Timestamp时间的格式化显示
     */
    public static String getDateFormat(Timestamp ts, String sFormat) {
        Date date = new Date(ts.getTime());
        SimpleDateFormat formatter = new SimpleDateFormat(sFormat);
        return formatter.format(date);
    }

    /**
     * @Title tsToString
     * @@Description 按yyyy-MM-dd格式化Timestamp日期
     */
    public static String getSDate(Timestamp ts) {
        Date date = new Date(ts.getTime());
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        return formatter.format(date);
    }

    /**
     * @Title stringToTs
     * @@Description 将String类型的日期转换为时间
     */
    public static Timestamp getTs(String dt) {
        Date date = java.sql.Date.valueOf(dt);
        Calendar tCal = Calendar.getInstance();
        tCal.setTime(date);
        return new Timestamp(tCal.getTime().getTime());
    }

    /**
     * @Title stringToString
     * @@Description 建议获得短日期的处理方式 例如: getShortDate(2004-10-10 10:10:10.123) = 2004-10-10
     */
    public static String getShortDate(String dt) {
        try {
            return dt.substring(0, dt.indexOf(" "));
        } catch (Exception e) {
            return dt;
        }
    }

    /**
     * @Title getTs
     * @@Description 取得当前日期时间
     */
    public static Timestamp getCurrDateTime() {
        Calendar tCal = Calendar.getInstance();
        Timestamp createDate = new Timestamp(tCal.getTime().getTime());
        return createDate;
    }

    /**
     * @Title getLong
     * @@Description 取得当前日期时间
     */
    public static long getMillSecondOfCurrnetDateTime() {
        return (new Date()).getTime();
    }

    /**
     * @Title dateToTs
     * @@Description 取得当前日期时间
     */
    public static Timestamp getCurrDateTimeByFormat_yyyyMMdd(Date date) {
        return new Timestamp(date.getYear(), date.getMonth(), date.getDate(), 0, 0, 0, 0);
    }

    /**
     * @Title tsToString
     * @@Description 获得最常见的日期格式内容 : 年-月-日 小时-分钟-秒
     */
    public static String getSDateTime(Timestamp ts) {
        return getDateFormat(ts, "yyyy-MM-dd HH:mm:ss");
    }

    /**
     * @Title tsToString
     * @@Description 按HH:mm:ss格式化日期
     */
    public static String getSTime(Timestamp ts) {
        return getDateFormat(ts, "HH:mm:ss");
    }

    /**
     * @Title getString
     * @@Description 获取当天的日期
     */
    public static String getToday() {
        Timestamp ts = new Timestamp(System
                .currentTimeMillis());
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        return formatter.format(ts);
    }

    /**
     * @@Description 当前日期增加一天
     */
    public static Date addOneDay(Date date) {
        if (date == null) {
            return null;
        } else {
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            cal.add(5, 1);
            date = cal.getTime();
            return date;
        }
    }

    /**
     * @Title getString
     * @@Description 获取明天的日期
     */
    public static String getTomorrow() {
        return getNextDay(getToday());
    }

    /**
     * @Title stringToString
     * @@Description 获得当前日期的下一天
     */
    public static String getNextDay(String date) {
        if (date == null || date.trim().length() == 0) {
            return "";
        }
        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd");
        Calendar calendar = Calendar.getInstance();
        try {
            calendar.setTime(f.parse(date));
        } catch (ParseException ex) {
            return date;
        }
        calendar.add(5, 1);
        return f.format(calendar.getTime());
    }

    /**
     * @Title intToString
     * @@Description 获取几天前或后的时间
     */
    public static String getTheDateStr(int num) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        GregorianCalendar gc = new GregorianCalendar();
        if (num != 0) {
            gc.add(GregorianCalendar.DATE, num);
        }
        Date theday = gc.getTime();
        return sdf.format(theday);
    }

    /**
     * @Title intToString
     * @@Description 获取几天前或后的时间指定格式
     */
    public static String getTheDateStrFormat(int num, String format) {
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        GregorianCalendar gc = new GregorianCalendar();
        if (num != 0) {
            gc.add(GregorianCalendar.DATE, num);
        }
        Date theday = gc.getTime();
        return sdf.format(theday);
    }

    /**
     * @Title intToDate
     * @@Description 得到几天前的时间
     */
    public static Date getTheDate(Date d, int day) {
        Calendar now = Calendar.getInstance();
        now.setTime(d);
        now.set(Calendar.DATE, now.get(Calendar.DATE) + day);
        return now.getTime();
    }

    /**
     * @Title tsToInt
     * @@Description 计算Timestamp日期之间的日差
     */
    public static int dateDiff(Timestamp dt1, Timestamp dt2) {
        long ldate1 = dt1.getTime();
        long ldate2 = dt2.getTime();
        return (int) ((ldate1 - ldate2) / (1000 * 60 * 60 * 24));
    }

    public static int dateDiff2(Date dt1, Date dt2) {
        long ldate1 = dt1.getTime();
        long ldate2 = dt2.getTime();
        return (int) ((ldate1 - ldate2) / (1000 * 60 * 60 * 24));
    }

    /**
     * 计算两个时间戳之间相差的小时数
     *
     * @return
     */
    public static int hourDiff(Timestamp dt1, Timestamp dt2) {
        long ldate1 = dt1.getTime();
        long ldate2 = dt2.getTime();
        return (int) ((ldate1 - ldate2) / (1000 * 60 * 60));
    }

    public static int hourDiff2(Date dt1, Date dt2) {
        long ldate1 = dt1.getTime();
        long ldate2 = dt2.getTime();
        return (int) ((ldate1 - ldate2) / (1000 * 60 * 60));
    }

    public static int minuteDiff2(Date dt1, Date dt2) {
        long ldate1 = dt1.getTime();
        long ldate2 = dt2.getTime();
        return (int) ((ldate1 - ldate2) / (1000 * 60));
    }

    /**
     * @@Description 计算两个日期字符串之前的天数差值 仅考虑日期
     */
    public static int calDayDiffBetweenTwoDateStr(String dateStr1, String dateStr2)
            throws ParseException {
        Date date1 = formater_yyyyMMdd.parse(dateStr1);
        Date date2 = formater_yyyyMMdd.parse(dateStr2);
        long diffMiliSeconds = date1.getTime() - date2.getTime();
        return (int) (diffMiliSeconds / (1000 * 60 * 60 * 24));
    }

    public static String getDateStrFromTimestamp(Timestamp time) {
        return formater_yyyyMMdd.format(time);
    }

    /**
     * @@Description 根据所给日期返回两Date日期相差的秒数
     */
    public static long getSecond(Date d1, Date d2) {
        long a1 = d1.getTime();
        long a2 = d2.getTime();
        long a3 = (a1 - a2) / 1000;
        return a3;
    }

    /**
     * @@Description 根据所给日期返回两日期相差的天数
     */
    public static long getDayNum(Date d1, Date d2) {
        long a1 = d1.getTime();
        long a2 = d2.getTime();
        long a3 = (a2 - a1) / (24 * 60 * 60 * 1000);
        return a3;
    }

    /**
     * @@Description 判断当前日期是星期几
     */
    public static int dayForWeek(String pTime) throws Exception {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Calendar c = Calendar.getInstance();
        c.setTime(format.parse(pTime));
        int dayForWeek = 0;
        if (c.get(Calendar.DAY_OF_WEEK) == 1) {
            dayForWeek = 7;
        } else {
            dayForWeek = c.get(Calendar.DAY_OF_WEEK) - 1;
        }
        return dayForWeek;
    }

    /**
     * @@Description 根据所秒数, 计算相差的时间并以**时**分**秒返回
     */
    public static String getBeapartDate(long m) {
        String beapartdate = "";
        int nDay = (int) m / (24 * 60 * 60);
        int nHour = (int) (m - nDay * 24 * 60 * 60) / (60 * 60);
        int nMinute = (int) (m - nDay * 24 * 60 * 60 - nHour * 60 * 60) / 60;
        beapartdate = Math.abs(nDay) + "天"
                + Math.abs(nHour) + "小时"
                + Math.abs(nMinute) + "分";
        return beapartdate;
    }

    /**
     * @@Description 检查日期格式
     */
    public static boolean checkDate(String date) {
        boolean ret = true;
        try {
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            df.setLenient(false);
            ret = df.format(df.parse(date)).equals(date);
        } catch (ParseException e) {
            System.out.println("日期格式错误");
            ret = false;
            return ret;
        }
        String eL = "^((\\d{2}(([02468][048])|([13579][26]))[\\-\\/\\s]?((((0?[13578])|(1[02]))[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])|(3[01])))|(((0?[469])|(11))[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])|(30)))|(0?2[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])))))|(\\d{2}(([02468][1235679])|([13579][01345789]))[\\-\\/\\s]?((((0?[13578])|(1[02]))[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])|(3[01])))|(((0?[469])|(11))[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])|(30)))|(0?2[\\-\\/\\s]?((0?[1-9])|(1[0-9])|(2[0-8]))))))(\\s(((0?[0-9])|([1][0-9])|([2][0-3]))\\:([0-5]?[0-9])((\\s)|(\\:([0-5]?[0-9])))))?$";
        Pattern p = Pattern.compile(eL);
        Matcher m = p.matcher(date);
        boolean b = m.matches();
        if (b) {

            System.out.println(date + "格式正确");
        } else {
            System.out.println(date + "格式错误");
        }
        return b;

    }

    public static boolean isYesterday(Date date) {

        Calendar todayCal = Calendar.getInstance();
        todayCal.setTime(new Date());
        Calendar dateCal = Calendar.getInstance();
        dateCal.setTime(date);
        dateCal.add(Calendar.DAY_OF_YEAR, 1);

        return dateCal.get(Calendar.DAY_OF_YEAR) == todayCal.get(Calendar.DAY_OF_YEAR);

    }

    /**
     * @@Description 根据日期计算所在周的上一周的周一
     */
    public static Date getPreviousFirstDayOfWeek(Date time) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd"); //设置时间格式
        Calendar cal = Calendar.getInstance();
        cal.setTime(time);
        //判断要计算的日期是否是周日，如果是则减一天计算周六的，否则会出问题，计算到下一周去了
        int dayWeek = cal.get(Calendar.DAY_OF_WEEK);//获得当前日期是一个星期的第几天
        if (1 == dayWeek) {
            cal.add(Calendar.DAY_OF_MONTH, -1);
        }
        cal.setFirstDayOfWeek(Calendar.MONDAY);//设置一个星期的第一天，按中国的习惯一个星期的第一天是星期一
        int day = cal.get(Calendar.DAY_OF_WEEK);//获得当前日期是一个星期的第几天

        cal.add(Calendar.DATE, cal.getFirstDayOfWeek() - day - 7);//根据日历的规则，给当前日期减去星期几与一个星期第一天的差值
        String firstDay = sdf.format(cal.getTime()) + " 00:00:00";
        sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); //设置时间格式
        Date result = null;
        try {
            result = sdf.parse(firstDay);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * @@Description 根据日期计算所在周的上一周的周日
     */
    public static Date getPreviousLastDayOfWeek(Date time) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        cal.setTime(time);
        //判断要计算的日期是否是周日，如果是则减一天计算周六的，否则会出问题，计算到下一周去了
        int dayWeek = cal.get(Calendar.DAY_OF_WEEK);
        if (1 == dayWeek) {
            cal.add(Calendar.DAY_OF_MONTH, -1);
        }
        cal.setFirstDayOfWeek(Calendar.MONDAY);
        int day = cal.get(Calendar.DAY_OF_WEEK);

        cal.add(Calendar.DATE, cal.getFirstDayOfWeek() - day - 1);
        String lastDay = sdf.format(cal.getTime()) + " 23:59:59";
        sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date result = null;
        try {
            result = sdf.parse(lastDay);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * @@Description 得到几天前的 最早的时间 YMD:00 00 00
     */
    public static Date getEarlyTimeOfDay(Date time, int day) {
        String firstDay = DateUtil.dateToYMD(DateUtil.getTheDate(time, day)) + " 00:00:00";
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date result = null;
        try {
            result = sdf.parse(firstDay);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * @@Description 得到几天前的 最迟的时间 YMD: 23:59:59
     */
    public static Date getLatestTimeOfDay(Date time, int day) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String firstDay = DateUtil.dateToYMD(DateUtil.getTheDate(time, day)) + " 23:59:59";
        Date result = null;
        try {
            result = sdf.parse(firstDay);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * @@Description 获取上个月的第一天  （yyyy-MM-dd HH:mm:ss）
     */
    public static Date getPreviousMonthFirstDay() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.MONTH, -1);
        int minDate = cal.getActualMinimum(Calendar.DATE);
        cal.set(Calendar.DATE, minDate);
        String firstDay = DateUtil.dateToYMD(cal.getTime()) + " 00:00:00";
        Date result = null;
        try {
            result = sdf.parse(firstDay);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * @@Description 获取上个月的最后一天（yyyy-MM-dd HH:mm:ss）
     */
    public static Date getPreviousMonthLastDay() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.MONTH, -1);
        int maxDate = cal.getActualMaximum(Calendar.DATE);

        cal.set(Calendar.DATE, maxDate);

        String lastDay = DateUtil.dateToYMD(cal.getTime()) + " 23:59:59";
        Date result = null;
        try {
            result = sdf.parse(lastDay);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * @@Description 根据日期计算所在周的周一
     */
    public static Date getCurrentFirstDayOfWeek(Date time) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        cal.setTime(time);
        //判断要计算的日期是否是周日，如果是则减一天计算周六的，否则会出问题，计算到下一周去了
        int dayWeek = cal.get(Calendar.DAY_OF_WEEK);
        if (1 == dayWeek) {
            cal.add(Calendar.DAY_OF_MONTH, -1);
        }
        cal.setFirstDayOfWeek(Calendar.MONDAY);
        int day = cal.get(Calendar.DAY_OF_WEEK);

        cal.add(Calendar.DATE, cal.getFirstDayOfWeek() - day);
        String firstDay = sdf.format(cal.getTime()) + " 00:00:00";
        sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date result = null;
        try {
            result = sdf.parse(firstDay);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * @@Description 得到某一年周的总数
     */
    public static int getMaxWeekNumOfYear(int year) {
        Calendar c = new GregorianCalendar();
        c.set(year, Calendar.DECEMBER, 31, 23, 59, 59);
        return getWeekOfYear(c.getTime());
    }

    /**
     * @@Description 取得当前日期是多少周
     */
    public static int getWeekOfYear(Date date) {
        Calendar c = new GregorianCalendar();
        c.setFirstDayOfWeek(Calendar.MONDAY);
        c.setMinimalDaysInFirstWeek(7);
        c.setTime(date);
        return c.get(Calendar.WEEK_OF_YEAR);
    }

    /**
     * @@Description 获取日期的年份
     */
    public static int getDateYear(Date date) {
        Calendar ca = Calendar.getInstance();
        ca.setTime(date);
        return ca.get(Calendar.YEAR);
    }

    /**
     * @@Description 获取周列表，最长20周 1:如果开始日期到当前日期的周数在一个年份里面，大于等于20周，则列最新的20周； 2：如果开始日期到当前日期的周数在一个年份里面，小于20周，则全部周列出来；
     * 3：如果开始日期到当前日期的周数不在一个年份里面，大于等于20周，列最新的20周； 4：如果开始日期到当前日期的周数不在一个年份里面，小于20周，全部列出来。
     */
    public static List<String> getWeeksList(Date startDate, int size) {
        List<String> list = new ArrayList<String>();
        Date currentDate = new Date();
        /*开始日期的年份*/
        int startYear = getDateYear(startDate);
        /*当前日期的年份*/
        int currentYear = getDateYear(currentDate);
        /*开始日期的周数*/
        int startWeek = getWeekOfYear(startDate);
        /*当前日期的周数*/
        int currentWeek = getWeekOfYear(currentDate);

        if (startYear == currentYear) {
            if (currentWeek - startWeek + 1 >= size) {
                for (int i = currentWeek; i >= currentWeek - 20; i--) {
                    list.add(currentYear + "年 第" + i + "周");
                }
            } else {
                for (int i = currentWeek; i >= startWeek; i--) {
                    list.add(currentYear + "年 第" + i + "周");
                }
            }
        } else {
            /*不在同个年份里面
             * 先看开始日期所在年份的周到年底的周数，再加上当前日期所在年份的周数
             * 如果大于20周取最新20周，否则全部
             * */
            //开始日期所在年份周的总数
            int startYearWeekNum = getMaxWeekNumOfYear(startYear);
            int weeksNum = startYearWeekNum - startWeek + 1 + currentWeek;//周总数
            if (weeksNum >= size) {
                for (int i = currentWeek; i >= 1; i--) {
                    list.add(currentYear + "年 第" + i + "周");
                }
                for (int i = startYearWeekNum; i > startYearWeekNum - size + currentWeek; i--) {
                    list.add(startYear + "年 第" + i + "周");
                }
            } else {
                for (int i = currentWeek; i >= 1; i--) {
                    list.add(currentYear + "年 第" + i + "周");
                }
                for (int i = startYearWeekNum; i >= startWeek; i--) {
                    list.add(startYear + "年 第" + i + "周");
                }
            }

        }
        return list;
    }

    /**
     * @@Description 得到某年某周的第一天
     */
    public static Date getFirstDayOfWeek(int year, int week) {
        Calendar c = new GregorianCalendar();
        c.set(Calendar.YEAR, year);
        c.set(Calendar.MONTH, Calendar.JANUARY);
        c.set(Calendar.DATE, 1);

        Calendar cal = (GregorianCalendar) c.clone();
        cal.add(Calendar.DATE, week * 7);

        return getFirstDayOfWeek(cal.getTime());
    }

    /**
     * @@Description 得到某年某周的最后一天
     */
    public static Date getLastDayOfWeek(int year, int week) {
        Calendar c = new GregorianCalendar();
        c.set(Calendar.YEAR, year);
        c.set(Calendar.MONTH, Calendar.JANUARY);
        c.set(Calendar.DATE, 1);

        Calendar cal = (GregorianCalendar) c.clone();
        cal.add(Calendar.DATE, week * 7);

        return getLastDayOfWeek(cal.getTime());
    }

    /**
     * @@Description 取得指定日期所在周的最后一天
     */
    public static Date getLastDayOfWeek(Date date) {
        Calendar c = new GregorianCalendar();
        c.setFirstDayOfWeek(Calendar.MONDAY);
        c.setTime(date);
        c.set(Calendar.DAY_OF_WEEK, c.getFirstDayOfWeek() + 6);
        return c.getTime();
    }

    /**
     * @@Description 取得指定日期所在周的第一天
     */
    public static Date getFirstDayOfWeek(Date date) {
        Calendar c = new GregorianCalendar();
        c.setFirstDayOfWeek(Calendar.MONDAY);
        c.setTime(date);
        c.set(Calendar.DAY_OF_WEEK, c.getFirstDayOfWeek());
        return c.getTime();
    }

    /**
     * @@Description 将时间对象转换为字符串 HH:mm
     */
    public static String getTimeFormatStr(Time time) {
        if (time == null) {
            return "";
        }
        String format = "HH:mm";
        Date d = new Date(time.getTime());
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.format(d);
    }

    /**
     * @@Description 将字符串转换为Time对象
     */
    public static Time getTimeByStr(String str) {
        if (str == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        String s = str.replaceAll("：", ":").replaceAll("[^0-9:]", "");
        try {
            Date d = sdf.parse(s);
            return new Time(d.getTime());
        } catch (ParseException e) {
            return null;
        }
    }

    /**
     * @@Description 获取当前日期后面days天的日期，只有年月日，没有时分秒
     */
    public static Date getDateAfter(int days) {
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DATE, +days);
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String dateString = formatter.format(c.getTime());
        Date date = null;
        try {
            date = formatter.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    /**
     * @@Description 获取某个日期后面days天的日期
     */
    public static Timestamp getTsAfter(Timestamp timestamp, int days) {
        timestamp.setDate(timestamp.getDate() + days);
        return timestamp;
    }

    /**
     * @@Description 获取当前日期的前days天，只有年月日，没有时分秒
     */
    public static Date getDateBefore(int days) {
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DATE, -days);
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String dateString = formatter.format(c.getTime());
        Date date = null;
        try {
            date = formatter.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    /**
     * @@Description 获取一个 yyyy/MM/dd hh:mm:ss 的两天后
     */
    public static String getTwoDaysAfterDate(String dateStr) {
        Calendar c = Calendar.getInstance();
        try {
            c.setTime(formater2.parse(dateStr));
            c.add(Calendar.DATE, +2);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return formater1.format(c.getTime());
    }

    /**
     * @@Description 获取一个 yyyy/MM/dd hh:mm:ss 的N天后
     */
    public static String getNDaysAfterDate(String dateStr, int n) {
        Calendar c = Calendar.getInstance();
        try {
            c.setTime(formater_yyyyMMdd.parse(dateStr));
            c.add(Calendar.DATE, +n);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return formater_yyyyMMdd.format(c.getTime());
    }

    /**
     * @@Description 获取一个 yyyy/MM/dd hh:mm:ss 的N天后
     */
    public static Date getNDaysAfterDate(Date date, int n) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.DATE, +n);
        return c.getTime();
    }

    /**
     * @@Description 获取一个 yyyy/MM/dd hh:mm:ss 的n天前
     */
    public static String getNDaysBeforeDate(String dateStr, int n) {
        Calendar c = Calendar.getInstance();
        try {
            c.setTime(formater_yyyyMMdd.parse(dateStr));
            c.add(Calendar.DATE, -n);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return formater_yyyyMMdd.format(c.getTime());
    }

    /**
     * @@Description 获取一个 yyyy/MM/dd hh:mm:ss 的n天前
     */
    public static Date getNDaysBeforeDate(Date date, int n) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.DATE, -n);
        return c.getTime();
    }

    /**
     * @@Description 例如 2013-1-1 -->20130101
     */
    public static long getTimeOfFormatDate(String dateString) throws ParseException {
        SimpleDateFormat formatOfDay = new SimpleDateFormat("yyyyMMdd");
        return formatOfDay.parse(dateString).getTime();
    }

    /**
     * @@Description 获取1个月前的那天的 Date
     */
    public static Date getOneMonthBeforeToday() {
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        c.add(Calendar.MONTH, -1);
        return c.getTime();
    }


    /**
     * @@Description 获取date的1个月后的那天
     */
    public static Date getOneMonthAfterToday(Date date) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.MONTH, 1);
        return c.getTime();
    }

    /**
     * @@Description 获取date的n个月后的那天
     */
    public static Date getNMonthAfterToday(Date date, int month) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.MONTH, month);
        return c.getTime();
    }

    /**
     * @@Description 获取date的1年后的那天
     */
    public static Date getOneYearAfterToday(Date date) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.YEAR, 1);
        return c.getTime();
    }

    /**
     * @@Description 返回一天的第一秒
     */
    public static String getFirstSecondStrOfDate(Date date) {
        String returnDateStr = formater_yyyyMMdd.format(date);
        return returnDateStr + " 00:00:00";
    }

    /**
     * @@Description 返回一天的第一秒
     */
    public static Date getFirstSecondOfDate(Date date) {
        String returnDateStr = formater_yyyyMMdd.format(date);
        Date returnDate = null;
        try {
            returnDate = formater2.parse(returnDateStr + " 00:00:00");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return returnDate;
    }

    /**
     * @@Description 返回这一天的最后一秒
     */
    public static String getLastSecondStrOfDate(Date date) {
        String returnDateStr = formater_yyyyMMdd.format(date);
        return returnDateStr + " 23:59:59";
    }

    /**
     * @@Description 返回这一天的最后一秒
     */
    public static Date getLastSecondOfDate(Date date) {
        String returnDateStr = formater_yyyyMMdd.format(date);
        Date returnDate = null;
        try {
            returnDate = formater2.parse(returnDateStr + " 23:59:59");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return returnDate;
    }

    /**
     * @@Description 两个时间在是同一天
     */
    public static boolean isTheSameDate(Timestamp date1, Timestamp date2) {
        String date1Str = formater_yyyyMMdd.format(date1);
        String date2Str = formater_yyyyMMdd.format(date2);
        if (date1Str.equals(date2Str)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @@Description 以给定的格式获取一个Date对应的日期部分 也就是 时分秒都是0
     */
    public static Date getDate(Date date, SimpleDateFormat formater) throws ParseException {
        return formater.parse(formater.format(date));
    }

    /**
     * @@Description yyyy-MM-dd格式获取一个Date对应的日期部分 也就是 时分秒都是0
     */
    public static Date getDate(Date date) {
        Date d = null;
        try {
            d = getDate(date, formater_yyyyMMdd);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return d;
    }

    /**
     * @@Description yyyy-MM-dd格式获取当前日期对应的日期部分 也就是 时分秒都是0
     */
    public static Date getDate() {
        return getDate(new Date());
    }

    /**
     * @@Description 判断d1是否比d2早 false
     */
    public static Boolean isBefore(Date d1, Date d2) {
        return d1.getTime() < d2.getTime();
    }

    /**
     * @@Description 计算用于显示的 当前日期到下限时间的天数
     */
    public static String getTimeRemain(Date offTime) {
        if (offTime == null) {
            return "暂未确定";
        }
        String offTimeStr = null;
        try {
            int remainTime = dateDiff2(offTime, DateUtil.formater_yyyyMMdd.parse(DateUtil.getToday()));
            if (remainTime < 0) {
                remainTime = 0;
            }
            offTimeStr = "" + remainTime;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return offTimeStr;
    }

    public static Date getLastMonthFirstDay() {
        Calendar now = Calendar.getInstance();
        now.set(Calendar.MONTH, now.get(Calendar.MONTH) - 1);
        now.set(Calendar.DAY_OF_MONTH, 1);
        now.set(Calendar.HOUR_OF_DAY, 0);
        now.set(Calendar.MINUTE, 0);
        now.set(Calendar.SECOND, 0);
        now.set(Calendar.MILLISECOND, 0);
        return now.getTime();
    }

    /**
     * todo YYYY-MM-DD 23:59:59.999这种格式的，精确到当天的最后一个毫秒，如果数据库中存储该字段采用的是Date类型，日期会加1，时分秒会自动去掉，慎用
     * @return
     */
    public static Date getLastMonthLastDay() {
        Calendar now = Calendar.getInstance();
        now.set(Calendar.DAY_OF_MONTH, 0);
        now.set(Calendar.HOUR_OF_DAY, 23);
        now.set(Calendar.MINUTE, 59);
        now.set(Calendar.SECOND, 59);
        now.set(Calendar.MILLISECOND, 999);
        return now.getTime();
    }

     /*

    获取当前时间之前或之后几小时 hour

   */

    public static Date getTimeByHour(int hour) {

        Calendar calendar = Calendar.getInstance();

        calendar.set(Calendar.HOUR_OF_DAY, calendar.get(Calendar.HOUR_OF_DAY) + hour);

        return new Date(calendar.getTimeInMillis());

    }

    /*

    获取当前时间之前或之后几分钟 minute

    */

    public static Date getTimeByMinute(int minute) {

        Calendar calendar = Calendar.getInstance();

        calendar.add(Calendar.MINUTE, minute);

        return new Date(calendar.getTimeInMillis());

    }

    /*

    获取当前时间之前或之后几秒钟 SECONDS

    */

    public static Date getTimeBySecond(int second) {

        Calendar calendar = Calendar.getInstance();

        calendar.add(Calendar.SECOND, second);

        return new Date(calendar.getTimeInMillis());

    }

    public static Date getStartTimeForQuery(String yyyyMMddStr) {
        if (Strings.isNullOrEmpty(yyyyMMddStr)) {
            return null;
        }
        return new DateTime(DateUtil.stringToDate(yyyyMMddStr, format_yyyyMMdd)).millisOfDay().withMinimumValue()
                .toDate();
    }

    public static Date getEndTimeForQuery(String yyyyMMddStr) {
        if (Strings.isNullOrEmpty(yyyyMMddStr)) {
            return null;
        }
        return new DateTime(DateUtil.stringToDate(yyyyMMddStr, format_yyyyMMdd)).millisOfDay().withMaximumValue()
                .toDate();
    }

    /**
     * 2      * 把long 转换成 日期 再转换成String类型 3
     */
    public static String transferLongToDate(String dateFormat, Long millSec) {
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
        Date date = new Date(millSec);
        return sdf.format(date);
    }

    /**
     * @@Description 获取当前月的最后一秒
     */
    public static Date getLastSecondOfCurMonth() {
        Calendar ca = Calendar.getInstance();
        ca.set(Calendar.DAY_OF_MONTH, ca.getActualMaximum(Calendar.DAY_OF_MONTH));
        String last = formater_yyyyMMdd.format(ca.getTime());
        Date returnDate = null;
        try {
            returnDate = formater2.parse(last + " 23:59:59");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return returnDate;
    }

    /**
     * @@Description 获取当月的第一天  （yyyy-MM-dd HH:mm:ss）
     */
    public static Date getCurrentMonthFirstDay() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.MONTH, 0);
        int minDate = cal.getActualMinimum(Calendar.DATE);
        cal.set(Calendar.DATE, minDate);
        String firstDay = DateUtil.dateToYMD(cal.getTime()) + " 00:00:00";
        Date result = null;
        try {
            result = sdf.parse(firstDay);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String toyyyyMMddHHmmss(Date date) {
        return new DateTime(date).toString("yyyyMMddHHmmss");
    }

    /**
     * 两个时间点间隔天数，不足一天为0, 如果start在end之后，返回为负数
     *
     * @param start
     * @param end
     * @return
     */
    public static long daysBetween(Date start, Date end) {
        if (start.after(end)) {
            return -daysBetween(end, start);
        }
        Interval interval = new Interval(start.getTime(), end.getTime());
        return interval.toDuration().getStandardDays();
    }
    /**
     * 获取上个月的最后一秒
     * @return
     */
    public static Date getLastSecondOfLastMonth() {
        Calendar ca = Calendar.getInstance();
        ca.set(Calendar.DAY_OF_MONTH, 0);
        String last = formater_yyyyMMdd.format(ca.getTime());
        Date returnDate = null;
        try {
            returnDate = formater2.parse(last + " 23:59:59");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return returnDate;
    }

    /**
     * @@Description 获取当前日期的前month月的日期，只有年月日，没有时分秒
     */
    public static Date getDateBeforeMonth(int month) {
        Calendar c = Calendar.getInstance();
        c.add(Calendar.MONTH, -month);
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String dateString = formatter.format(c.getTime());
        Date date = null;
        try {
            date = formatter.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }
}
