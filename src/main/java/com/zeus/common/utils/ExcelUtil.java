package com.zeus.common.utils;

import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import jxl.CellType;
import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.format.VerticalAlignment;
import jxl.write.Label;
import jxl.write.NumberFormat;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.util.CollectionUtils;

/**
 * @author yishan@dian.so
 * @version 1.0.0
 * @data 2019/3/18
 */
public class ExcelUtil {

    /**
     * 导出Excel
     * @param sheetName sheet名称
     * @param title 标题
     * @param values 内容
     * @param wb HSSFWorkbook对象
     * @return
     */
    public static HSSFWorkbook getHSSFWorkbook(String sheetName,String []title,String [][]values, HSSFWorkbook wb){

        // 第一步，创建一个HSSFWorkbook，对应一个Excel文件
        if(wb == null){
            wb = new HSSFWorkbook();
        }

        // 第二步，在workbook中添加一个sheet,对应Excel文件中的sheet
        HSSFSheet sheet = wb.createSheet(sheetName);

        // 第三步，在sheet中添加表头第0行,注意老版本poi对Excel的行数列数有限制
        HSSFRow row = sheet.createRow(0);

        // 第四步，创建单元格，并设置值表头 设置表头居中
        HSSFCellStyle style = wb.createCellStyle();
         // 创建一个居中格式

        //声明列对象
        HSSFCell cell = null;

        //创建标题
        for(int i=0;i<title.length;i++){
            cell = row.createCell(i);
            cell.setCellValue(title[i]);
            cell.setCellStyle(style);
        }

        //创建内容
        if(values != null) {
            for(int i=0;i<values.length;i++){
                row = sheet.createRow(i + 1);
                for(int j=0;j<values[i].length;j++){
                    //将内容按顺序赋给对应的列对象
                    row.createCell(j).setCellValue(values[i][j]);
                }
            }
        }
        return wb;
    }
    public static void createExcel(String url,String fileName,Map<Integer,String> labels) {
        if(CollectionUtils.isEmpty(labels)) {
            return;
        }
        try {
            // 打开文件
            Workbook wb = Workbook.getWorkbook(new File(url,fileName+".xls")); // 获得原始文档
            WritableWorkbook book = Workbook.createWorkbook(new File(url,fileName+"copy.xls"),wb);

            // 生成名为“sheet1”的工作表，参数0表示这是第一页
            WritableSheet sheet = book.getSheet(0);
            sheet.setColumnView(10,30);
            // 在Label对象的构造子中指名单元格位置是第一列第一行(0,0),单元格内容为string
            Label label = new Label(10, 0, "错误信息");
            // 将定义好的单元格添加到工作表中
            sheet.addCell(label);
            for(Entry<Integer,String> entry :labels.entrySet()) {
                Label label1 = new Label(10, entry.getKey(), entry.getValue(),
                        getDataCellFormat(CellType.LABEL));
                sheet.addCell(label1);
            }
            // 写入数据并关闭文件
            book.write();
            book.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }
    public static WritableCellFormat getDataCellFormat(CellType type) {
        WritableCellFormat wcf = null;
        try {
            // 字体样式
            if (type == CellType.NUMBER || type == CellType.NUMBER_FORMULA) {// 数字
                NumberFormat nf = new NumberFormat("#.00");
                wcf = new WritableCellFormat(nf);
            } else {
                WritableFont wf = new WritableFont(WritableFont.TIMES, 10,
                        WritableFont.NO_BOLD, false);
                // 字体颜色
                wf.setColour(Colour.BLACK);
                wcf = new WritableCellFormat(wf);
            }
            // 对齐方式
            wcf.setAlignment(Alignment.CENTRE);
            wcf.setVerticalAlignment(VerticalAlignment.CENTRE);
            // 设置上边框
            wcf.setBorder(Border.TOP, BorderLineStyle.THIN);
            // 设置下边框
            wcf.setBorder(Border.BOTTOM, BorderLineStyle.THIN);
            // 设置左边框
            wcf.setBorder(Border.LEFT, BorderLineStyle.THIN);
            // 设置右边框
            wcf.setBorder(Border.RIGHT, BorderLineStyle.THIN);

            // 设置背景色
            wcf.setBackground(Colour.RED);

            // 自动换行
            wcf.setWrap(true);

        } catch (WriteException e) {
            e.printStackTrace();
        }
        return wcf;
    }

}
