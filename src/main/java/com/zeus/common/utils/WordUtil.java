package com.zeus.common.utils;


import lombok.extern.slf4j.Slf4j;
import org.apache.commons.csv.CSVFormat;
import org.apache.poi.hwpf.HWPFDocument;
import org.apache.poi.hwpf.usermodel.*;
import org.apache.poi.ooxml.POIXMLDocument;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.xwpf.usermodel.*;
import org.apache.xmlbeans.XmlOptions;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTBody;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URL;
import java.net.URLDecoder;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Slf4j
public class WordUtil {


    public static void main(String[] args) throws FileNotFoundException {
        Map map = new HashMap();
        map.put("name", "小明");
        map.put("mobile", "18842689096");
        map.put("house", "西城时代");
        map.put("area", "129");
        map.put("result", "赞成");
        map.put("num", "42432432");
        getBuild("static/doc/业主大会会议表决票样.doc", map, "aa.doc");
    }


    public static void getBuild(String tmpFile, Map<String, String> contentMap, String fileName, HttpServletResponse response) {

        InputStream inputStream = WordUtil.class.getClassLoader().getResourceAsStream(tmpFile);
//        InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(tmpFile);
        HWPFDocument document = null;
        try {
            document = new HWPFDocument(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
        // 读取文本内容
        Range bodyRange = document.getRange();
        // 替换内容
        for (Map.Entry<String, String> entry : contentMap.entrySet()) {
            bodyRange.replaceText("${" + entry.getKey() + "}", entry.getValue());
        }

        //导出到文件
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            document.write(byteArrayOutputStream);
            response.setContentType(response.getContentType());
            fileName = fileName.replaceAll(";", " ");
            response.setHeader("Content-Disposition",
                    "attachment;filename=" + new String(fileName.getBytes("UTF8"), "ISO8859-1") + ".doc");
            CSVFormat format = CSVFormat.DEFAULT.withRecordSeparator("\n");
            byte[] bytes = byteArrayOutputStream.toByteArray();
            response.setHeader("Content-Length", String.valueOf(bytes.length));
            BufferedOutputStream bos = null;
            bos = new BufferedOutputStream(response.getOutputStream());
            bos.write(bytes);
            bos.close();
            byteArrayOutputStream.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void getBuilds(String tmpFile, List<Map<String, String>> contentMaps, String fileName, HttpServletResponse response) throws Exception {

//        String url = URLDecoder.decode(WordUtil.class.getClassLoader().getResource(tmpFile).toString());
        List<XWPFDocument> xwpfDocuments = new ArrayList<XWPFDocument>();
        for (Map<String, String> contentMap : contentMaps) {
            XWPFDocument xwpfDocument = generateWord(contentMap, tmpFile);
            xwpfDocuments.add(xwpfDocument);
        }

        XWPFDocument xwpfDocument = xwpfDocuments.get(0);
        for (int i = 0; i < xwpfDocuments.size(); i++) {
            //每次的追加为了避免样式和格式混乱 加上分页符
            //当是只有一条数据的时候 直接输出
            if (i == 0) {
                xwpfDocument = xwpfDocuments.get(0);
                continue;
            } else {
                //当存在多条时候
                xwpfDocument = mergeWord(xwpfDocument, xwpfDocuments.get(i));
            }
        }


        //导出到文件
        try {
            BufferedOutputStream bos = null;
//            for(HWPFDocument document1 : documents) {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            xwpfDocument.write(byteArrayOutputStream);
//                byte[] bytes = byteArrayOutputStream.toByteArray();
//
//            }
            response.setContentType(response.getContentType());
            fileName = fileName.replaceAll(";", " ");
            response.setHeader("Content-Disposition",
                    "attachment;filename=" + new String(fileName.getBytes("UTF8"), "ISO8859-1") + ".doc");
            CSVFormat format = CSVFormat.DEFAULT.withRecordSeparator("\n");
            byte[] bytes = byteArrayOutputStream.toByteArray();
            response.setHeader("Content-Length", String.valueOf(bytes.length));

            bos = new BufferedOutputStream(response.getOutputStream());
            bos.write(bytes);
            bos.close();
//            byteArrayOutputStream.close();


        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static XWPFDocument mergeWord(XWPFDocument document, XWPFDocument doucDocument2) throws Exception {
        XWPFDocument src1Document = document;
        XWPFParagraph p = src1Document.createParagraph();
        //设置分页符
        p.setPageBreak(true);
        CTBody src1Body = src1Document.getDocument().getBody();
        XWPFDocument src2Document = doucDocument2;
        CTBody src2Body = src2Document.getDocument().getBody();
        XWPFParagraph p2 = src2Document.createParagraph();
        XmlOptions optionsOuter = new XmlOptions();
        optionsOuter.setSaveOuter();
        String appendString = src2Body.xmlText(optionsOuter);
        String srcString = src1Body.xmlText();
        String prefix = srcString.substring(0, srcString.indexOf(">") + 1);
        String mainPart = srcString.substring(srcString.indexOf(">") + 1, srcString.lastIndexOf("<"));
        String sufix = srcString.substring(srcString.lastIndexOf("<"));
        String addPart = appendString.substring(appendString.indexOf(">") + 1, appendString.lastIndexOf("<"));
        CTBody makeBody = CTBody.Factory.parse(prefix + mainPart + addPart + sufix);
        src1Body.set(makeBody);
        return src1Document;
    }


    public static void getBuild(String tmpFile, Map<String, String> contentMap, String exportFile) {

        InputStream inputStream = WordUtil.class.getClassLoader().getResourceAsStream(tmpFile);
//        InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(tmpFile);
        HWPFDocument document = null;
        try {
            document = new HWPFDocument(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
        // 读取文本内容
        Range bodyRange = document.getRange();
        // 替换内容
        for (Map.Entry<String, String> entry : contentMap.entrySet()) {
            bodyRange.replaceText("${" + entry.getKey() + "}", entry.getValue());
        }

        //导出到文件
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            document.write(byteArrayOutputStream);
            OutputStream outputStream = new FileOutputStream(exportFile);
            outputStream.write(byteArrayOutputStream.toByteArray());
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * 替换word占位符的内容 避免了不解析的情况出现
     *
     * @param param
     * @param filePath
     * @return
     */
    public static XWPFDocument generateWord(Map<String, String> param, String filePath) {
        XWPFDocument doc = null;
        try {
            OPCPackage pack = POIXMLDocument.openPackage(filePath);
            doc = new XWPFDocument(pack);
            replaceInPara(doc, param);    //替换文本里面的变量
            replaceInTable(doc, param); //替换表格里面的变量
            return doc;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return doc;
    }

    /**
     * 替换表格里面的变量
     *
     * @param doc    要替换的文档
     * @param params 参数
     */
    private static void replaceInTable(XWPFDocument doc, Map<String, String> params) {
        Iterator<XWPFTable> iterator = doc.getTablesIterator();
        XWPFTable table;
        List<XWPFTableRow> rows;
        List<XWPFTableCell> cells;
        List<XWPFParagraph> paras;
        while (iterator.hasNext()) {
            table = iterator.next();
            if (table.getRows().size() > 1) {
                //判断表格是需要替换还是需要插入，判断逻辑有$为替换，表格无$为插入
                if (matcher(table.getText()).find()) {
                    rows = table.getRows();
                    for (XWPFTableRow row : rows) {
                        cells = row.getTableCells();
                        for (XWPFTableCell cell : cells) {
                            paras = cell.getParagraphs();
                            for (XWPFParagraph para : paras) {
                                replaceInPara(para, params);
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * 正则匹配字符串
     *
     * @param str
     * @return
     */
    private static Matcher matcher(String str) {
        Pattern pattern = Pattern.compile("\\$\\{(.+?)\\}", Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(str);
        return matcher;
    }

    /**
     * 替换段落里面的变量
     *
     * @param doc    要替换的文档
     * @param params 参数
     */
    private static void replaceInPara(XWPFDocument doc, Map<String, String> params) {
        if (params != null && params.size() > 0) {

            List<XWPFParagraph> paragraphList = doc.getParagraphs();
            if (paragraphList != null && paragraphList.size() > 0) {
                for (XWPFParagraph paragraph : paragraphList) {
                    replaceInPara(paragraph,params);
                }
            }
        }
    }

    /**
     * 替换段落里面的变量
     *
     * @param para   要替换的段落
     * @param params 参数
     */
    private static void replaceInPara(XWPFParagraph para, Map<String, String> params) {
        //处理段落
        String tempString = "";
        Set<XWPFRun> runSet = new HashSet<>();
        char lastChar = ' ';
        List<XWPFRun> runs = para.getRuns();
        for (XWPFRun run : runs) {
            String text = run.getText(0);
            if (text == null) continue;
            text = replaceText(text, params);
            run.setText("", 0);
            run.setText(text, 0);
            for (int i = 0; i < text.length(); i++) {
                char ch = text.charAt(i);
                if (ch == '$') {
                    runSet = new HashSet<>();
                    runSet.add(run);
                    tempString = text;
                } else if (ch == '{') {

                    if (lastChar == '$') {
                        if (runSet.contains(run)) {

                        } else {
                            runSet.add(run);
                            tempString = tempString + text;
                        }
                    } else {
                        runSet = new HashSet<>();
                        tempString = "";
                    }
                } else if (ch == '}') {

                    if (tempString != null && tempString.indexOf("${") >= 0) {
                        if (runSet.contains(run)) {

                        } else {
                            runSet.add(run);
                            tempString = tempString + text;
                        }
                    } else {
                        runSet = new HashSet<>();
                        tempString = "";
                    }
                    if (runSet.size() > 0) {
                        String replaceText = replaceText(tempString, params);
                        if (!replaceText.equals(tempString)) {
                            int index = 0;
                            XWPFRun aRun = null;
                            for (XWPFRun tempRun : runSet) {
                                tempRun.setText("", 0);
                                if (index == 0) {
                                    aRun = tempRun;
                                }
                                index++;
                            }
                            aRun.setText(replaceText, 0);
                        }
                        runSet = new HashSet<>();
                        tempString = "";
                    }
                } else {
                    if (runSet.size() <= 0) continue;
                    if (runSet.contains(run)) continue;
                    runSet.add(run);
                    tempString = tempString + text;
                }
                lastChar = ch;
            }

        }
    }


    private static String replaceText(String text, Map<String, String> map) {
        if (text != null) {
            for (Map.Entry<String, String> entry : map.entrySet()) {
                String key = entry.getKey();
                if (text.indexOf(key) != -1) {
                    Object value = entry.getValue();
                    if (value instanceof String) {//文本替换
                        text = text.replace(key, value.toString());
                    }
                }
            }
        }
        return text;
    }

}
