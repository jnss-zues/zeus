package com.zeus.common.utils;

import javax.servlet.http.HttpServletRequest;
import java.io.*;

public class UploadFileUtil
{  
      
    public static void main(String[] args)  
    {  
    	long l = System.currentTimeMillis();
//        file2BetyArray();  
//        fileToBetyArray();  
        System.out.println(System.currentTimeMillis() - l);
    }  
  
    public static byte[] file2BetyArray()  
    {  
        File file = new File("C:\\Users\\Administrator\\Desktop\\szcport.zip");  
        if (!file.exists()) {  
            return null;  
        }  
        FileInputStream stream = null;  
        ByteArrayOutputStream out = null;  
        try {  
            stream = new FileInputStream(file);  
            out = new ByteArrayOutputStream(1000);  
            byte[] b = new byte[1000];  
            int n;  
            while ((n = stream.read(b)) != -1) {  
                out.write(b, 0, n);  
            }  
            return out.toByteArray();// 此方法大文件OutOfMemory  
        } catch (Exception e) {  
            System.out.println(e.toString());  
        } finally {  
            try {  
                stream.close();  
                out.close();  
            } catch (IOException e) {  
                e.printStackTrace();  
            }  
  
        }  
        return null;  
    }  
  
    public static byte[] fileToBetyArray(File file)  
    {  
        FileInputStream fileInputStream = null;  
        byte[] bFile = null;  
        try {  
            bFile = new byte[(int) file.length()];  
            fileInputStream = new FileInputStream(file);  
            fileInputStream.read(bFile);  
            fileInputStream.close();  
            System.out.println("Done");  
        } catch (Exception e) {  
            e.printStackTrace();  
        } finally {  
            try {  
                fileInputStream.close();  
                bFile.clone();  
            } catch (IOException e) {  
                e.printStackTrace();  
            }  
        }  
        return bFile;  
    } 
    public static String fileName2UTF8(HttpServletRequest request,String realName) throws UnsupportedEncodingException{
    	String agent = request.getHeader("USER-AGENT");    
		String downLoadName = null;  
		if (null != agent && -1 != agent.indexOf("MSIE"))   //IE  
		{    
		  downLoadName = java.net.URLEncoder.encode(realName, "UTF-8");   
		}    
		  else if (null != agent && -1 != agent.indexOf("Mozilla")) //Firefox  
		{        
		  downLoadName = new String(realName.getBytes("UTF-8"),"iso-8859-1");     
		}    
		else     
		{  
		  downLoadName = java.net.URLEncoder.encode(realName, "UTF-8");   
		}
		return downLoadName;    
    }
}