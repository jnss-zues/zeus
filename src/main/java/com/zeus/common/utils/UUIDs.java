package com.zeus.common.utils;

import java.util.UUID;

/**
 * 
 * @author: zhuweimin
 * @Date:2017年11月19日下午10:18:40
 * @Description:uuid 生成
 * @ClassName:UUID
 */
public class UUIDs {
	 public UUIDs() {}  
public static void main(String[] args) {
	
	String uuid = UUIDs.getUUID();
	System.out.println(uuid);
}	    
	  /**
	   * 随机生成UUID
	   * @return
	   */
	  public static synchronized String getUUID(){
	    UUID uuid=UUID.randomUUID();
	    String str = uuid.toString(); 
	    String uuidStr=str.replace("-", "");
	    return uuidStr;
	  }
	  
	  /**
	   * 根据字符串生成固定UUID
	   * @param name
	   * @return
	   */
	  public static synchronized String getUUID(String name){
	    UUID uuid=UUID.nameUUIDFromBytes(name.getBytes());
	    String str = uuid.toString(); 
	    String uuidStr=str.replace("-", "");
	    return uuidStr;
	  }
	  
	   
}
