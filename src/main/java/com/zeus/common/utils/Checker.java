package com.zeus.common.utils;

import com.alibaba.fastjson.JSON;
import java.util.Collection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.lang3.StringUtils;

/**
 * @author yishan@dian.so
 * @version 1.0.0
 * @data 2019/3/20
 */
public class Checker {

    private static final Pattern PATTERN_MOBILE = Pattern.compile("^[1][3,4,5,6,7,8,9][0-9]{9}$");
    private static final Pattern PATTERN_URI_FOR_WEB = Pattern.compile("^http://[0-9a-zA-Z_/\\.%-]+\\??[0-9a-zA-Z_=%&-]*$|^https://[0-9a-zA-Z_/\\.%]+\\??[0-9a-zA-Z_=%&-]*$|^[0-9a-zA-Z_/\\.%]+\\??[0-9a-zA-Z_=%&-]*$");
    private static final Pattern PATTERN_URI_FOR_APP = Pattern.compile("^imlifer://[0-9a-zA-Z_/\\.%]+\\??[0-9a-zA-Z_=%&]*$");
    private static final Pattern PATTERN_COLOR_RGB_HEX = Pattern.compile("^#[0-9A-Fa-f]{6}$");
    private static final Pattern PATTERN_CLIENT_VERSION = Pattern.compile("^[0-9]+\\.[0-9]+\\.[0-9]+$");

    public Checker() {
    }

    public static boolean isEmpty(String str) {
        return StringUtils.isEmpty(str);
    }

    public static boolean isBlank(String str) {
        return StringUtils.isBlank(str);
    }

    public static boolean isEmpty(Collection collection) {
        return collection == null || collection.isEmpty();
    }

    public static boolean isNull(Object obj) {
        return obj == null;
    }

    public static boolean inArray(Object target, Object... array) {
        if (target != null && array != null && array.length > 0) {
            Object[] var2 = array;
            int var3 = array.length;

            for(int var4 = 0; var4 < var3; ++var4) {
                Object obj = var2[var4];
                if (target.equals(obj)) {
                    return true;
                }
            }

            return false;
        } else {
            return false;
        }
    }

    public static boolean inRange(int target, Integer min, Integer max) {
        if (min == null && max == null) {
            return false;
        } else if (min != null && target < min.intValue()) {
            return false;
        } else {
            return max == null || target <= max.intValue();
        }
    }

    public static boolean inRange(long target, Long min, Integer max) {
        if (min == null && max == null) {
            return false;
        } else if (min != null && target < min.longValue()) {
            return false;
        } else {
            return max == null || target <= (long)max.intValue();
        }
    }

    public static boolean isMobile(String mobile) {
        if (isBlank(mobile)) {
            return false;
        } else {
            Matcher m = PATTERN_MOBILE.matcher(mobile);
            return m.matches();
        }
    }

    public static boolean isPositive(Number n) {
        isNull(n);
        long l = 0L;
        if (n instanceof Short) {
            l = (long)n.shortValue();
        } else if (n instanceof Integer) {
            l = (long)n.intValue();
        } else if (n instanceof Long) {
            l = n.longValue();
        }

        return l >= 1L;
    }

    public static boolean isUri4Web(String uri) {
        if (isBlank(uri)) {
            return false;
        } else {
            Matcher m = PATTERN_URI_FOR_WEB.matcher(uri);
            return m.matches();
        }
    }

    public static boolean isUri4App(String uri) {
        if (isBlank(uri)) {
            return false;
        } else {
            Matcher m = PATTERN_URI_FOR_APP.matcher(uri);
            return m.matches();
        }
    }

    public static boolean isColorRGBHex(String color) {
        if (isBlank(color)) {
            return false;
        } else {
            Matcher m = PATTERN_COLOR_RGB_HEX.matcher(color);
            return m.matches();
        }
    }

    public static boolean isJSONString(String jsonString) {
        try {
            if (isBlank(jsonString)) {
                return false;
            } else {
                JSON.parseObject(jsonString);
                return true;
            }
        } catch (Exception var2) {
            return false;
        }
    }

    public static boolean isContainAny(String str, String any) {
        return StringUtils.containsAny(str, any);
    }

    public static boolean isClientVersion(String clientVersion) {
        if (isBlank(clientVersion)) {
            return false;
        } else {
            Matcher m = PATTERN_CLIENT_VERSION.matcher(clientVersion);
            return m.matches();
        }
    }
}
