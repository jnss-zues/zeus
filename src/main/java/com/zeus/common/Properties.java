package com.zeus.common;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class Properties {

    //微信API根节点
    @Value("${weixin_api_host}")
    public String weixinApiHost;

    //appid
    @Value("${appId}")
    public String appId;

    //获取openId
    @Value("${get_openId}")
    public String getOpenId;

    //获取secret
    @Value("${secret}")
    public String getSecret;

    //获取grantType
    @Value("${grant_type}")
    public String getGrantType;

    //获取grantType
    @Value("${upload_path}")
    public String uploadPath;

    @Value("${upload_imgs_path}")
    public String uploadImgsPath;


    //获取上传二维码目录
    @Value("${upload_path_grcode_vote}")
    public String uploadPathGrcodeVote;

    @Value("${upload_path_grcode_notice}")
    public String uploadPathGrcodeNotice;

    //投票分享的url
    @Value("${vote_share_url}")
    public String voteShareUrl;

    @Value("${notice_share_url}")
    public String noticeShareUrl;

    @Value("${login_url}")
    public String loginUrl;

    @Value("redirectUri")
    public String redirectUri;

    @Value("vote_status_timer")
    public String voteStatusTimer;

    @Value("${common_code}")
    public Integer commonCode;

}
