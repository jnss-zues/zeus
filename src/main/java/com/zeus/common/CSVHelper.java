package com.zeus.common;

import com.google.common.base.Function;
import com.google.common.collect.Lists;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * CVS 格式文件导入导出工具类
 *
 * @author will
 * @since 2015-09-16
 */
public class CSVHelper {

    private static final Logger logger = LoggerFactory.getLogger(CSVHelper.class);

    private static FileWriter fileWriter = null;
    private static CSVPrinter csvPrinter = null;
    private final static CSVFormat csvFormat = CSVFormat.DEFAULT.withRecordSeparator("\n");
    private final static String commonCsvHead = new String(new byte[] { (byte) 0xEF, (byte) 0xBB,(byte) 0xBF });

    public static <F> boolean exportToHttpResponse(List titles,
                                                      List<F> records,
                                                      Function<F, Iterable> recordTransfer,
                                                      String fileName,
                                                      HttpServletResponse response) {
        try {
            response.setContentType("application/octet-stream");
            response.setCharacterEncoding("GB18030");
            response.setHeader("Transfer-Encoding", "chuncked");
            response.setHeader("Content-Disposition",
                    "attachment;filename=\"" + new String(fileName.getBytes("UTF8"), "ISO8859-1") + ".csv\"");
            CSVFormat format = CSVFormat.DEFAULT.withRecordSeparator("\n");
            export(format, response.getWriter(), titles, records, recordTransfer);
            response.getWriter().flush();
            return true;
        } catch (Exception e) {
            logger.error("[ExportToHttpResponseFailed]", e);
            return false;
        }
    }

    public static <F> void export(CSVFormat format,
                               Writer writer,
                               List titles,
                               List<F> records,
                               Function<F, Iterable> recordTransfer) throws IOException {
        CSVPrinter printer = new CSVPrinter(writer, format);
        printer.printRecord(titles.toArray());
        for (F record : records) {
            Iterable iterable = recordTransfer.apply(record);
            if (iterable != null) {
                printer.printRecord(iterable);
            }
        }
    }

    public static boolean export(List<String> titles, List<String[]> dataList, String filePath) {
        if (!StringUtil.isNotBlank(filePath)) {
            return false;
        }
        if (dataList.isEmpty()) {
            return false;
        }
        try {
            File file = new File(filePath + ".csv");
            if (file.exists()) {
                file.delete();
            }
            fileWriter = new FileWriter(filePath + ".csv",true);
            fileWriter.write(commonCsvHead);
            csvPrinter = new CSVPrinter(fileWriter, csvFormat);
            csvPrinter.printRecord(titles.toArray());
            for (String[] data : dataList) {
                csvPrinter.printRecord(data);
            }
            return true;
        } catch (IOException e) {
            logger.error("csv导出异常", e.getCause());
        } finally {
            try {
                csvPrinter.flush();
                fileWriter.flush();
                fileWriter.close();
                csvPrinter.close();
            }catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }
    /**
     * sql result export csv
     * @param dataList
     * @param filePath
     * @return
     */
    public static boolean export(List<LinkedHashMap> dataList, String filePath) {
        if (!StringUtil.isNotBlank(filePath)) {
            return false;
        }
        if (dataList.isEmpty() || dataList.size() == 0) {
            return false;
        }
        try {
            File file = new File(filePath + ".csv");
            if (file.exists()) {
                file.delete();
            }
            fileWriter = new FileWriter(filePath + ".csv", true);
            fileWriter.write(commonCsvHead);
            csvPrinter = new CSVPrinter(fileWriter, csvFormat);
            csvPrinter.printRecord(dataList.get(0).keySet());
            for (LinkedHashMap data : dataList) {
                csvPrinter.printRecord(data.values());
            }
            return true;
        } catch (IOException e) {
            logger.error("csv导出异常", e.getCause());
        } finally {
            try {
                csvPrinter.flush();
                fileWriter.flush();
                fileWriter.close();
                csvPrinter.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }


    /**
     * download
     * @param response
     * @param fileRealPath
     */
    public static void download(HttpServletResponse response, String fileRealPath) {
        String fileName = new File(fileRealPath).getName();
        try {
            fileName = new String(fileName.getBytes("GBK"), "ISO8859_1");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        response.setContentType("multipart/form-data");
        response.setHeader("Content-Disposition", "attachment; filename="
                + fileName);

        byte[] b = new byte[1024];
        int len = 0;

        FileInputStream fis = null;
        ServletOutputStream sos = null;
        try {
            fis = new FileInputStream(fileRealPath);
            sos = response.getOutputStream();
            while ((len = fis.read(b)) != -1) {
                sos.write(b, 0, len);
            }
            sos.flush();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (fis != null) {
                    fis.close();
                }
                if (sos != null) {
                    sos.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static List<String> getFiles(String filePath) {
        List<String> filelist = Lists.newArrayList();
        File root = new File(filePath);
        File[] files = root.listFiles();
        for (File file : files) {
            filelist.add(file.getAbsolutePath());
        }
        return filelist;
    }
}
