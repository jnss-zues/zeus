package com.zeus.common;

import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
public class ApiUtil {

    @Resource
    private Properties api;

    /**
     * 获取OpenId
     */
    public String getOpenId(String jsCode) {
        StringBuilder requestUrl = new StringBuilder();
        requestUrl.append(api.weixinApiHost).append(api.getOpenId).append("?appid=")
                .append(api.appId).append("&secret=").append(api.getSecret).append("&js_code=")
                .append(jsCode).append("&grant_type=").append(api.getGrantType);
        String response = HttpsRequest.sendGet(requestUrl.toString().trim(), "");
        return response;
    }

}
