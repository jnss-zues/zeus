/**
 * 
 */
package com.zeus.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URL;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;


public class HttpsRequest {
	
	public static Logger log = LoggerFactory.getLogger(HttpsRequest.class);

    /**
     * 向指定URL发送GET方法的请求,默认编码UTF-8
     * @param url  发送请求的URL
     *  @param param 请求参数，请求参数应该是 name1=value1&name2=value2 的形式。
     *  @return URL 所代表远程资源的响应结果 
     **/
    public static String sendGet(String url, String param) {
        return sendGet(url, param, "utf-8");
    }

    /**
     * 向指定URL发送GET方法的请求
     * @param url 发送请求的URL
     * @param param 请求参数，请求参数应该是 name1=value1&name2=value2 的形式。 
     * @param charset 网页编码 
     * @return URL 所代表远程资源的响应结果 
     * */
    public static String sendGet(String url, String param,String charSet) {
        String result = "";
        BufferedReader in = null;
        try {
            SSLContext ctx = SSLContext.getInstance("TLS");
            ctx.init(new KeyManager[0],
                    new TrustManager[] { new DefaultTrustManager() },
                    new SecureRandom());
            SSLContext.setDefault(ctx);

            String urlNameString = url;
            
            log.debug("请求路径URL:"+urlNameString);
            if(!(param == null || "".equals(param.trim()))){
            	urlNameString = urlNameString + "?" + param;
            }
            
            URL realUrl = new URL(urlNameString);
            // 打开和URL之间的连接
            HttpsURLConnection connection = (HttpsURLConnection) realUrl.openConnection();
            // 设置通用的请求属性
            connection.setRequestProperty("accept", "*/*");
            connection.setRequestProperty("connection", "Keep-Alive");
            connection.setRequestProperty("user-agent",
                    "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");

            connection.setHostnameVerifier(new HostnameVerifier() {

                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });
            // 建立实际的连接
            connection.connect();
            // 定义 BufferedReader输入流来读取URL的响应
            in = new BufferedReader(new InputStreamReader(connection.getInputStream(),charSet));
            String line;
            while ((line = in.readLine()) != null) {
                result += line;
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("发送GET请求出现异常！");
        }finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        log.debug("返回参数Response:" + result);
        return result;
    }


    /** * 
     * 向指定 URL 发送POST方法的请求，默认编码UTF-8
     * @param url 发送请求的 URL 
     * @param param 请求参数，请求参数应该是 name1=value1&name2=value2 的形式 
     * @return 所代表远程资源的响应结果 
     * */
    public static String sendPost(String url, String param){
        return sendPost(url, param, "utf-8");
    }

    /**
     * 向指定 URL 发送POST方法的请求
     * @param url  发送请求的 URL
     * @param param  请求参数，请求参数应该是 name1=value1&name2=value2 的形式。 
     * @param charSet  网页编码 
     * @return 所代表远程资源的响应结果 
     * */
    public static String sendPost(String url, String param , String charSet) {
        PrintWriter out = null;
        BufferedReader in = null;
        String result = "";
        try {
            SSLContext ctx = SSLContext.getInstance("TLS");
            ctx.init(new KeyManager[0],
                    new TrustManager[] { new DefaultTrustManager() },
                    new SecureRandom());
            SSLContext.setDefault(ctx);
            
            log.debug("请求路径URL:"+url);
            log.debug("请求参数:"+param);
            URL realUrl = new URL(url);
            // 打开和URL之间的连接
            HttpsURLConnection conn = (HttpsURLConnection) realUrl.openConnection();
            // 设置通用的请求属性
            conn.setRequestProperty("accept", "*/*");
            conn.setRequestProperty("connection", "Keep-Alive");
            conn.setRequestProperty("user-agent","Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
            conn.setRequestProperty("Content-Type", "application/json");
            // 发送POST请求必须设置如下两行
            conn.setDoOutput(true);
            conn.setDoInput(true);

            conn.setHostnameVerifier(new HostnameVerifier() {

                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });

            // 获取URLConnection对象对应的输出流
            out = new PrintWriter(conn.getOutputStream());
            // 发送请求参数
            out.print(param);
            // flush输出流的缓冲
            out.flush();
            // 定义BufferedReader输入流来读取URL的响应
            in = new BufferedReader(
                    new InputStreamReader(conn.getInputStream(),charSet));
            String line;
            while ((line = in.readLine()) != null) {
                result += line;
            }
        } catch (Exception e) {            
            e.printStackTrace();
            throw new RuntimeException("发送 POST 请求出现异常！");
            
        }finally{
            try{
                if(out!=null){
                    out.close();
                }
                if(in!=null){
                    in.close();
                }
            }
            catch(IOException ex){
                ex.printStackTrace();
            }
        }
        log.debug("返回参数Response:" + result);
        return result;
    }


    /** * 
     * 向指定 URL 发送Put方法的请求，默认编码UTF-8
     * @param url 发送请求的 URL 
     * @param param 请求参数，请求参数应该是 name1=value1&name2=value2 的形式 
     * @return 所代表远程资源的响应结果 
     * */
    public static String sendPut(String url, String param){
        return sendPut(url, param, "utf-8");
    }

    /**
     * 向指定 URL 发送put方法的请求
     * @param url  发送请求的 URL
     * @param param  请求参数，请求参数应该是 name1=value1&name2=value2 的形式。 
     * @param charSet  网页编码 
     * @return 所代表远程资源的响应结果 
     * */
    public static String sendPut(String url, String param , String charSet) {
        PrintWriter out = null;
        BufferedReader in = null;
        String result = "";
        try {
            SSLContext ctx = SSLContext.getInstance("TLS");
            ctx.init(new KeyManager[0],
                    new TrustManager[] { new DefaultTrustManager() },
                    new SecureRandom());
            SSLContext.setDefault(ctx);

            log.debug("请求路径URL:"+url);
            log.debug("请求参数:"+param);
            URL realUrl = new URL(url);
            // 打开和URL之间的连接
            HttpsURLConnection conn = (HttpsURLConnection) realUrl.openConnection();
            // 设置通用的请求属性
            conn.setRequestProperty("accept", "*/*");
            conn.setRequestProperty("connection", "Keep-Alive");
            conn.setRequestProperty("user-agent","Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
            conn.setRequestProperty("Content-Type", "application/json");
            
            conn.setRequestMethod("PUT");
            
            conn.setDoOutput(true);
            conn.setDoInput(true);

            conn.setHostnameVerifier(new HostnameVerifier() {

                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });

            // 获取URLConnection对象对应的输出流
            out = new PrintWriter(conn.getOutputStream());
            // 发送请求参数
            out.print(param);
            // flush输出流的缓冲
            out.flush();
            // 定义BufferedReader输入流来读取URL的响应
            in = new BufferedReader(
                    new InputStreamReader(conn.getInputStream(),charSet));
            String line;
            while ((line = in.readLine()) != null) {
                result += line;
            }
        } catch (Exception e) {            
            e.printStackTrace();
            throw new RuntimeException("发送 PUT 请求出现异常！");
            
        }finally{
            try{
                if(out!=null){
                    out.close();
                }
                if(in!=null){
                    in.close();
                }
            }
            catch(IOException ex){
                ex.printStackTrace();
            }
        }
        log.debug("返回参数Response:" + result);
        return result;
    }
    
    
    
    
    
    private static class DefaultTrustManager implements X509TrustManager {

        public void checkClientTrusted(X509Certificate[] arg0, String arg1)
                throws CertificateException {
        }

        
        public void checkServerTrusted(X509Certificate[] arg0, String arg1)
                throws CertificateException {
        }

        public X509Certificate[] getAcceptedIssuers() {
            return null;
        }

    }
}
