package com.zeus.common;

public class Constant {

    public static final boolean SUCCESS = true;

    public static final boolean FAILURE = false;

    public static final String ERROR = "error";

    public static  final Integer DICT_DEPT_TYPE = 1;

    public static  final Integer ROOT_DEPT_PID = 0;

    public static  final String DEPT_FINAL = "final";

    public static  final Integer DICT_MEETING_STATUS_TYPE = 2;

    public static final Integer MEETING_STATUS_AFTER = 100;

    public static final Integer MEETING_STATUS_BEFORE = 101;

    public static final Integer MEETING_STATUS_END = 102;

    public static final Integer MEETING_DEAFULT_PRAISE_NUM = 0;

}
