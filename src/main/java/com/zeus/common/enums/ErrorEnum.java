package com.zeus.common.enums;

public enum ErrorEnum {

    PARAMS_NULL(1,"参数为空"),
    GOODS_UPLOAD_ERROR(2,"上传图片失败"),
    GOODS_IMG_INVALID(3,"商品图片不存在"),
    LOGIN_FAILED(4,"登录失败"),
    NONE_PERMISSION(5,"对不起，无投票权限"),
    HAS_RECORD(6,"您已经投过票了"),
    VOTE_NOT_START(7,"投票未开始"),
    VOTE_FINISHED(8,"投票已结束"),
    NONE_RIGHT(9,"确权失败"),
    INVOKE_ERROR(10,"远程接口调用失败"),
    WX_LOGIN_ERROR(11,"微信登录失败"),
    NONE_OPENID(12,"openId为空"),
    UN_LOGIN(13,"未登录"),
    SEND_MESSAGE_ERROR(14,"短信发送异常"),
    NEED_RIGHT(15,"需要确权"),
    VALID_CODE_INVALID(16,"验证码无效"),
    INVALID_MOBILE(17,"无效的手机号"),
    NONE_COMMUNITY(18,"未选择小区"),
    HAS_VOTE_RUNNING(19,"已有进行中的投票，无法发布新的投票"),
    NONE_VOTE(20,"无投票信息"),
    CAN_NOT_UPDATE(21,"已终止的投票无法修改"),
    CAN_NOT_UPDATE_START_TIME(22,"进行中的投票无法修改开始时间"),
    NONE_OWNER(23,"不存在的业主"),
    NOT_FINISH_VOTE(24,"还有未投票的选项，请投票"),
    HAS_HOUSE_VOTE(25,"该业主已存在")
    ;



    private Integer code;

    private String msg;

    ErrorEnum(Integer code,String msg){
        this.code = code;
        this.msg = msg;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
