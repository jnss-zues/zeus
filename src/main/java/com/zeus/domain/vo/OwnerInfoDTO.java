package com.zeus.domain.vo;

import lombok.Data;

/**
 * @author yishan@dian.so
 * @version 1.0.0
 * @data 2019/3/3
 */
@Data
public class OwnerInfoDTO {

    //区 幢 单元 房号 面积 房屋类型 业主姓名 电话

    private String houseId;
    private String district;
    private Integer buildingNum;
    private Integer unit;
    private Integer roomNum;
    private Float area;
    private Integer houseType;
    private String houseTypeStr;
    private String names;
    private String mobiles;
    private String no;
}
