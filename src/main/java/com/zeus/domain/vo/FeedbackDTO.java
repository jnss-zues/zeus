package com.zeus.domain.vo;

import com.zeus.domain.entity.Feedback;
import lombok.Data;

/**
 * @author yishan@dian.so
 * @version 1.0.0
 * @data 2019/3/4
 */
@Data
public class FeedbackDTO extends Feedback{
    private String statusStr;
    private String createTimeStr;
    private String feedbackTimeStr;
}
