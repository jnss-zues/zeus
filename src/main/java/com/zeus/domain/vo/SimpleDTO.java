package com.zeus.domain.vo;

import lombok.Data;

/**
 * @author yishan@dian.so
 * @version 1.0.0
 * @data 2019/3/3
 */
@Data
public class SimpleDTO implements Comparable<SimpleDTO>{

    private Integer id;
    private String text;

    @Override
    public int compareTo(SimpleDTO o) {
        if(id!=o.getId()){
            return id-o.getId();
        }else {
            return text.compareTo(o.getText());
        }
    }
}
