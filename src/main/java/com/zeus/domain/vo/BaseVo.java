package com.zeus.domain.vo;

import com.zeus.common.enums.ErrorEnum;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

@Data
public class BaseVo<T> {

    private boolean success;

    private String msg;

    private Object data;

    private String code;

    public BaseVo(boolean success,String msg,Object data,String code){
        this.success = success;
        this.msg = msg;
        this.data = data;
        this.code = code;
    }

    /**
     * 创建正确返回vo
     * @param msg
     * @param data
     * @return
     */
    public static BaseVo create(String msg,Object data){
        return new BaseVo(true,msg,data,null);
    }
    public static BaseVo create(Object data){
        return new BaseVo(true,StringUtils.EMPTY,data,null);
    }

    /**
     * 创建异常Vo
     * @param errorEnum
     * @return
     */
    public static BaseVo errorCreate(ErrorEnum errorEnum){
        return new BaseVo(false,errorEnum.getMsg(),null,String.valueOf(errorEnum.getCode()));
    }

    public static BaseVo errorCreate(String code, String msg){
        return new BaseVo(false,msg,null,code);
    }
}
