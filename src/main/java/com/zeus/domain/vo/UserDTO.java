package com.zeus.domain.vo;

import com.zeus.domain.param.UserParams;
import lombok.Data;

/**
 * @author yishan@dian.so
 * @version 1.0.0
 * @data 2019/3/2
 */
@Data
public class UserDTO extends UserParams{

    private String dutyStr;
    private Integer zone;
    private String zoneStr;
}
