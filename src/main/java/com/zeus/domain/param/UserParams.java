package com.zeus.domain.param;

import lombok.Data;

/**
 * @author yishan@dian.so
 * @version 1.0.0
 * @data 2019/3/2
 */
@Data
public class UserParams {

    private String name;
    private String mobile;
    private String room;
    private Integer duty;
    private Integer communityId;
    private Integer id;
}
