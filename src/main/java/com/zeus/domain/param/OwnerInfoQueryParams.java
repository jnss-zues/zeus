package com.zeus.domain.param;

import com.zeus.domain.dto.PageDTO;

/**
 * @author yishan@dian.so
 * @version 1.0.0
 * @data 2019/3/3
 */

public class OwnerInfoQueryParams extends PageDTO {

    private Integer buildingNum;
    private Integer unit;
    private String district;
    private Integer communityId;
    private Integer status;

    public Integer getBuildingNum() {
        return buildingNum;
    }

    public void setBuildingNum(Integer buildingNum) {
        this.buildingNum = buildingNum;
    }

    public Integer getUnit() {
        return unit;
    }

    public void setUnit(Integer unit) {
        this.unit = unit;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public Integer getCommunityId() {
        return communityId;
    }

    public void setCommunityId(Integer communityId) {
        this.communityId = communityId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
