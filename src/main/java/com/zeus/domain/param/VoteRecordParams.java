package com.zeus.domain.param;

import com.zeus.domain.entity.HouseVoteIssue;
import com.zeus.domain.entity.VoteRecord;
import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
public class VoteRecordParams extends VoteRecord {

    //客户端的openId
    private String openId;

    //投票结果(多议题)
    private List<HouseVoteIssue> results;


}
