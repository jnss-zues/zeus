package com.zeus.domain.param;

import com.zeus.domain.entity.Feedback;
import lombok.Data;

/**
 * @author yishan@dian.so
 * @version 1.0.0
 * @data 2019/3/4
 */
@Data
public class FeedbackHandleParams extends Feedback{

    private Integer feedbackId;
    private Integer userId;
    private String userName;
}
