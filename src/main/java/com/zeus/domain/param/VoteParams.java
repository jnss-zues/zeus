package com.zeus.domain.param;

import com.zeus.domain.entity.Vote;
import com.zeus.domain.entity.VoteIssue;
import lombok.Data;

import java.util.List;

/**
 * 投票搜索参数
 */
@Data
public class VoteParams extends Vote {

    //当前页 默认第1页
    private Integer pageNum = 0;

    //每页大小 默认每页10条
    private Integer pageSize = 10;

    //议题列表
    private List<VoteIssue> issues;

}
