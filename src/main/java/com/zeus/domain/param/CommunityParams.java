package com.zeus.domain.param;

import lombok.Data;

/**
 * @author yishan@dian.so
 * @version 1.0.0
 * @data 2019/3/2
 */
@Data
public class CommunityParams {

    private Integer communityId;
    private Integer householdNum;
    private String buildingArea;
    private String signature;
    private Integer voteType;
}
