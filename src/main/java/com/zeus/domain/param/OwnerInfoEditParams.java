package com.zeus.domain.param;

import java.util.List;
import lombok.Data;

/**
 * @author yishan@dian.so
 * @version 1.0.0
 * @data 2019/3/3
 */
@Data
public class OwnerInfoEditParams {

    private String houseId;
    private String names;
    private String mobiles;

    private String district;
    private Integer buildingNum;
    private Integer unit;
    private Integer roomNum;
    private Float area;
    private Integer houseType;
    private Integer no;

}
