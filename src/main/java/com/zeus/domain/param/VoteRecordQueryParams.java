package com.zeus.domain.param;

import com.zeus.domain.entity.HouseVote;
import com.zeus.domain.entity.VoteRecord;
import lombok.Data;

import java.util.List;

@Data
public class VoteRecordQueryParams extends HouseVote {

    //状态
    private Integer status;

    //幢
    private Integer buildingNum;

    //单元
    private Integer unit;

    //姓名
    private String name;

    //区
    private String district;

    //投票人名称
    private String operatorName;

    //当前页 默认第1页
    private Integer pageNum = 0;

    //每页大小 默认每页10条
    private Integer pageSize = 10;

    //参与投票的单位(区)
    private String voteUnits;

    //房号Id列表
    private List<String> houseIds;

    //投票人电话
    private String voteMobile;

}
