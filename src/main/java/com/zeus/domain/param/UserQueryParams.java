package com.zeus.domain.param;

import com.zeus.domain.dto.PageDTO;
import lombok.Data;

/**
 * @author yishan@dian.so
 * @version 1.0.0
 * @data 2019/3/2
 */
@Data
public class UserQueryParams extends PageDTO {

    private Integer communityId;
}
