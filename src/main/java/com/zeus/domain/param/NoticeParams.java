package com.zeus.domain.param;

import com.zeus.domain.entity.Notice;
import lombok.Data;

@Data
public class NoticeParams extends Notice {

    private String tempId;

    //当前页 默认第1页
    private Integer pageNum = 0;

    //每页大小 默认每页10条
    private Integer pageSize = 10;

}
