package com.zeus.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum VoteRecordStatusEnum {

    UNKNOWN(0,"未接收"),
    NOT_VOTE(1,"已接收未投票"),
    HAS_VOTE(2,"已投票")
    ;

    private Integer code;

    private String desc;

    public static String getDesc(Integer code){
        VoteRecordStatusEnum[] voteResultEnums = VoteRecordStatusEnum.values();
        for(VoteRecordStatusEnum voteResultEnum : voteResultEnums){
            if(voteResultEnum.getCode().equals(code)){
                return voteResultEnum.getDesc();
            }

        }
        return null;

    }



}
