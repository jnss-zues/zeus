package com.zeus.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum VoteResultEnum {

    UNKNOWN(0,"未投票"),
    NOT_START(1,"赞成"),
    UNDERWAY(2,"反对"),
    WAIVER(3,"弃权")
    ;

    private Integer code;

    private String desc;

    public static String getDesc(Integer code){
        VoteResultEnum[] voteResultEnums = VoteResultEnum.values();
        for(VoteResultEnum voteResultEnum : voteResultEnums){
            if(voteResultEnum.getCode().equals(code)){
                return voteResultEnum.getDesc();
            }

        }
        return "未投票";

    }



}
