package com.zeus.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author yishan@dian.so
 * @version 1.0.0
 * @data 2019/3/2
 */
@Getter
@AllArgsConstructor
public enum VoteTypeEnum {

    UNKNOWN(0,"未知"),
    NOT_ADD_VOTE(1,"参与未表决计入多数票"),
    ADD_VOTE(2,"参与未表决不计入多数票")
    ;

    private Integer code;

    private String desc;

    public static String getDesc(Integer code){
        if(code == null) {
            return null;
        }
        VoteTypeEnum[] voteTypeEnums = VoteTypeEnum.values();
        for(VoteTypeEnum voteTypeEnum : voteTypeEnums){
            if(voteTypeEnum.getCode().equals(code)){
                return voteTypeEnum.getDesc();
            }

        }
        return null;

    }
}
