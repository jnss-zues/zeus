package com.zeus.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author yishan@dian.so
 * @version 1.0.0
 * @data 2019/3/2
 */
@Getter
@AllArgsConstructor
public enum SmsKeyEnum {

    LOGIN(1,"_login"),
    RIGHT(2,"_right"),
    ;

    private Integer code;

    private String desc;

    public static String getDesc(Integer code){
        if(code == null) {
            return null;
        }
        SmsKeyEnum[] voteTypeEnums = SmsKeyEnum.values();
        for(SmsKeyEnum voteTypeEnum : voteTypeEnums){
            if(voteTypeEnum.getCode().equals(code)){
                return voteTypeEnum.getDesc();
            }

        }
        return null;

    }
}
