package com.zeus.domain.dto;

import lombok.Data;

@Data
public class VoteRecordDTO {

    private String district;

    private Integer buildingNum;

    private Integer unit;

    private Integer roomNum;

    private Integer area;

    private Integer operator;

    private String name;

    private String mobile;

    private Integer recordId;

    private Integer voteId;

    private String houseId;

    private Integer houseType;

    private String status;

    private Integer result;

    private String resultStr;


}
