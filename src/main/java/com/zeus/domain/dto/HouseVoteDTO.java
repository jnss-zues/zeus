package com.zeus.domain.dto;

import com.zeus.domain.entity.HouseVote;
import com.zeus.domain.entity.HouseVoteIssue;
import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
public class HouseVoteDTO extends HouseVote {

    //投票结果
    private String resultStr;

    //投票记录状态
    private String statusStr;

    //投票结果(多议题)
    private List<HouseVoteIssue> results;
}
