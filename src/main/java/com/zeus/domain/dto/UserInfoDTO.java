package com.zeus.domain.dto;

import com.zeus.domain.entity.User;
import lombok.Data;

@Data
public class UserInfoDTO extends User {

    //小区名
    private String community;

    //小区署名
    private String signature;

}
