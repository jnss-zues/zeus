package com.zeus.domain.dto;

import lombok.Data;

@Data
public class CountVoteDTO {

    private Integer id;

    //投票标题
    private String title;

    //投票数
    private Integer recordNum;

    //赞成票数
    private Integer zcNum;

    //反对票数
    private Integer fdNum;

    //弃权票
    private Integer qqNum;

    //接受未投票数
    private Integer wcyNum;

    //未参与数
    private Integer wcyyNum;

    //赞成率
    private Double zcl;

    //反对率
    private Double fdl;

    //弃权率
    private Double qql;

    //未参与率
    private Double wcyl;

    //赞成面积
    private Double zcAreaCount;


    private Double allAreaCount;

    //赞成面积总数
    private Double zcAreaCountPre;

    //议题id
    private Integer issueId;

    //议题内容
    private String issue;

}
