package com.zeus.domain.dto;

import lombok.Data;


@Data
public class TextMessage {

    private String content;

    private String toUserName;

    private String fromUserName;

    private String createTime;

    private String msgType;

    private String EventKey;

    private String Ticket;

}
