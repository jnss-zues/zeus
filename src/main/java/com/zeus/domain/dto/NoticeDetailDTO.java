package com.zeus.domain.dto;

import com.zeus.domain.entity.Imgs;
import com.zeus.domain.entity.Notice;
import lombok.Data;

import java.util.List;

@Data
public class NoticeDetailDTO extends Notice {

    private String publisherName;

    private List<Imgs> imgList;

}
