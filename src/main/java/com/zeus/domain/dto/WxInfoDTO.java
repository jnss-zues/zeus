package com.zeus.domain.dto;

import com.zeus.domain.entity.WxInfo;
import lombok.Data;

@Data
public class WxInfoDTO {

    private String openId;

    private String nickname;

    private Integer sex;

    private String city;

    private String province;

    private String language;

    private String country;

    private String headimgurl;

    public WxInfo toDO(){
        WxInfoDTO wxInfoDTO= this;
        WxInfo wxInfo = new WxInfo();
        wxInfo.setWxCity(wxInfoDTO.getCity());
        wxInfo.setWxAvatarUrl(wxInfoDTO.getHeadimgurl());
        wxInfo.setWxCountry(wxInfoDTO.getCountry());
        wxInfo.setWxGender(String.valueOf(wxInfoDTO.getSex()));
        wxInfo.setWxName(wxInfoDTO.getNickname());
        wxInfo.setWxOpenid(wxInfoDTO.getOpenId());
        return wxInfo;
    }

}
