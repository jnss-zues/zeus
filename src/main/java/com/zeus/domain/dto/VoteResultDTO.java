package com.zeus.domain.dto;

import com.zeus.domain.entity.HouseVoteIssue;
import lombok.Data;

@Data
public class VoteResultDTO extends HouseVoteIssue {

    //投票结果
    private String resultStr;
}
