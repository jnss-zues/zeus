package com.zeus.domain.dto;

/**
 * @author yishan@dian.so
 * @version 1.0.0
 * @data 2019/3/9
 */

public class PageDTO {

    private Integer offset = 0;
    private Integer pageSize = 10;
    private Integer pageNum = 1;
    private Integer totalCount;

    public Integer getOffset() {
        return (pageNum-1) * pageSize;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Integer getPageNum() {
        return pageNum;
    }

    public void setPageNum(Integer pageNum) {
        this.pageNum = pageNum;
    }

    public Integer getTotalCount(Integer count) {
        return (count  +  pageSize  - 1) / pageSize;
    }

    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }
}
