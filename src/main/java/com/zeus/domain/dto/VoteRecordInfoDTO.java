package com.zeus.domain.dto;

import com.zeus.domain.entity.VoteRecord;
import lombok.Data;

@Data
public class VoteRecordInfoDTO extends VoteRecord {

    //分区
    private String district;

    //房号
    private String address;
}
