package com.zeus.domain.dto;

import lombok.Data;

import java.util.List;

@Data
public class VoteCountInfoDTO {

    private CountHouseDTO countHouse;

    private List<CountVoteDTO> countVote;

    private List<CountVoteDTO> countVoteTwo;
}
