package com.zeus.domain.dto;

import lombok.Data;

@Data
public class CountHouseDTO {

    private Integer houseNum;

    private Integer ownerNum;

    private Integer areaCount;

    private Double communityArea;

    private Integer communityOwnerNum;

    private Integer districtOwnerNum;
}
