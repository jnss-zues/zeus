package com.zeus.domain.dto;

import com.zeus.domain.entity.HouseVoteIssue;
import com.zeus.domain.entity.Vote;
import com.zeus.domain.entity.VoteIssue;
import com.zeus.domain.entity.VoteRecord;
import lombok.Data;

import java.util.List;

@Data
public class VoteDetailDTO extends Vote {

    private List<VoteRecord> recordList;

    private String statusStr;

    private String publisherStr;

    private boolean canVote = true;

    private Integer result;

    private String resultStr;

    private List<VoteIssue> issues;

    private List<VoteResultDTO> results;

}
