package com.zeus.domain.entity;

import com.google.common.base.Converter;
import com.zeus.domain.VoteTypeEnum;
import com.zeus.domain.vo.CommunityDTO;
import java.util.Date;
import lombok.Data;

@Data
public class Community {
    private Integer id;

    private String name;

    private String city;

    private String province;

    private String country;

    private String adress;

    private Integer householdNum;

    private String buildingArea;

    private String signature;

    private Integer voteType;

    private String remark;

    private Date createTime;

    private Date updateTime;

    private Integer deleted;

    public CommunityDTO convert() {
        CommunityDTOConvert convert = new CommunityDTOConvert();
        return convert.convert(this);
    }

    private class CommunityDTOConvert extends Converter<Community, CommunityDTO> {

        @Override
        protected CommunityDTO doForward(Community community) {
            CommunityDTO communityDTO = new CommunityDTO();
            communityDTO.setBuildingArea(community.getBuildingArea());
            communityDTO.setCommunityId(community.getId());
            communityDTO.setHouseholdNum(community.getHouseholdNum());
            communityDTO.setSignature(community.getSignature());
            communityDTO.setVoteType(community.getVoteType());
            communityDTO.setVoteTypeStr(VoteTypeEnum.getDesc(community.getVoteType()));
            return communityDTO;
        }

        @Override
        protected Community doBackward(CommunityDTO communityDTO) {
            return null;
        }
    }

}