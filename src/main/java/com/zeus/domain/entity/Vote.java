package com.zeus.domain.entity;

import java.util.Date;

public class Vote {
    private Integer id;

    private String title;

    private String voteDeclare;

    private Integer publisher;

    private Integer operator;

    private Date startTime;

    private Date endTime;

    private Integer maxNum;

    private Date authenticLightTime;

    private String voteUnits;

    private Integer status;

    private Integer totalTicketNum;

    private Integer totalOwnerNum;

    private String totalArea;

    private Integer shouldTicketNum;

    private String remark;

    private Date createTime;

    private Date updateTime;

    private Integer deleted;

    private String issue;

    private Integer communityId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title == null ? null : title.trim();
    }

    public String getVoteDeclare() {
        return voteDeclare;
    }

    public void setVoteDeclare(String voteDeclare) {
        this.voteDeclare = voteDeclare == null ? null : voteDeclare.trim();
    }

    public Integer getPublisher() {
        return publisher;
    }

    public void setPublisher(Integer publisher) {
        this.publisher = publisher;
    }

    public Integer getOperator() {
        return operator;
    }

    public void setOperator(Integer operator) {
        this.operator = operator;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Integer getMaxNum() {
        return maxNum;
    }

    public void setMaxNum(Integer maxNum) {
        this.maxNum = maxNum;
    }

    public Date getAuthenticLightTime() {
        return authenticLightTime;
    }

    public void setAuthenticLightTime(Date authenticLightTime) {
        this.authenticLightTime = authenticLightTime;
    }

    public String getVoteUnits() {
        return voteUnits;
    }

    public void setVoteUnits(String voteUnits) {
        this.voteUnits = voteUnits == null ? null : voteUnits.trim();
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getTotalTicketNum() {
        return totalTicketNum;
    }

    public void setTotalTicketNum(Integer totalTicketNum) {
        this.totalTicketNum = totalTicketNum;
    }

    public Integer getTotalOwnerNum() {
        return totalOwnerNum;
    }

    public void setTotalOwnerNum(Integer totalOwnerNum) {
        this.totalOwnerNum = totalOwnerNum;
    }

    public String getTotalArea() {
        return totalArea;
    }

    public void setTotalArea(String totalArea) {
        this.totalArea = totalArea == null ? null : totalArea.trim();
    }

    public Integer getShouldTicketNum() {
        return shouldTicketNum;
    }

    public void setShouldTicketNum(Integer shouldTicketNum) {
        this.shouldTicketNum = shouldTicketNum;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getDeleted() {
        return deleted;
    }

    public void setDeleted(Integer deleted) {
        this.deleted = deleted;
    }

    public String getIssue() {
        return issue;
    }

    public void setIssue(String issue) {
        this.issue = issue == null ? null : issue.trim();
    }

    public Integer getCommunityId() {
        return communityId;
    }

    public void setCommunityId(Integer communityId) {
        this.communityId = communityId;
    }
}