package com.zeus.domain.entity;

import com.google.common.base.Converter;
import com.zeus.domain.HouseTypeEnum;
import com.zeus.domain.vo.OwnerInfoDTO;
import java.util.Date;
import lombok.Data;

@Data
public class House {
    private String id;

    private Integer communityId;

    private Integer buildingNum;

    private Integer unit;

    private Integer roomNum;

    private Float area;

    private Integer houseType;

    private String names;

    private String mobiles;

    private Integer status;

    private Integer type;

    private String wxName;

    private String wxOpenid;

    private String wxAvatarUrl;

    private String wxCity;

    private String wxProvince;

    private String wxCountry;

    private String wxGender;

    private String remark;

    private Date createTime;

    private Date updateTime;

    private Integer deleted;

    private String district;

    private Integer no;


    public OwnerInfoDTO convert() {
        OwnerInfoDTOConvert ownerInfoDTOConvert = new OwnerInfoDTOConvert();
        return ownerInfoDTOConvert.convert(this);
    }

    private class OwnerInfoDTOConvert extends Converter<House,OwnerInfoDTO> {

        @Override
        protected OwnerInfoDTO doForward(House house) {
            OwnerInfoDTO ownerInfoDTO = new OwnerInfoDTO();
            ownerInfoDTO.setArea(house.getArea());
            ownerInfoDTO.setBuildingNum(house.getBuildingNum());
            ownerInfoDTO.setDistrict(house.getDistrict());
            ownerInfoDTO.setHouseId(house.getId());
            ownerInfoDTO.setHouseType(house.getHouseType());
            ownerInfoDTO.setHouseTypeStr(HouseTypeEnum.getDesc(house.getHouseType()));
            ownerInfoDTO.setMobiles(house.getMobiles());
            ownerInfoDTO.setNames(house.getNames());
            ownerInfoDTO.setUnit(house.getUnit());
            ownerInfoDTO.setRoomNum(house.getRoomNum());
            ownerInfoDTO.setNo(String.format("%03d",house.getNo()));
            return ownerInfoDTO;
        }

        @Override
        protected House doBackward(OwnerInfoDTO ownerInfoDTO) {
            return null;
        }
    }


}