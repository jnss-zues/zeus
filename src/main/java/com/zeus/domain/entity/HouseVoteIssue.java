package com.zeus.domain.entity;

import java.util.Date;

public class HouseVoteIssue {
    private Integer id;

    private Integer voteId;

    private String issue;

    private String remark;

    private Date createTime;

    private Date updateTime;

    private Integer deleted;

    private Integer houseVoteId;

    private Integer result;

    private Date voteTime;

    private Integer voteIssueId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getVoteId() {
        return voteId;
    }

    public void setVoteId(Integer voteId) {
        this.voteId = voteId;
    }

    public String getIssue() {
        return issue;
    }

    public void setIssue(String issue) {
        this.issue = issue == null ? null : issue.trim();
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getDeleted() {
        return deleted;
    }

    public void setDeleted(Integer deleted) {
        this.deleted = deleted;
    }

    public Integer getHouseVoteId() {
        return houseVoteId;
    }

    public void setHouseVoteId(Integer houseVoteId) {
        this.houseVoteId = houseVoteId;
    }

    public Integer getResult() {
        return result;
    }

    public void setResult(Integer result) {
        this.result = result;
    }

    public Date getVoteTime() {
        return voteTime;
    }

    public void setVoteTime(Date voteTime) {
        this.voteTime = voteTime;
    }

    public Integer getVoteIssueId() {
        return voteIssueId;
    }

    public void setVoteIssueId(Integer voteIssueId) {
        this.voteIssueId = voteIssueId;
    }
}