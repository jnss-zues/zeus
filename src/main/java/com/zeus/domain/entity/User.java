package com.zeus.domain.entity;

import com.google.common.base.Converter;
import com.zeus.domain.DutyTypeEnum;
import com.zeus.domain.vo.UserDTO;
import java.util.Date;
import lombok.Data;

@Data
public class User {
    private Integer id;

    private Integer communityId;

    private String name;

    private String mobile;

    private Integer zone;

    private String room;

    private String loginName;

    private String password;

    private String idNum;

    private Integer status;

    private Integer type;

    private String wxName;

    private String wxOpenid;

    private String wxAvatarUrl;

    private String wxCity;

    private String wxProvince;

    private String wxCountry;

    private String wxGender;

    private String remark;

    private Date createTime;

    private Date updateTime;

    private Integer deleted;

    private Integer duty;

    public UserDTO convert() {
        UserDTOConvert userDTOConvert = new UserDTOConvert();
        return userDTOConvert.convert(this);
    }

    private class UserDTOConvert extends Converter<User, UserDTO> {

        @Override
        protected UserDTO doForward(User user) {
            UserDTO userDTO = new UserDTO();
            userDTO.setDutyStr(DutyTypeEnum.getDesc(user.getDuty()));
            userDTO.setDuty(user.getDuty());
            userDTO.setZone(user.getZone());
            userDTO.setZoneStr("");
            userDTO.setMobile(user.getMobile());
            userDTO.setName(user.getName());
            userDTO.setRoom(user.getRoom());
            userDTO.setId(user.getId());
            return userDTO;
        }

        @Override
        protected User doBackward(UserDTO userDTO) {
            return null;
        }
    }

}