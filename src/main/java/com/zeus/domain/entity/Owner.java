package com.zeus.domain.entity;

import java.util.Date;
import lombok.Data;

@Data
public class Owner {
    private Integer id;

    private String houseId;

    private String mobile;

    private String idNum;

    private String wxId;

    private Integer status;

    private Integer type;

    private String remark;

    private Date createTime;

    private Date updateTime;

    private Integer deleted;

    private String name;

    @Override
    public String toString() {
        return "Owner{" +
                "id=" + id +
                ", houseId=" + houseId +
                ", mobile=" + mobile +
                ", idNum='" + idNum + '\'' +
                ", wxId='" + wxId + '\'' +
                ", status=" + status +
                ", type=" + type +
                ", remark='" + remark + '\'' +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                ", deleted=" + deleted +
                '}';
    }

}