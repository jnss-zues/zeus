package com.zeus.domain.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class VoteExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public VoteExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andTitleIsNull() {
            addCriterion("title is null");
            return (Criteria) this;
        }

        public Criteria andTitleIsNotNull() {
            addCriterion("title is not null");
            return (Criteria) this;
        }

        public Criteria andTitleEqualTo(String value) {
            addCriterion("title =", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleNotEqualTo(String value) {
            addCriterion("title <>", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleGreaterThan(String value) {
            addCriterion("title >", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleGreaterThanOrEqualTo(String value) {
            addCriterion("title >=", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleLessThan(String value) {
            addCriterion("title <", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleLessThanOrEqualTo(String value) {
            addCriterion("title <=", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleLike(String value) {
            addCriterion("title like", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleNotLike(String value) {
            addCriterion("title not like", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleIn(List<String> values) {
            addCriterion("title in", values, "title");
            return (Criteria) this;
        }

        public Criteria andTitleNotIn(List<String> values) {
            addCriterion("title not in", values, "title");
            return (Criteria) this;
        }

        public Criteria andTitleBetween(String value1, String value2) {
            addCriterion("title between", value1, value2, "title");
            return (Criteria) this;
        }

        public Criteria andTitleNotBetween(String value1, String value2) {
            addCriterion("title not between", value1, value2, "title");
            return (Criteria) this;
        }

        public Criteria andVoteDeclareIsNull() {
            addCriterion("vote_declare is null");
            return (Criteria) this;
        }

        public Criteria andVoteDeclareIsNotNull() {
            addCriterion("vote_declare is not null");
            return (Criteria) this;
        }

        public Criteria andVoteDeclareEqualTo(String value) {
            addCriterion("vote_declare =", value, "voteDeclare");
            return (Criteria) this;
        }

        public Criteria andVoteDeclareNotEqualTo(String value) {
            addCriterion("vote_declare <>", value, "voteDeclare");
            return (Criteria) this;
        }

        public Criteria andVoteDeclareGreaterThan(String value) {
            addCriterion("vote_declare >", value, "voteDeclare");
            return (Criteria) this;
        }

        public Criteria andVoteDeclareGreaterThanOrEqualTo(String value) {
            addCriterion("vote_declare >=", value, "voteDeclare");
            return (Criteria) this;
        }

        public Criteria andVoteDeclareLessThan(String value) {
            addCriterion("vote_declare <", value, "voteDeclare");
            return (Criteria) this;
        }

        public Criteria andVoteDeclareLessThanOrEqualTo(String value) {
            addCriterion("vote_declare <=", value, "voteDeclare");
            return (Criteria) this;
        }

        public Criteria andVoteDeclareLike(String value) {
            addCriterion("vote_declare like", value, "voteDeclare");
            return (Criteria) this;
        }

        public Criteria andVoteDeclareNotLike(String value) {
            addCriterion("vote_declare not like", value, "voteDeclare");
            return (Criteria) this;
        }

        public Criteria andVoteDeclareIn(List<String> values) {
            addCriterion("vote_declare in", values, "voteDeclare");
            return (Criteria) this;
        }

        public Criteria andVoteDeclareNotIn(List<String> values) {
            addCriterion("vote_declare not in", values, "voteDeclare");
            return (Criteria) this;
        }

        public Criteria andVoteDeclareBetween(String value1, String value2) {
            addCriterion("vote_declare between", value1, value2, "voteDeclare");
            return (Criteria) this;
        }

        public Criteria andVoteDeclareNotBetween(String value1, String value2) {
            addCriterion("vote_declare not between", value1, value2, "voteDeclare");
            return (Criteria) this;
        }

        public Criteria andPublisherIsNull() {
            addCriterion("publisher is null");
            return (Criteria) this;
        }

        public Criteria andPublisherIsNotNull() {
            addCriterion("publisher is not null");
            return (Criteria) this;
        }

        public Criteria andPublisherEqualTo(Integer value) {
            addCriterion("publisher =", value, "publisher");
            return (Criteria) this;
        }

        public Criteria andPublisherNotEqualTo(Integer value) {
            addCriterion("publisher <>", value, "publisher");
            return (Criteria) this;
        }

        public Criteria andPublisherGreaterThan(Integer value) {
            addCriterion("publisher >", value, "publisher");
            return (Criteria) this;
        }

        public Criteria andPublisherGreaterThanOrEqualTo(Integer value) {
            addCriterion("publisher >=", value, "publisher");
            return (Criteria) this;
        }

        public Criteria andPublisherLessThan(Integer value) {
            addCriterion("publisher <", value, "publisher");
            return (Criteria) this;
        }

        public Criteria andPublisherLessThanOrEqualTo(Integer value) {
            addCriterion("publisher <=", value, "publisher");
            return (Criteria) this;
        }

        public Criteria andPublisherIn(List<Integer> values) {
            addCriterion("publisher in", values, "publisher");
            return (Criteria) this;
        }

        public Criteria andPublisherNotIn(List<Integer> values) {
            addCriterion("publisher not in", values, "publisher");
            return (Criteria) this;
        }

        public Criteria andPublisherBetween(Integer value1, Integer value2) {
            addCriterion("publisher between", value1, value2, "publisher");
            return (Criteria) this;
        }

        public Criteria andPublisherNotBetween(Integer value1, Integer value2) {
            addCriterion("publisher not between", value1, value2, "publisher");
            return (Criteria) this;
        }

        public Criteria andOperatorIsNull() {
            addCriterion("operator is null");
            return (Criteria) this;
        }

        public Criteria andOperatorIsNotNull() {
            addCriterion("operator is not null");
            return (Criteria) this;
        }

        public Criteria andOperatorEqualTo(Integer value) {
            addCriterion("operator =", value, "operator");
            return (Criteria) this;
        }

        public Criteria andOperatorNotEqualTo(Integer value) {
            addCriterion("operator <>", value, "operator");
            return (Criteria) this;
        }

        public Criteria andOperatorGreaterThan(Integer value) {
            addCriterion("operator >", value, "operator");
            return (Criteria) this;
        }

        public Criteria andOperatorGreaterThanOrEqualTo(Integer value) {
            addCriterion("operator >=", value, "operator");
            return (Criteria) this;
        }

        public Criteria andOperatorLessThan(Integer value) {
            addCriterion("operator <", value, "operator");
            return (Criteria) this;
        }

        public Criteria andOperatorLessThanOrEqualTo(Integer value) {
            addCriterion("operator <=", value, "operator");
            return (Criteria) this;
        }

        public Criteria andOperatorIn(List<Integer> values) {
            addCriterion("operator in", values, "operator");
            return (Criteria) this;
        }

        public Criteria andOperatorNotIn(List<Integer> values) {
            addCriterion("operator not in", values, "operator");
            return (Criteria) this;
        }

        public Criteria andOperatorBetween(Integer value1, Integer value2) {
            addCriterion("operator between", value1, value2, "operator");
            return (Criteria) this;
        }

        public Criteria andOperatorNotBetween(Integer value1, Integer value2) {
            addCriterion("operator not between", value1, value2, "operator");
            return (Criteria) this;
        }

        public Criteria andStartTimeIsNull() {
            addCriterion("start_time is null");
            return (Criteria) this;
        }

        public Criteria andStartTimeIsNotNull() {
            addCriterion("start_time is not null");
            return (Criteria) this;
        }

        public Criteria andStartTimeEqualTo(Date value) {
            addCriterion("start_time =", value, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeNotEqualTo(Date value) {
            addCriterion("start_time <>", value, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeGreaterThan(Date value) {
            addCriterion("start_time >", value, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("start_time >=", value, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeLessThan(Date value) {
            addCriterion("start_time <", value, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeLessThanOrEqualTo(Date value) {
            addCriterion("start_time <=", value, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeIn(List<Date> values) {
            addCriterion("start_time in", values, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeNotIn(List<Date> values) {
            addCriterion("start_time not in", values, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeBetween(Date value1, Date value2) {
            addCriterion("start_time between", value1, value2, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeNotBetween(Date value1, Date value2) {
            addCriterion("start_time not between", value1, value2, "startTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeIsNull() {
            addCriterion("end_time is null");
            return (Criteria) this;
        }

        public Criteria andEndTimeIsNotNull() {
            addCriterion("end_time is not null");
            return (Criteria) this;
        }

        public Criteria andEndTimeEqualTo(Date value) {
            addCriterion("end_time =", value, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeNotEqualTo(Date value) {
            addCriterion("end_time <>", value, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeGreaterThan(Date value) {
            addCriterion("end_time >", value, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("end_time >=", value, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeLessThan(Date value) {
            addCriterion("end_time <", value, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeLessThanOrEqualTo(Date value) {
            addCriterion("end_time <=", value, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeIn(List<Date> values) {
            addCriterion("end_time in", values, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeNotIn(List<Date> values) {
            addCriterion("end_time not in", values, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeBetween(Date value1, Date value2) {
            addCriterion("end_time between", value1, value2, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeNotBetween(Date value1, Date value2) {
            addCriterion("end_time not between", value1, value2, "endTime");
            return (Criteria) this;
        }

        public Criteria andMaxNumIsNull() {
            addCriterion("max_num is null");
            return (Criteria) this;
        }

        public Criteria andMaxNumIsNotNull() {
            addCriterion("max_num is not null");
            return (Criteria) this;
        }

        public Criteria andMaxNumEqualTo(Integer value) {
            addCriterion("max_num =", value, "maxNum");
            return (Criteria) this;
        }

        public Criteria andMaxNumNotEqualTo(Integer value) {
            addCriterion("max_num <>", value, "maxNum");
            return (Criteria) this;
        }

        public Criteria andMaxNumGreaterThan(Integer value) {
            addCriterion("max_num >", value, "maxNum");
            return (Criteria) this;
        }

        public Criteria andMaxNumGreaterThanOrEqualTo(Integer value) {
            addCriterion("max_num >=", value, "maxNum");
            return (Criteria) this;
        }

        public Criteria andMaxNumLessThan(Integer value) {
            addCriterion("max_num <", value, "maxNum");
            return (Criteria) this;
        }

        public Criteria andMaxNumLessThanOrEqualTo(Integer value) {
            addCriterion("max_num <=", value, "maxNum");
            return (Criteria) this;
        }

        public Criteria andMaxNumIn(List<Integer> values) {
            addCriterion("max_num in", values, "maxNum");
            return (Criteria) this;
        }

        public Criteria andMaxNumNotIn(List<Integer> values) {
            addCriterion("max_num not in", values, "maxNum");
            return (Criteria) this;
        }

        public Criteria andMaxNumBetween(Integer value1, Integer value2) {
            addCriterion("max_num between", value1, value2, "maxNum");
            return (Criteria) this;
        }

        public Criteria andMaxNumNotBetween(Integer value1, Integer value2) {
            addCriterion("max_num not between", value1, value2, "maxNum");
            return (Criteria) this;
        }

        public Criteria andAuthenticLightTimeIsNull() {
            addCriterion("authentic_light_time is null");
            return (Criteria) this;
        }

        public Criteria andAuthenticLightTimeIsNotNull() {
            addCriterion("authentic_light_time is not null");
            return (Criteria) this;
        }

        public Criteria andAuthenticLightTimeEqualTo(Date value) {
            addCriterion("authentic_light_time =", value, "authenticLightTime");
            return (Criteria) this;
        }

        public Criteria andAuthenticLightTimeNotEqualTo(Date value) {
            addCriterion("authentic_light_time <>", value, "authenticLightTime");
            return (Criteria) this;
        }

        public Criteria andAuthenticLightTimeGreaterThan(Date value) {
            addCriterion("authentic_light_time >", value, "authenticLightTime");
            return (Criteria) this;
        }

        public Criteria andAuthenticLightTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("authentic_light_time >=", value, "authenticLightTime");
            return (Criteria) this;
        }

        public Criteria andAuthenticLightTimeLessThan(Date value) {
            addCriterion("authentic_light_time <", value, "authenticLightTime");
            return (Criteria) this;
        }

        public Criteria andAuthenticLightTimeLessThanOrEqualTo(Date value) {
            addCriterion("authentic_light_time <=", value, "authenticLightTime");
            return (Criteria) this;
        }

        public Criteria andAuthenticLightTimeIn(List<Date> values) {
            addCriterion("authentic_light_time in", values, "authenticLightTime");
            return (Criteria) this;
        }

        public Criteria andAuthenticLightTimeNotIn(List<Date> values) {
            addCriterion("authentic_light_time not in", values, "authenticLightTime");
            return (Criteria) this;
        }

        public Criteria andAuthenticLightTimeBetween(Date value1, Date value2) {
            addCriterion("authentic_light_time between", value1, value2, "authenticLightTime");
            return (Criteria) this;
        }

        public Criteria andAuthenticLightTimeNotBetween(Date value1, Date value2) {
            addCriterion("authentic_light_time not between", value1, value2, "authenticLightTime");
            return (Criteria) this;
        }

        public Criteria andVoteUnitsIsNull() {
            addCriterion("vote_units is null");
            return (Criteria) this;
        }

        public Criteria andVoteUnitsIsNotNull() {
            addCriterion("vote_units is not null");
            return (Criteria) this;
        }

        public Criteria andVoteUnitsEqualTo(String value) {
            addCriterion("vote_units =", value, "voteUnits");
            return (Criteria) this;
        }

        public Criteria andVoteUnitsNotEqualTo(String value) {
            addCriterion("vote_units <>", value, "voteUnits");
            return (Criteria) this;
        }

        public Criteria andVoteUnitsGreaterThan(String value) {
            addCriterion("vote_units >", value, "voteUnits");
            return (Criteria) this;
        }

        public Criteria andVoteUnitsGreaterThanOrEqualTo(String value) {
            addCriterion("vote_units >=", value, "voteUnits");
            return (Criteria) this;
        }

        public Criteria andVoteUnitsLessThan(String value) {
            addCriterion("vote_units <", value, "voteUnits");
            return (Criteria) this;
        }

        public Criteria andVoteUnitsLessThanOrEqualTo(String value) {
            addCriterion("vote_units <=", value, "voteUnits");
            return (Criteria) this;
        }

        public Criteria andVoteUnitsLike(String value) {
            addCriterion("vote_units like", value, "voteUnits");
            return (Criteria) this;
        }

        public Criteria andVoteUnitsNotLike(String value) {
            addCriterion("vote_units not like", value, "voteUnits");
            return (Criteria) this;
        }

        public Criteria andVoteUnitsIn(List<String> values) {
            addCriterion("vote_units in", values, "voteUnits");
            return (Criteria) this;
        }

        public Criteria andVoteUnitsNotIn(List<String> values) {
            addCriterion("vote_units not in", values, "voteUnits");
            return (Criteria) this;
        }

        public Criteria andVoteUnitsBetween(String value1, String value2) {
            addCriterion("vote_units between", value1, value2, "voteUnits");
            return (Criteria) this;
        }

        public Criteria andVoteUnitsNotBetween(String value1, String value2) {
            addCriterion("vote_units not between", value1, value2, "voteUnits");
            return (Criteria) this;
        }

        public Criteria andStatusIsNull() {
            addCriterion("status is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("status is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(Integer value) {
            addCriterion("status =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(Integer value) {
            addCriterion("status <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(Integer value) {
            addCriterion("status >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("status >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(Integer value) {
            addCriterion("status <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(Integer value) {
            addCriterion("status <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<Integer> values) {
            addCriterion("status in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<Integer> values) {
            addCriterion("status not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(Integer value1, Integer value2) {
            addCriterion("status between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("status not between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andTotalTicketNumIsNull() {
            addCriterion("total_ticket_num is null");
            return (Criteria) this;
        }

        public Criteria andTotalTicketNumIsNotNull() {
            addCriterion("total_ticket_num is not null");
            return (Criteria) this;
        }

        public Criteria andTotalTicketNumEqualTo(Integer value) {
            addCriterion("total_ticket_num =", value, "totalTicketNum");
            return (Criteria) this;
        }

        public Criteria andTotalTicketNumNotEqualTo(Integer value) {
            addCriterion("total_ticket_num <>", value, "totalTicketNum");
            return (Criteria) this;
        }

        public Criteria andTotalTicketNumGreaterThan(Integer value) {
            addCriterion("total_ticket_num >", value, "totalTicketNum");
            return (Criteria) this;
        }

        public Criteria andTotalTicketNumGreaterThanOrEqualTo(Integer value) {
            addCriterion("total_ticket_num >=", value, "totalTicketNum");
            return (Criteria) this;
        }

        public Criteria andTotalTicketNumLessThan(Integer value) {
            addCriterion("total_ticket_num <", value, "totalTicketNum");
            return (Criteria) this;
        }

        public Criteria andTotalTicketNumLessThanOrEqualTo(Integer value) {
            addCriterion("total_ticket_num <=", value, "totalTicketNum");
            return (Criteria) this;
        }

        public Criteria andTotalTicketNumIn(List<Integer> values) {
            addCriterion("total_ticket_num in", values, "totalTicketNum");
            return (Criteria) this;
        }

        public Criteria andTotalTicketNumNotIn(List<Integer> values) {
            addCriterion("total_ticket_num not in", values, "totalTicketNum");
            return (Criteria) this;
        }

        public Criteria andTotalTicketNumBetween(Integer value1, Integer value2) {
            addCriterion("total_ticket_num between", value1, value2, "totalTicketNum");
            return (Criteria) this;
        }

        public Criteria andTotalTicketNumNotBetween(Integer value1, Integer value2) {
            addCriterion("total_ticket_num not between", value1, value2, "totalTicketNum");
            return (Criteria) this;
        }

        public Criteria andTotalOwnerNumIsNull() {
            addCriterion("total_owner_num is null");
            return (Criteria) this;
        }

        public Criteria andTotalOwnerNumIsNotNull() {
            addCriterion("total_owner_num is not null");
            return (Criteria) this;
        }

        public Criteria andTotalOwnerNumEqualTo(Integer value) {
            addCriterion("total_owner_num =", value, "totalOwnerNum");
            return (Criteria) this;
        }

        public Criteria andTotalOwnerNumNotEqualTo(Integer value) {
            addCriterion("total_owner_num <>", value, "totalOwnerNum");
            return (Criteria) this;
        }

        public Criteria andTotalOwnerNumGreaterThan(Integer value) {
            addCriterion("total_owner_num >", value, "totalOwnerNum");
            return (Criteria) this;
        }

        public Criteria andTotalOwnerNumGreaterThanOrEqualTo(Integer value) {
            addCriterion("total_owner_num >=", value, "totalOwnerNum");
            return (Criteria) this;
        }

        public Criteria andTotalOwnerNumLessThan(Integer value) {
            addCriterion("total_owner_num <", value, "totalOwnerNum");
            return (Criteria) this;
        }

        public Criteria andTotalOwnerNumLessThanOrEqualTo(Integer value) {
            addCriterion("total_owner_num <=", value, "totalOwnerNum");
            return (Criteria) this;
        }

        public Criteria andTotalOwnerNumIn(List<Integer> values) {
            addCriterion("total_owner_num in", values, "totalOwnerNum");
            return (Criteria) this;
        }

        public Criteria andTotalOwnerNumNotIn(List<Integer> values) {
            addCriterion("total_owner_num not in", values, "totalOwnerNum");
            return (Criteria) this;
        }

        public Criteria andTotalOwnerNumBetween(Integer value1, Integer value2) {
            addCriterion("total_owner_num between", value1, value2, "totalOwnerNum");
            return (Criteria) this;
        }

        public Criteria andTotalOwnerNumNotBetween(Integer value1, Integer value2) {
            addCriterion("total_owner_num not between", value1, value2, "totalOwnerNum");
            return (Criteria) this;
        }

        public Criteria andTotalAreaIsNull() {
            addCriterion("total_area is null");
            return (Criteria) this;
        }

        public Criteria andTotalAreaIsNotNull() {
            addCriterion("total_area is not null");
            return (Criteria) this;
        }

        public Criteria andTotalAreaEqualTo(String value) {
            addCriterion("total_area =", value, "totalArea");
            return (Criteria) this;
        }

        public Criteria andTotalAreaNotEqualTo(String value) {
            addCriterion("total_area <>", value, "totalArea");
            return (Criteria) this;
        }

        public Criteria andTotalAreaGreaterThan(String value) {
            addCriterion("total_area >", value, "totalArea");
            return (Criteria) this;
        }

        public Criteria andTotalAreaGreaterThanOrEqualTo(String value) {
            addCriterion("total_area >=", value, "totalArea");
            return (Criteria) this;
        }

        public Criteria andTotalAreaLessThan(String value) {
            addCriterion("total_area <", value, "totalArea");
            return (Criteria) this;
        }

        public Criteria andTotalAreaLessThanOrEqualTo(String value) {
            addCriterion("total_area <=", value, "totalArea");
            return (Criteria) this;
        }

        public Criteria andTotalAreaLike(String value) {
            addCriterion("total_area like", value, "totalArea");
            return (Criteria) this;
        }

        public Criteria andTotalAreaNotLike(String value) {
            addCriterion("total_area not like", value, "totalArea");
            return (Criteria) this;
        }

        public Criteria andTotalAreaIn(List<String> values) {
            addCriterion("total_area in", values, "totalArea");
            return (Criteria) this;
        }

        public Criteria andTotalAreaNotIn(List<String> values) {
            addCriterion("total_area not in", values, "totalArea");
            return (Criteria) this;
        }

        public Criteria andTotalAreaBetween(String value1, String value2) {
            addCriterion("total_area between", value1, value2, "totalArea");
            return (Criteria) this;
        }

        public Criteria andTotalAreaNotBetween(String value1, String value2) {
            addCriterion("total_area not between", value1, value2, "totalArea");
            return (Criteria) this;
        }

        public Criteria andShouldTicketNumIsNull() {
            addCriterion("should_ticket_num is null");
            return (Criteria) this;
        }

        public Criteria andShouldTicketNumIsNotNull() {
            addCriterion("should_ticket_num is not null");
            return (Criteria) this;
        }

        public Criteria andShouldTicketNumEqualTo(Integer value) {
            addCriterion("should_ticket_num =", value, "shouldTicketNum");
            return (Criteria) this;
        }

        public Criteria andShouldTicketNumNotEqualTo(Integer value) {
            addCriterion("should_ticket_num <>", value, "shouldTicketNum");
            return (Criteria) this;
        }

        public Criteria andShouldTicketNumGreaterThan(Integer value) {
            addCriterion("should_ticket_num >", value, "shouldTicketNum");
            return (Criteria) this;
        }

        public Criteria andShouldTicketNumGreaterThanOrEqualTo(Integer value) {
            addCriterion("should_ticket_num >=", value, "shouldTicketNum");
            return (Criteria) this;
        }

        public Criteria andShouldTicketNumLessThan(Integer value) {
            addCriterion("should_ticket_num <", value, "shouldTicketNum");
            return (Criteria) this;
        }

        public Criteria andShouldTicketNumLessThanOrEqualTo(Integer value) {
            addCriterion("should_ticket_num <=", value, "shouldTicketNum");
            return (Criteria) this;
        }

        public Criteria andShouldTicketNumIn(List<Integer> values) {
            addCriterion("should_ticket_num in", values, "shouldTicketNum");
            return (Criteria) this;
        }

        public Criteria andShouldTicketNumNotIn(List<Integer> values) {
            addCriterion("should_ticket_num not in", values, "shouldTicketNum");
            return (Criteria) this;
        }

        public Criteria andShouldTicketNumBetween(Integer value1, Integer value2) {
            addCriterion("should_ticket_num between", value1, value2, "shouldTicketNum");
            return (Criteria) this;
        }

        public Criteria andShouldTicketNumNotBetween(Integer value1, Integer value2) {
            addCriterion("should_ticket_num not between", value1, value2, "shouldTicketNum");
            return (Criteria) this;
        }

        public Criteria andRemarkIsNull() {
            addCriterion("remark is null");
            return (Criteria) this;
        }

        public Criteria andRemarkIsNotNull() {
            addCriterion("remark is not null");
            return (Criteria) this;
        }

        public Criteria andRemarkEqualTo(String value) {
            addCriterion("remark =", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotEqualTo(String value) {
            addCriterion("remark <>", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkGreaterThan(String value) {
            addCriterion("remark >", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkGreaterThanOrEqualTo(String value) {
            addCriterion("remark >=", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLessThan(String value) {
            addCriterion("remark <", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLessThanOrEqualTo(String value) {
            addCriterion("remark <=", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLike(String value) {
            addCriterion("remark like", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotLike(String value) {
            addCriterion("remark not like", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkIn(List<String> values) {
            addCriterion("remark in", values, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotIn(List<String> values) {
            addCriterion("remark not in", values, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkBetween(String value1, String value2) {
            addCriterion("remark between", value1, value2, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotBetween(String value1, String value2) {
            addCriterion("remark not between", value1, value2, "remark");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("create_time is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("create_time is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterion("create_time =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterion("create_time <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterion("create_time >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("create_time >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Date value) {
            addCriterion("create_time <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("create_time <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterion("create_time in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterion("create_time not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterion("create_time between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("create_time not between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNull() {
            addCriterion("update_time is null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNotNull() {
            addCriterion("update_time is not null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeEqualTo(Date value) {
            addCriterion("update_time =", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotEqualTo(Date value) {
            addCriterion("update_time <>", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThan(Date value) {
            addCriterion("update_time >", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("update_time >=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThan(Date value) {
            addCriterion("update_time <", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThanOrEqualTo(Date value) {
            addCriterion("update_time <=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIn(List<Date> values) {
            addCriterion("update_time in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotIn(List<Date> values) {
            addCriterion("update_time not in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeBetween(Date value1, Date value2) {
            addCriterion("update_time between", value1, value2, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotBetween(Date value1, Date value2) {
            addCriterion("update_time not between", value1, value2, "updateTime");
            return (Criteria) this;
        }

        public Criteria andDeletedIsNull() {
            addCriterion("deleted is null");
            return (Criteria) this;
        }

        public Criteria andDeletedIsNotNull() {
            addCriterion("deleted is not null");
            return (Criteria) this;
        }

        public Criteria andDeletedEqualTo(Integer value) {
            addCriterion("deleted =", value, "deleted");
            return (Criteria) this;
        }

        public Criteria andDeletedNotEqualTo(Integer value) {
            addCriterion("deleted <>", value, "deleted");
            return (Criteria) this;
        }

        public Criteria andDeletedGreaterThan(Integer value) {
            addCriterion("deleted >", value, "deleted");
            return (Criteria) this;
        }

        public Criteria andDeletedGreaterThanOrEqualTo(Integer value) {
            addCriterion("deleted >=", value, "deleted");
            return (Criteria) this;
        }

        public Criteria andDeletedLessThan(Integer value) {
            addCriterion("deleted <", value, "deleted");
            return (Criteria) this;
        }

        public Criteria andDeletedLessThanOrEqualTo(Integer value) {
            addCriterion("deleted <=", value, "deleted");
            return (Criteria) this;
        }

        public Criteria andDeletedIn(List<Integer> values) {
            addCriterion("deleted in", values, "deleted");
            return (Criteria) this;
        }

        public Criteria andDeletedNotIn(List<Integer> values) {
            addCriterion("deleted not in", values, "deleted");
            return (Criteria) this;
        }

        public Criteria andDeletedBetween(Integer value1, Integer value2) {
            addCriterion("deleted between", value1, value2, "deleted");
            return (Criteria) this;
        }

        public Criteria andDeletedNotBetween(Integer value1, Integer value2) {
            addCriterion("deleted not between", value1, value2, "deleted");
            return (Criteria) this;
        }

        public Criteria andIssueIsNull() {
            addCriterion("issue is null");
            return (Criteria) this;
        }

        public Criteria andIssueIsNotNull() {
            addCriterion("issue is not null");
            return (Criteria) this;
        }

        public Criteria andIssueEqualTo(String value) {
            addCriterion("issue =", value, "issue");
            return (Criteria) this;
        }

        public Criteria andIssueNotEqualTo(String value) {
            addCriterion("issue <>", value, "issue");
            return (Criteria) this;
        }

        public Criteria andIssueGreaterThan(String value) {
            addCriterion("issue >", value, "issue");
            return (Criteria) this;
        }

        public Criteria andIssueGreaterThanOrEqualTo(String value) {
            addCriterion("issue >=", value, "issue");
            return (Criteria) this;
        }

        public Criteria andIssueLessThan(String value) {
            addCriterion("issue <", value, "issue");
            return (Criteria) this;
        }

        public Criteria andIssueLessThanOrEqualTo(String value) {
            addCriterion("issue <=", value, "issue");
            return (Criteria) this;
        }

        public Criteria andIssueLike(String value) {
            addCriterion("issue like", value, "issue");
            return (Criteria) this;
        }

        public Criteria andIssueNotLike(String value) {
            addCriterion("issue not like", value, "issue");
            return (Criteria) this;
        }

        public Criteria andIssueIn(List<String> values) {
            addCriterion("issue in", values, "issue");
            return (Criteria) this;
        }

        public Criteria andIssueNotIn(List<String> values) {
            addCriterion("issue not in", values, "issue");
            return (Criteria) this;
        }

        public Criteria andIssueBetween(String value1, String value2) {
            addCriterion("issue between", value1, value2, "issue");
            return (Criteria) this;
        }

        public Criteria andIssueNotBetween(String value1, String value2) {
            addCriterion("issue not between", value1, value2, "issue");
            return (Criteria) this;
        }

        public Criteria andCommunityIdIsNull() {
            addCriterion("community_id is null");
            return (Criteria) this;
        }

        public Criteria andCommunityIdIsNotNull() {
            addCriterion("community_id is not null");
            return (Criteria) this;
        }

        public Criteria andCommunityIdEqualTo(Integer value) {
            addCriterion("community_id =", value, "communityId");
            return (Criteria) this;
        }

        public Criteria andCommunityIdNotEqualTo(Integer value) {
            addCriterion("community_id <>", value, "communityId");
            return (Criteria) this;
        }

        public Criteria andCommunityIdGreaterThan(Integer value) {
            addCriterion("community_id >", value, "communityId");
            return (Criteria) this;
        }

        public Criteria andCommunityIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("community_id >=", value, "communityId");
            return (Criteria) this;
        }

        public Criteria andCommunityIdLessThan(Integer value) {
            addCriterion("community_id <", value, "communityId");
            return (Criteria) this;
        }

        public Criteria andCommunityIdLessThanOrEqualTo(Integer value) {
            addCriterion("community_id <=", value, "communityId");
            return (Criteria) this;
        }

        public Criteria andCommunityIdIn(List<Integer> values) {
            addCriterion("community_id in", values, "communityId");
            return (Criteria) this;
        }

        public Criteria andCommunityIdNotIn(List<Integer> values) {
            addCriterion("community_id not in", values, "communityId");
            return (Criteria) this;
        }

        public Criteria andCommunityIdBetween(Integer value1, Integer value2) {
            addCriterion("community_id between", value1, value2, "communityId");
            return (Criteria) this;
        }

        public Criteria andCommunityIdNotBetween(Integer value1, Integer value2) {
            addCriterion("community_id not between", value1, value2, "communityId");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}