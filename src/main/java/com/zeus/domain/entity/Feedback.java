package com.zeus.domain.entity;

import com.google.common.base.Converter;
import com.zeus.common.utils.DateUtil;
import com.zeus.domain.vo.FeedbackDTO;
import java.util.Date;
import lombok.Data;
import org.springframework.beans.BeanUtils;

@Data
public class Feedback {
    private Integer id;

    private Integer communityId;

    private Integer buildingNum;

    private Integer unit;

    private Integer roomNum;

    private String question;

    private Integer feedbackOwner;

    private String mobile;

    private Date feedbackTime;

    private Integer operator;

    private Integer status;

    private String remark;

    private Date createTime;

    private Date updateTime;

    private Integer deleted;

    private String feedbackOwnerName;

    private String operatorName;

    private String district;

    public FeedbackDTO convert() {
        FeedbackDTOConvert feedbackDTOConvert = new FeedbackDTOConvert();
        return feedbackDTOConvert.convert(this);
    }

    private class FeedbackDTOConvert extends Converter<Feedback,FeedbackDTO> {

        @Override
        protected FeedbackDTO doForward(Feedback feedback) {
            FeedbackDTO feedbackDTO = new FeedbackDTO();
            BeanUtils.copyProperties(feedback,feedbackDTO);
            feedbackDTO.setCreateTimeStr(DateUtil.dateToString(feedback.getCreateTime()));
            feedbackDTO.setFeedbackTimeStr(DateUtil.dateToString(feedback.getFeedbackTime()));
            if(feedback.getStatus() != null && feedback.getStatus() == 1) {
                feedbackDTO.setStatusStr("已解决");
            }else {
                feedbackDTO.setStatusStr("未解决");
            }
            return feedbackDTO;
        }

        @Override
        protected Feedback doBackward(FeedbackDTO feedbackDTO) {
            return null;
        }
    }
}