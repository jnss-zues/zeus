package com.zeus.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author yishan@dian.so
 * @version 1.0.0
 * @data 2019/3/3
 */
@Getter
@AllArgsConstructor
public enum HouseTypeEnum {

    UNKNOWN(0,"未知"),
    HOUSE(1,"住宅"),
    NOT_HOUSE(2,"非住宅")
    ;

    private Integer code;

    private String desc;

    public static String getDesc(Integer code){
        if(code == null) {
            return null;
        }
        HouseTypeEnum[] houseTypeEnums = HouseTypeEnum.values();
        for(HouseTypeEnum houseTypeEnum : houseTypeEnums){
            if(houseTypeEnum.getCode().equals(code)){
                return houseTypeEnum.getDesc();
            }

        }
        return null;
    }

    public static Integer getCode(String desc){
        if(desc == null) {
            return null;
        }
        HouseTypeEnum[] houseTypeEnums = HouseTypeEnum.values();
        for(HouseTypeEnum houseTypeEnum : houseTypeEnums){
            if(houseTypeEnum.getDesc().equals(desc)){
                return houseTypeEnum.getCode();
            }
        }
        return null;
    }
}
