package com.zeus.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum SubjectTypeEnum {

    UNKNOWN(0,"未知"),
    NOTICE(1,"公告"),
    VOTE(2,"投票分享"),
     ;

    private Integer code;

    private String desc;

    public static String getDesc(Integer code){
        if(code == null) {
            return null;
        }
        SubjectTypeEnum[] subjectTypeEnums = SubjectTypeEnum.values();
        for(SubjectTypeEnum subjectTypeEnum : subjectTypeEnums){
            if(subjectTypeEnum.getCode().equals(code)){
                return subjectTypeEnum.getDesc();
            }

        }
        return null;

    }
}
