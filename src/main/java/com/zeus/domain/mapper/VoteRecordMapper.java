package com.zeus.domain.mapper;

import com.zeus.domain.dto.VoteRecordDTO;
import com.zeus.domain.entity.VoteRecord;
import com.zeus.domain.entity.VoteRecordExample;
import java.util.List;

import com.zeus.domain.param.VoteRecordQueryParams;
import org.apache.ibatis.annotations.Param;

public interface VoteRecordMapper {
    long countByExample(VoteRecordExample example);

    int deleteByExample(VoteRecordExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(VoteRecord record);

    int insertSelective(VoteRecord record);

    List<VoteRecord> selectByExample(VoteRecordExample example);

    VoteRecord selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") VoteRecord record, @Param("example") VoteRecordExample example);

    int updateByExample(@Param("record") VoteRecord record, @Param("example") VoteRecordExample example);

    int updateByPrimaryKeySelective(VoteRecord record);

    int updateByPrimaryKey(VoteRecord record);

    List<VoteRecordDTO> recordList(VoteRecordQueryParams voteRecordQueryParams);
}