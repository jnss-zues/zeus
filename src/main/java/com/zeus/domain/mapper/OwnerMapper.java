package com.zeus.domain.mapper;

import com.zeus.domain.entity.Owner;
import com.zeus.domain.entity.OwnerExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface OwnerMapper {
    long countByExample(OwnerExample example);

    int deleteByExample(OwnerExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Owner record);

    int insertSelective(Owner record);

    List<Owner> selectByExample(OwnerExample example);

    Owner selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Owner record, @Param("example") OwnerExample example);

    int updateByExample(@Param("record") Owner record, @Param("example") OwnerExample example);

    int updateByPrimaryKeySelective(Owner record);

    int updateByPrimaryKey(Owner record);

    void batchInsert(@Param("owners") List<Owner> owners);

    void deleteByHouseId(String houseId);

    Owner getByMobile(@Param("mobile")String mobile);

    Integer getOwnerByMobile(@Param("houseId")String houseId, @Param("list")List<String> list);
}