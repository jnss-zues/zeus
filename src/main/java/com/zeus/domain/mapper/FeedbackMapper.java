package com.zeus.domain.mapper;

import com.zeus.domain.entity.Feedback;
import com.zeus.domain.entity.FeedbackExample;
import com.zeus.domain.param.OwnerInfoQueryParams;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface FeedbackMapper {
    long countByExample(FeedbackExample example);

    int deleteByExample(FeedbackExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Feedback record);

    int insertSelective(Feedback record);

    List<Feedback> selectByExample(FeedbackExample example);

    Feedback selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Feedback record, @Param("example") FeedbackExample example);

    int updateByExample(@Param("record") Feedback record, @Param("example") FeedbackExample example);

    int updateByPrimaryKeySelective(Feedback record);

    int updateByPrimaryKey(Feedback record);

    List<Feedback> listByCondition(@Param("communityId") Integer communityId,  @Param("offset") Integer offset, @Param("pageSize") Integer pageSize ,
            @Param("district") String district,  @Param("buildingNum") Integer buildingNum, @Param("unit") Integer unit, @Param("status") Integer status);

    Integer countByCondition(@Param("communityId") Integer communityId,
            @Param("district") String district,  @Param("buildingNum") Integer buildingNum, @Param("unit") Integer unit, @Param("status") Integer status);
}