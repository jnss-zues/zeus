package com.zeus.domain.mapper;

import com.zeus.domain.entity.HouseVote;
import com.zeus.domain.entity.HouseVoteExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface HouseVoteMapper {
    long countByExample(HouseVoteExample example);

    int deleteByExample(HouseVoteExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(HouseVote record);

    int insertBatch(@Param("houseVoteList") List<HouseVote> houseVoteList);

    int insertSelective(HouseVote record);

    List<HouseVote> selectByExample(HouseVoteExample example);

    HouseVote selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") HouseVote record, @Param("example") HouseVoteExample example);

    int updateByExample(@Param("record") HouseVote record, @Param("example") HouseVoteExample example);

    int updateByPrimaryKeySelective(HouseVote record);

    int updateByPrimaryKey(HouseVote record);
}