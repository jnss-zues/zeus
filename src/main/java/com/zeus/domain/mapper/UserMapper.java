package com.zeus.domain.mapper;

import com.zeus.domain.entity.User;
import com.zeus.domain.entity.UserExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface UserMapper {
    long countByExample(UserExample example);

    int deleteByExample(UserExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(User record);

    int insertSelective(User record);

    List<User> selectByExample(UserExample example);

    User selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") User record, @Param("example") UserExample example);

    int updateByExample(@Param("record") User record, @Param("example") UserExample example);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);

    void deleteById(@Param("id") Integer id);

    List<User> selectByCommunityId(@Param("communityId") Integer communityId,  @Param("offset") Integer offset, @Param("pageSize") Integer pageSize);

    Integer countByCommunityId(@Param("communityId") Integer communityId);


    Integer getByMobile(@Param("mobile")String mobile,@Param("communityId")Integer communityId);
}