package com.zeus.domain.mapper;

import com.zeus.domain.entity.VoteIssue;
import com.zeus.domain.entity.VoteIssueExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface VoteIssueMapper {
    long countByExample(VoteIssueExample example);

    int deleteByExample(VoteIssueExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(VoteIssue record);

    int insertSelective(VoteIssue record);

    List<VoteIssue> selectByExample(VoteIssueExample example);

    VoteIssue selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") VoteIssue record, @Param("example") VoteIssueExample example);

    int updateByExample(@Param("record") VoteIssue record, @Param("example") VoteIssueExample example);

    int updateByPrimaryKeySelective(VoteIssue record);

    int updateByPrimaryKey(VoteIssue record);
}