package com.zeus.domain.mapper;

import com.zeus.domain.entity.House;
import com.zeus.domain.entity.HouseExample;
import com.zeus.domain.param.OwnerInfoQueryParams;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface HouseMapper {
    long countByExample(HouseExample example);

    int deleteByExample(HouseExample example);

    int deleteByPrimaryKey(String id);

    int insert(House record);

    int insertSelective(House record);

    List<House> selectByExample(HouseExample example);

    House selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") House record, @Param("example") HouseExample example);

    int updateByExample(@Param("record") House record, @Param("example") HouseExample example);

    int updateByPrimaryKeySelective(House record);

    int updateByPrimaryKey(House record);

    List<House> listByCommunityId(@Param("communityId") Integer communityId);

    List<House> listByCondition(@Param("communityId") Integer communityId,  @Param("offset") Integer offset, @Param("pageSize") Integer pageSize ,
            @Param("district") String district,  @Param("buildingNum") Integer buildingNum, @Param("unit") Integer unit,@Param("roomNum")Integer roomNum);

    Integer countByCondition(@Param("communityId") Integer communityId,
            @Param("district") String district,  @Param("buildingNum") Integer buildingNum, @Param("unit") Integer unit,@Param("roomNum")Integer roomNum);

    void batchInsert(@Param("houses") List<House> houses);

    List<House> test(@Param("communityId") Integer communityId,  @Param("offset") Integer offset, @Param("pageSize") Integer pageSize);

    void deleteByCommunityId(Integer communityId);

    List<House> listByNamesAndMobilesLike(@Param("name") String name,@Param("mobile") String mobile);
}