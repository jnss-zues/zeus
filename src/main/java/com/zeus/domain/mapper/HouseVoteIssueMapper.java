package com.zeus.domain.mapper;

import com.zeus.domain.entity.HouseVoteIssue;
import com.zeus.domain.entity.HouseVoteIssueExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface HouseVoteIssueMapper {
    long countByExample(HouseVoteIssueExample example);

    int deleteByExample(HouseVoteIssueExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(HouseVoteIssue record);

    int insertSelective(HouseVoteIssue record);

    List<HouseVoteIssue> selectByExample(HouseVoteIssueExample example);

    HouseVoteIssue selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") HouseVoteIssue record, @Param("example") HouseVoteIssueExample example);

    int updateByExample(@Param("record") HouseVoteIssue record, @Param("example") HouseVoteIssueExample example);

    int updateByPrimaryKeySelective(HouseVoteIssue record);

    int updateByPrimaryKey(HouseVoteIssue record);
}