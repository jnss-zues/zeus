package com.zeus.domain.mapper;

import com.zeus.domain.dto.CountHouseDTO;
import com.zeus.domain.dto.CountVoteDTO;
import com.zeus.domain.entity.Vote;
import com.zeus.domain.entity.VoteExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface VoteMapper {
    long countByExample(VoteExample example);

    int deleteByExample(VoteExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Vote record);

    int insertSelective(Vote record);

    List<Vote> selectByExample(VoteExample example);

    Vote selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Vote record, @Param("example") VoteExample example);

    int updateByExample(@Param("record") Vote record, @Param("example") VoteExample example);

    int updateByPrimaryKeySelective(Vote record);

    int updateByPrimaryKey(Vote record);

    CountHouseDTO countHouse(Integer communityId);

    List<CountVoteDTO> countVote(Integer voteId);

    List<CountVoteDTO> countVoteTwo(Integer voteId);

    Integer countDistrictOwnerNum(@Param("district") String district,@Param("communityId") Integer communityId);
}