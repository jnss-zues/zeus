package com.zeus.domain.mapper;

import com.zeus.domain.entity.WxInfo;
import com.zeus.domain.entity.WxInfoExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface WxInfoMapper {
    long countByExample(WxInfoExample example);

    int deleteByExample(WxInfoExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(WxInfo record);

    int insertSelective(WxInfo record);

    List<WxInfo> selectByExample(WxInfoExample example);

    WxInfo selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") WxInfo record, @Param("example") WxInfoExample example);

    int updateByExample(@Param("record") WxInfo record, @Param("example") WxInfoExample example);

    int updateByPrimaryKeySelective(WxInfo record);

    int updateByPrimaryKey(WxInfo record);
}