package com.zeus.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author yishan@dian.so
 * @version 1.0.0
 * @data 2019/3/2
 */
@Getter
@AllArgsConstructor
public enum DutyTypeEnum {

    UNKNOWN(0,"未知"),
    YWH(1,"业委会"),
    ZYZ(2,"志愿者")
    ;

    private Integer code;

    private String desc;

    public static String getDesc(Integer code){
        if(code == null) {
            return null;
        }
        DutyTypeEnum[] dutyTypeEnums = DutyTypeEnum.values();
        for(DutyTypeEnum dutyTypeEnum : dutyTypeEnums){
            if(dutyTypeEnum.getCode().equals(code)){
                return dutyTypeEnum.getDesc();
            }

        }
        return null;

    }
}
