package com.zeus.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

@Getter
@AllArgsConstructor
public enum VoteStatusEnum {

    UNKNOWN(0,"未知"),
    NOT_START(1,"未开始"),
    UNDERWAY(2,"进行中"),
    TERMINATION(3,"已终止"),
    UNPUBLICIZED(4,"未公示"),
    PUBLICIZED(5,"已公示")
    ;

    private Integer code;

    private String desc;

    public static String getDesc(Integer code){
        VoteStatusEnum[] voteStatusEnums = VoteStatusEnum.values();
        for(VoteStatusEnum voteStatusEnum : voteStatusEnums){
            if(voteStatusEnum.getCode().equals(code)){
                return voteStatusEnum.getDesc();
            }

        }
        return null;

    }



}
