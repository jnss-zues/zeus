package com.zeus.dao;

import com.zeus.domain.entity.HouseVoteIssue;

import java.util.List;

public interface HouseVoteIssueDao {

    /**
     * 查询投票记录议题详情
     * @param houseVoteId
     * @return
     */
    List<HouseVoteIssue> listByHouseVote(Integer houseVoteId);


    /**
     * 查询投票记录议题列表
     * @param voteId
     * @return
     */
    List<HouseVoteIssue> listByVote(Integer voteId);

    /**
     * 插入投票结果
     * @param houseVoteIssue
     */
    void insert(HouseVoteIssue houseVoteIssue);
}
