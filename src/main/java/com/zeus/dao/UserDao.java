package com.zeus.dao;

import com.zeus.domain.entity.User;

import java.util.List;

public interface UserDao {

    List<User> listByIds(List<Integer> ids);

    User login(String userName,String password,Integer communityId);

    User getByMobile(String userName,Integer communityId);

    User getById(Integer id);

    User getByOpenId(String openId);

    User getByOpenId(String openId,Integer communityId);

    void update(User user);


}
