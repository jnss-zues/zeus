package com.zeus.dao;

import com.zeus.domain.entity.Vote;
import com.zeus.domain.param.VoteParams;

import java.util.List;

public interface VoteDao {

    List<Vote> listVote(VoteParams voteParams);

    Vote getVote(Integer id);

    void insert(Vote vote);

    void update(Vote vote);

    void delete(Integer id);

    void start();

    void end();


}
