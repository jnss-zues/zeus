package com.zeus.dao;

import com.zeus.domain.entity.WxInfo;

public interface WxInfoDao {

    WxInfo getByOpenId(String openId);

    void insert(WxInfo wxInfo);

    void update(WxInfo wxInfo);

    WxInfo getByOwner(Integer ownerId);

    WxInfo getByMobile(String mobile);

    void deleteByOwner(Integer ownerId);

}
