package com.zeus.dao;

import com.zeus.domain.entity.House;

import java.util.List;

public interface HouseDao {

    House getById(String id);

    List<House> listByDistrict(String district,Integer communityId);

    House getByNo(Integer no,Integer communityId);
}
