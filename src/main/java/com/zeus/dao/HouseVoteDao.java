package com.zeus.dao;

import com.zeus.domain.entity.HouseVote;
import com.zeus.domain.param.VoteRecordQueryParams;

import java.util.List;

public interface HouseVoteDao {

    void insertBatch(List<HouseVote> houseVoteList);

    List<HouseVote> listByParam(VoteRecordQueryParams voteRecordQueryParams);

    List<HouseVote> listByMobile(String mobile,Integer voteId);

    void update(HouseVote houseVote);

    void affirm(String mobile,Integer voteId);

    HouseVote getById(Integer id);

    void insert(HouseVote houseVote);

    HouseVote getByNo(String no,Integer voteId);
}
