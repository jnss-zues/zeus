package com.zeus.dao;

import com.zeus.domain.entity.Owner;

import java.util.List;

public interface OwnerDao {

    Owner getById(Integer id);

    Owner getByMobile(String mobile);

    List<Owner> listByMobile(String mobile);

}
