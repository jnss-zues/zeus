package com.zeus.dao.impl;

import com.zeus.dao.WxInfoDao;
import com.zeus.domain.entity.WxInfo;
import com.zeus.domain.entity.WxInfoExample;
import com.zeus.domain.mapper.WxInfoMapper;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.List;

@Repository
public class WxInfoDaoImpl implements WxInfoDao {

    @Resource
    private WxInfoMapper wxInfoMapper;

    @Override
    public WxInfo getByOpenId(String openId) {
        WxInfoExample wxInfoExample = new WxInfoExample();
        wxInfoExample.createCriteria().andWxOpenidEqualTo(openId).andDeletedEqualTo(0);
        wxInfoExample.setOrderByClause("-create_time");
        List<WxInfo> wxInfos = wxInfoMapper.selectByExample(wxInfoExample);
        if(!CollectionUtils.isEmpty(wxInfos)){
            return wxInfos.get(0);
        }
        return null;
    }

    @Override
    public void insert(WxInfo wxInfo) {
        wxInfoMapper.insertSelective(wxInfo);
    }

    @Override
    public void update(WxInfo wxInfo) {
        wxInfoMapper.updateByPrimaryKeySelective(wxInfo);
    }

    @Override
    public WxInfo getByOwner(Integer owner) {
        WxInfoExample wxInfoExample = new WxInfoExample();
        wxInfoExample.createCriteria().andOwnerIdEqualTo(owner).andDeletedEqualTo(0);
        List<WxInfo> wxInfos = wxInfoMapper.selectByExample(wxInfoExample);
        if(!CollectionUtils.isEmpty(wxInfos)){
            return wxInfos.get(0);
        }
        return null;
    }

    @Override
    public WxInfo getByMobile(String mobile) {
        WxInfoExample wxInfoExample = new WxInfoExample();
        wxInfoExample.createCriteria().andMobileEqualTo(mobile).andDeletedEqualTo(0);
        List<WxInfo> wxInfos = wxInfoMapper.selectByExample(wxInfoExample);
        if(!CollectionUtils.isEmpty(wxInfos)){
            return wxInfos.get(0);
        }
        return null;
    }

    @Override
    public void deleteByOwner(Integer ownerId) {
        WxInfoExample wxInfoExample = new WxInfoExample();
        wxInfoExample.createCriteria().andOwnerIdEqualTo(ownerId).andDeletedEqualTo(0);
        WxInfo wxInfo = new WxInfo();
        wxInfo.setDeleted(1);
        wxInfoMapper.updateByExampleSelective(wxInfo,wxInfoExample);
    }
}
