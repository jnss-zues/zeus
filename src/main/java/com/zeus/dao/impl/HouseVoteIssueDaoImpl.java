package com.zeus.dao.impl;

import com.zeus.dao.HouseVoteIssueDao;
import com.zeus.domain.entity.HouseVoteIssue;
import com.zeus.domain.entity.HouseVoteIssueExample;
import com.zeus.domain.mapper.HouseVoteIssueMapper;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;

@Repository
public class HouseVoteIssueDaoImpl implements HouseVoteIssueDao {

    @Resource
    private HouseVoteIssueMapper houseVoteIssueMapper;

    @Override
    public List<HouseVoteIssue> listByHouseVote(Integer houseVoteId) {
        HouseVoteIssueExample houseVoteIssueExample = new HouseVoteIssueExample();
        houseVoteIssueExample.createCriteria().andHouseVoteIdEqualTo(houseVoteId)
                .andDeletedEqualTo(0);
        houseVoteIssueExample.setOrderByClause("id");
        return houseVoteIssueMapper.selectByExample(houseVoteIssueExample);
    }

    @Override
    public List<HouseVoteIssue> listByVote(Integer voteId) {
        HouseVoteIssueExample houseVoteIssueExample = new HouseVoteIssueExample();
        houseVoteIssueExample.createCriteria().andVoteIdEqualTo(voteId)
                .andDeletedEqualTo(0);
        return houseVoteIssueMapper.selectByExample(houseVoteIssueExample);
    }

    @Override
    public void insert(HouseVoteIssue houseVoteIssue) {
        houseVoteIssueMapper.insertSelective(houseVoteIssue);
    }
}
