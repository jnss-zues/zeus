package com.zeus.dao.impl;

import com.zeus.common.StringUtil;
import com.zeus.dao.HouseDao;
import com.zeus.domain.entity.House;
import com.zeus.domain.entity.HouseExample;
import com.zeus.domain.mapper.HouseMapper;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.List;

@Repository
public class HouseDaoImpl implements HouseDao {
    @Resource
    private HouseMapper houseMapper;

    @Override
    public House getById(String id) {
        return houseMapper.selectByPrimaryKey(id);
    }

    @Override
    public List<House> listByDistrict(String district,Integer communityId) {
        HouseExample houseExample = new HouseExample();
        HouseExample.Criteria criteria = houseExample.createCriteria();
        criteria.andDeletedEqualTo(0).andCommunityIdEqualTo(communityId);
        if(StringUtil.isNotBlank(district) && !district.trim().equals("0")){
            criteria.andDistrictEqualTo(district);
        }
        return houseMapper.selectByExample(houseExample);
    }

    @Override
    public House getByNo(Integer no, Integer communityId) {
        HouseExample houseExample = new HouseExample();
        HouseExample.Criteria criteria = houseExample.createCriteria();
        criteria.andDeletedEqualTo(0).andCommunityIdEqualTo(communityId).andNoEqualTo(no);
        List<House> houses = houseMapper.selectByExample(houseExample);
        if(!CollectionUtils.isEmpty(houses)){
            return houses.get(0);
        }
        return null;
    }
}
