package com.zeus.dao.impl;

import com.zeus.dao.VoteIssueDao;
import com.zeus.domain.entity.VoteIssue;
import com.zeus.domain.entity.VoteIssueExample;
import com.zeus.domain.mapper.VoteIssueMapper;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

@Repository
public class VoteIssueDaoImpl implements VoteIssueDao {

    @Resource
    private VoteIssueMapper voteIssueMapper;

    @Override
    public void insertBatch(List<VoteIssue> voteIssues) {

    }

    @Override
    public void insert(VoteIssue voteIssue) {
        voteIssueMapper.insertSelective(voteIssue);
    }

    @Override
    public List<VoteIssue> listByVote(Integer voteId) {
        VoteIssueExample voteIssueExample = new VoteIssueExample();
        voteIssueExample.createCriteria().andVoteIdEqualTo(voteId).andDeletedEqualTo(0);
        return voteIssueMapper.selectByExample(voteIssueExample);
    }

    @Override
    public void update(VoteIssue voteIssues) {
        voteIssues.setUpdateTime(new Date());
        voteIssueMapper.updateByPrimaryKeySelective(voteIssues);

    }
}
