package com.zeus.dao.impl;

import com.zeus.dao.CommunityDao;
import com.zeus.domain.entity.Community;
import com.zeus.domain.entity.CommunityExample;
import com.zeus.domain.mapper.CommunityMapper;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;

@Repository
public class CommunityDaoImpl implements CommunityDao {

    @Resource
    private CommunityMapper communityMapper;

    @Override
    public List<Community> listAll() {
        CommunityExample communityExample = new CommunityExample();
        communityExample.createCriteria().andDeletedEqualTo(0);
        return communityMapper.selectByExample(communityExample);
    }
}
