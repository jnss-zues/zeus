package com.zeus.dao.impl;

import com.zeus.dao.VoteRecordDao;
import com.zeus.domain.entity.VoteRecord;
import com.zeus.domain.entity.VoteRecordExample;
import com.zeus.domain.mapper.VoteRecordMapper;
import com.zeus.domain.param.VoteRecordQueryParams;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;

@Repository
public class VoteRecordDaoImpl implements VoteRecordDao {

    @Resource
    private VoteRecordMapper voteRecordMapper;

    @Override
    public List<VoteRecord> listVoteRecord(Integer voteId) {
        VoteRecordExample voteRecordExample = new VoteRecordExample();
        voteRecordExample.createCriteria().andVoteIdEqualTo(voteId);
        return voteRecordMapper.selectByExample(voteRecordExample);
    }

    @Override
    public List<VoteRecord> listVoteRecord(VoteRecordQueryParams voteRecordQueryParams) {
        VoteRecordExample voteRecordExample = new VoteRecordExample();
        VoteRecordExample.Criteria criteria = voteRecordExample.createCriteria();
        if(voteRecordQueryParams.getVoteId() != null){
            criteria.andVoteIdEqualTo(voteRecordQueryParams.getVoteId());
        }
//        if(voteRecordQueryParams.getOperator() != null){
//            criteria.andOperatorEqualTo(voteRecordQueryParams.getOperator());
//        }
//        if(voteRecordQueryParams.getHouseId() != null){
//            criteria.andHouseIdEqualTo(voteRecordQueryParams.getHouseId());
//        }
        voteRecordExample.setOrderByClause("-create_time");
        return voteRecordMapper.selectByExample(voteRecordExample);
    }

    @Override
    public VoteRecord getVoteRecord(Integer id) {
        return voteRecordMapper.selectByPrimaryKey(id);
    }

    @Override
    public void insert(VoteRecord voteRecord) {
        voteRecordMapper.insertSelective(voteRecord);
    }

    @Override
    public void delete(Integer id) {
        VoteRecord voteRecord = new VoteRecord();
        voteRecord.setId(id);
        voteRecord.setDeleted(1);
        voteRecordMapper.updateByPrimaryKeySelective(voteRecord);

    }

    @Override
    public void deleteByVoteId(Integer voteId) {
        VoteRecordExample voteRecordExample = new VoteRecordExample();
        voteRecordExample.createCriteria().andVoteIdEqualTo(voteId);
        VoteRecord voteRecord = new VoteRecord();
        voteRecord.setDeleted(1);
        voteRecordMapper.updateByExampleSelective(voteRecord,voteRecordExample);

    }

    @Override
    public void update(VoteRecord voteRecord) {
        voteRecordMapper.updateByPrimaryKey(voteRecord);

    }
}
