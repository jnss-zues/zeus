package com.zeus.dao.impl;

import com.zeus.common.Properties;
import com.zeus.common.StringUtil;
import com.zeus.common.utils.RedisUtil;
import com.zeus.controller.LoginController;
import com.zeus.controller.MessageController;
import com.zeus.dao.UserDao;
import com.zeus.domain.entity.User;
import com.zeus.domain.entity.UserExample;
import com.zeus.domain.mapper.UserMapper;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.List;

@Repository
public class UserDaoImpl implements UserDao {

    @Resource
    private UserMapper userMapper;

    @Resource
    private Properties properties;

    @Resource
    private RedisUtil redisUtil;


    @Override
    public List<User> listByIds(List<Integer> ids) {
        UserExample userExample = new UserExample();
        userExample.createCriteria().andIdIn(ids);
        return userMapper.selectByExample(userExample);
    }

    @Override
    public User login(String userName, String password,Integer communityId) {
        UserExample userExample = new UserExample();
        userExample.createCriteria().andMobileEqualTo(userName).andCommunityIdEqualTo(communityId)
                .andDeletedEqualTo(0);
        List<User> users = userMapper.selectByExample(userExample);
        if(!CollectionUtils.isEmpty(users)){
            User user = users.get(0);
            Integer checkPassword = (Integer) redisUtil.get(userName +"_login");
            if(StringUtil.equals(password,String.valueOf(properties.commonCode))
                    || StringUtil.equals(password,String.valueOf(checkPassword))){
                redisUtil.del(userName +"_login");
                return user;
            }
        }
        return null;
    }

    @Override
    public User getByMobile(String userName, Integer communityId) {
        UserExample userExample = new UserExample();
        userExample.createCriteria().andMobileEqualTo(userName).andCommunityIdEqualTo(communityId)
                .andDeletedEqualTo(0);
        List<User> users = userMapper.selectByExample(userExample);
        if(!CollectionUtils.isEmpty(users)){
            return users.get(0);
        }
        return null;
    }

    @Override
    public User getById(Integer id) {
        return userMapper.selectByPrimaryKey(id);
    }

    @Override
    public User getByOpenId(String openId) {
        UserExample example = new UserExample();
        example.createCriteria().andWxOpenidEqualTo(openId).andDeletedEqualTo(0);
        List<User> users = userMapper.selectByExample(example);
        if(!CollectionUtils.isEmpty(users)){
            return users.get(0);
        }
        return null;
    }

    @Override
    public User getByOpenId(String openId, Integer communityId) {
        UserExample example = new UserExample();
        example.createCriteria().andWxOpenidEqualTo(openId).andCommunityIdEqualTo(communityId).andDeletedEqualTo(0);
        List<User> users = userMapper.selectByExample(example);
        if(!CollectionUtils.isEmpty(users)){
            return users.get(0);
        }
        return null;
    }

    @Override
    public void update(User user) {
        userMapper.updateByPrimaryKeySelective(user);
    }
}
