package com.zeus.dao.impl;

import com.zeus.common.StringUtil;
import com.zeus.dao.HouseVoteDao;
import com.zeus.domain.VoteRecordStatusEnum;
import com.zeus.domain.entity.HouseVote;
import com.zeus.domain.entity.HouseVoteExample;
import com.zeus.domain.mapper.HouseVoteMapper;
import com.zeus.domain.param.VoteRecordQueryParams;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.List;

@Repository
public class HouseVoteDaoImpl implements HouseVoteDao {

    @Resource
    private HouseVoteMapper houseVoteMapper;


    @Override
    public void insertBatch(List<HouseVote> houseVoteList) {
        houseVoteMapper.insertBatch(houseVoteList);
    }

    @Override
    public List<HouseVote> listByParam(VoteRecordQueryParams voteRecordQueryParams) {
        HouseVoteExample houseVoteExample = new HouseVoteExample();
        HouseVoteExample.Criteria criteria = houseVoteExample.createCriteria();
        criteria.andDeletedEqualTo(0);
        if(voteRecordQueryParams.getVoteId()!= null){
            criteria.andVoteIdEqualTo(voteRecordQueryParams.getVoteId());
        }
        if(voteRecordQueryParams.getStatus() != null){
            criteria.andStatusEqualTo(voteRecordQueryParams.getStatus());
        }
        if(voteRecordQueryParams.getBuildingNum() != null){
            criteria.andBuildingNumEqualTo(voteRecordQueryParams.getBuildingNum());
        }
        if(voteRecordQueryParams.getUnit() != null){
            criteria.andUnitEqualTo(voteRecordQueryParams.getUnit());
        }
        if(StringUtil.isNotBlank(voteRecordQueryParams.getName())){
            criteria.andNamesLike("%" + voteRecordQueryParams.getName()+ "%");
        }
        if(StringUtil.isNotBlank(voteRecordQueryParams.getMobiles())){
            criteria.andMobilesLike("%" + voteRecordQueryParams.getMobiles()+ "%");
        }
        if(StringUtil.isNotBlank(voteRecordQueryParams.getVoteNo())){
            criteria.andVoteNoLike("%" + voteRecordQueryParams.getVoteNo()+ "%");
        }
        if(StringUtil.isNotBlank(voteRecordQueryParams.getDistrict())){
            criteria.andDistrictEqualTo(voteRecordQueryParams.getDistrict());
        }
        if(StringUtil.isNotBlank(voteRecordQueryParams.getVoteMobile())){
            criteria.andVoteMobileEqualTo(voteRecordQueryParams.getVoteMobile());
        }
        houseVoteExample.setOrderByClause("vote_no");
        return houseVoteMapper.selectByExample(houseVoteExample);
    }

    @Override
    public List<HouseVote> listByMobile(String mobile,Integer voteId) {
        HouseVoteExample houseVoteExample = new HouseVoteExample();
        houseVoteExample.createCriteria().andMobilesLike("%"+ mobile +"%")
                .andDeletedEqualTo(0).andVoteIdEqualTo(voteId);
        houseVoteExample.setOrderByClause("-update_time");
        return houseVoteMapper.selectByExample(houseVoteExample);
    }

    @Override
    public void update(HouseVote houseVote) {
        houseVoteMapper.updateByPrimaryKeySelective(houseVote);

    }

    @Override
    public void affirm(String mobile, Integer voteId) {
        HouseVoteExample houseVoteExample = new HouseVoteExample();
        houseVoteExample.createCriteria().andMobilesLike("%"+ mobile +"%")
                .andVoteIdEqualTo(voteId).andStatusEqualTo(0).andDeletedEqualTo(0);
        HouseVote houseVote = new HouseVote();
        houseVote.setStatus(VoteRecordStatusEnum.NOT_VOTE.getCode());
        houseVoteMapper.updateByExampleSelective(houseVote,houseVoteExample);
    }

    @Override
    public HouseVote getById(Integer id) {
        return houseVoteMapper.selectByPrimaryKey(id);
    }

    @Override
    public void insert(HouseVote houseVote) {
        houseVoteMapper.insertSelective(houseVote);
    }

    @Override
    public HouseVote getByNo(String no, Integer voteId) {
        HouseVoteExample houseVoteExample = new HouseVoteExample();
        houseVoteExample.createCriteria().andDeletedEqualTo(0).andVoteNoLike("%"+no).andVoteIdEqualTo(voteId);
        List<HouseVote> houseVotes = houseVoteMapper.selectByExample(houseVoteExample);
        if(!CollectionUtils.isEmpty(houseVotes)){
            return houseVotes.get(0);
        }
        return null;
    }
}
