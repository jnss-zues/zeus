package com.zeus.dao.impl;

import com.zeus.dao.OwnerDao;
import com.zeus.domain.entity.Owner;
import com.zeus.domain.entity.OwnerExample;
import com.zeus.domain.mapper.OwnerMapper;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.List;

@Repository
public class OwnerDaoImpl implements OwnerDao {

    @Resource
    private OwnerMapper ownerMapper;

    @Override
    public Owner getById(Integer id) {
        return ownerMapper.selectByPrimaryKey(id);
    }

    @Override
    public Owner getByMobile(String mobile) {
        OwnerExample ownerExample = new OwnerExample();
        ownerExample.createCriteria().andMobileEqualTo(mobile).andDeletedEqualTo(0);
        List<Owner> ownerList = ownerMapper.selectByExample(ownerExample);
        if(!CollectionUtils.isEmpty(ownerList)){
            return ownerList.get(0);
        }
        return null;
    }

    @Override
    public List<Owner> listByMobile(String mobile) {
        OwnerExample ownerExample = new OwnerExample();
        ownerExample.createCriteria().andMobileEqualTo(mobile).andDeletedEqualTo(0);
        List<Owner> ownerList = ownerMapper.selectByExample(ownerExample);
        return ownerList;
    }
}
