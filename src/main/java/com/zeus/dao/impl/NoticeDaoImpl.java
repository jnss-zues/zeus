package com.zeus.dao.impl;

import com.zeus.dao.NoticeDao;
import com.zeus.domain.entity.Notice;
import com.zeus.domain.entity.NoticeExample;
import com.zeus.domain.mapper.NoticeMapper;
import com.zeus.domain.param.NoticeParams;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;

@Repository
public class NoticeDaoImpl implements NoticeDao {

    @Resource
    private NoticeMapper noticeMapper;

    @Override
    public void insert(Notice notice) {
        noticeMapper.insertSelective(notice);
    }

    @Override
    public void delete(Integer id) {
        Notice notice = new Notice();
        notice.setId(id);
        notice.setDeleted(1);
        noticeMapper.updateByPrimaryKeySelective(notice);
    }

    @Override
    public void update(Notice notice) {
        noticeMapper.updateByPrimaryKeySelective(notice);
    }

    @Override
    public List<Notice> listNotice(NoticeParams noticeParams) {
        NoticeExample noticeExample = new NoticeExample();
        NoticeExample.Criteria criteria = noticeExample.createCriteria();
        criteria.andCommunityIdEqualTo(noticeParams.getCommunityId());
        criteria.andDeletedEqualTo(0);
        if(StringUtils.isNoneBlank(noticeParams.getTitle())){
            criteria.andTitleLike("%"+ noticeParams.getTitle() +"%");
        }
        if(StringUtils.isNoneBlank(noticeParams.getContent())){
            criteria.andTitleLike("%"+ noticeParams.getContent() +"%");
        }
        noticeExample.setOrderByClause("-create_time");
        return noticeMapper.selectByExample(noticeExample);
    }

    @Override
    public Notice getInfo(Integer id) {
        return noticeMapper.selectByPrimaryKey(id);
    }

}
