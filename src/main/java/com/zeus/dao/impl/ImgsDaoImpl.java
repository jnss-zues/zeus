package com.zeus.dao.impl;

import com.zeus.dao.ImgsDao;
import com.zeus.domain.entity.Imgs;
import com.zeus.domain.entity.ImgsExample;
import com.zeus.domain.mapper.ImgsMapper;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;

@Repository
public class ImgsDaoImpl implements ImgsDao {
    @Resource
    private ImgsMapper imgsMapper;

    @Override
    public List<Imgs> listBySubjectId(Integer noticeId,Integer type) {
        ImgsExample imgsExample = new ImgsExample();
        imgsExample.createCriteria().andSubjectIdEqualTo(noticeId).andTypeEqualTo(type);
        return imgsMapper.selectByExample(imgsExample);
    }

    @Override
    public Imgs add(Imgs Imgs) {
        imgsMapper.insertSelective(Imgs);
        return Imgs;
    }

    @Override
    public void updateTemp(Integer subjectId, String tempId) {
        ImgsExample ImgsExample = new ImgsExample();
        ImgsExample.createCriteria().andTempIdEqualTo(tempId);
        Imgs img = new Imgs();
        img.setSubjectId(subjectId);
        imgsMapper.updateByExampleSelective(img,ImgsExample);
    }

    @Override
    public Imgs getInfo(Integer id) {
        return imgsMapper.selectByPrimaryKey(id);
    }
}
