package com.zeus.dao.impl;

import com.zeus.dao.VoteDao;
import com.zeus.domain.entity.Vote;
import com.zeus.domain.entity.VoteExample;
import com.zeus.domain.mapper.VoteIssueMapper;
import com.zeus.domain.mapper.VoteMapper;
import com.zeus.domain.param.VoteParams;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

@Repository
public class VoteDaoImpl implements VoteDao {

    @Resource
    private VoteMapper voteMapper;

    @Override
    public List<Vote> listVote(VoteParams voteParams) {
        VoteExample voteExample = new VoteExample();
        VoteExample.Criteria criteria = voteExample.createCriteria();
        criteria.andCommunityIdEqualTo(voteParams.getCommunityId());
        criteria.andDeletedEqualTo(0);
        if(voteParams.getStatus() != null){
            criteria.andStatusEqualTo(voteParams.getStatus());
        }
        if(voteParams.getTitle() != null){
            criteria.andTitleLike("%" + voteParams.getTitle() + "%");
        }
        voteExample.setOrderByClause("-create_time");
        return voteMapper.selectByExample(voteExample);
    }

    @Override
    public Vote getVote(Integer id) {
        return voteMapper.selectByPrimaryKey(id);
    }

    @Override
    public void insert(Vote vote) {
        voteMapper.insertSelective(vote);
    }

    @Override
    public void update(Vote vote) {
        voteMapper.updateByPrimaryKeySelective(vote);
    }

    @Override
    public void delete(Integer id) {
        Vote vote = voteMapper.selectByPrimaryKey(id);
        vote.setDeleted(1);
        voteMapper.updateByPrimaryKey(vote);

    }

    @Override
    public void start() {
        VoteExample voteExample = new VoteExample();
        voteExample.createCriteria().andStatusEqualTo(1)
                .andStartTimeLessThan(new Date()).andDeletedEqualTo(0);
        Vote vote = new Vote();
        vote.setStatus(2);
        voteMapper.updateByExampleSelective(vote,voteExample);

    }

    @Override
    public void end() {
        VoteExample voteExample = new VoteExample();
        voteExample.createCriteria().andStatusEqualTo(2)
                .andEndTimeLessThan(new Date()).andDeletedEqualTo(0);
        Vote vote = new Vote();
        vote.setStatus(3);
        voteMapper.updateByExampleSelective(vote,voteExample);
    }

}
