package com.zeus.dao;

import com.zeus.domain.entity.Notice;
import com.zeus.domain.param.NoticeParams;

import java.util.List;

public interface NoticeDao {

    void insert(Notice notice);

    void delete(Integer id);

    void update(Notice notice);

    List<Notice> listNotice(NoticeParams noticeParams);

    Notice getInfo(Integer id);
}
