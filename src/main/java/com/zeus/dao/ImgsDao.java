package com.zeus.dao;

import com.zeus.domain.entity.Imgs;

import java.util.List;

public interface ImgsDao {

    List<Imgs> listBySubjectId(Integer noticeId,Integer type);

    Imgs getInfo(Integer id);

    Imgs add(Imgs Imgs);

    void updateTemp(Integer subjectId,String tempId);

}
