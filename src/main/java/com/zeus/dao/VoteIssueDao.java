package com.zeus.dao;

import com.zeus.domain.entity.VoteIssue;

import java.util.List;

public interface VoteIssueDao {

    /**
     * 批量插入
     * @param voteIssues
     */
    void insertBatch(List<VoteIssue> voteIssues);

    /**
     * 新增
     * @param voteIssue
     */
    void insert(VoteIssue voteIssue);

    /**
     * 获取投票议题列表
     * @param voteId
     * @return
     */
    List<VoteIssue> listByVote(Integer voteId);


    /**
     * 更新
     * @param voteIssues
     */
    void update(VoteIssue voteIssues);

}
