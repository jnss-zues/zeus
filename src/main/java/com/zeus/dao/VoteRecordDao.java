package com.zeus.dao;

import com.zeus.domain.entity.VoteRecord;
import com.zeus.domain.param.VoteRecordQueryParams;

import java.util.List;

public interface VoteRecordDao {

    List<VoteRecord> listVoteRecord(Integer voteId);

    List<VoteRecord> listVoteRecord(VoteRecordQueryParams voteRecordQueryParams);

    VoteRecord getVoteRecord(Integer id);

    void insert(VoteRecord voteRecord);

    void delete(Integer id);

    void deleteByVoteId(Integer voteId);

    void update(VoteRecord voteRecord);
}
