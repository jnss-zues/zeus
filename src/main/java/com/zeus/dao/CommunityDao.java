package com.zeus.dao;

import com.zeus.domain.entity.Community;
import org.springframework.stereotype.Repository;

import java.util.List;

public interface CommunityDao {

    List<Community> listAll();
 }
