package com.zeus.service;

import com.zeus.domain.vo.BaseVo;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface ImgsService {

    BaseVo uploadImgs(MultipartFile[] imgs, Integer type, String userId, String tempId) throws IOException;

    String createGrCode(Integer voteId);

    String createNoticeGrCode(Integer noticeId);

    void saveGrCode(Integer voteId);

}
