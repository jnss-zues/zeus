package com.zeus.service;

import com.github.pagehelper.PageInfo;
import com.zeus.domain.dto.NoticeDetailDTO;
import com.zeus.domain.entity.Notice;
import com.zeus.domain.param.NoticeParams;
import com.zeus.domain.vo.BaseVo;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public interface NoticeService {

    void create(NoticeParams noticeParams);

    void update(NoticeParams noticeParams);

    PageInfo listNotice(NoticeParams noticeParams);

    void delete(NoticeParams noticeParams);

    NoticeDetailDTO getDetail(Integer id, HttpServletRequest request);

    List<NoticeDetailDTO> setPublisherName(List<Notice> notices,String community);

    void getQrCodeImg(Integer noticeId,HttpServletResponse response);

}
