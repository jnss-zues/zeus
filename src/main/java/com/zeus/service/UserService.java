package com.zeus.service;

import com.alibaba.fastjson.JSONObject;
import com.zeus.domain.entity.User;
import com.zeus.domain.param.UserParams;
import com.zeus.domain.param.UserQueryParams; /**
 * @author yishan@dian.so
 * @version 1.0.0
 * @data 2019/3/2
 */
public interface UserService {

    Boolean delete(Integer userId);

    String add(UserParams userParams);

    JSONObject listUser(UserQueryParams userQueryParams);

    User login(String userName,String password,Integer communityId);

    User getByMobile(String mobile,Integer communityId);

    User getByOpenId(String openId);

    User getByOpenId(String openId,Integer communityId);

    User getById(Integer id);

    void update(User user);
}
