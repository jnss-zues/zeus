package com.zeus.service;

import com.zeus.domain.dto.WxInfoDTO;
import com.zeus.domain.entity.WxInfo;

import java.util.Map;

public interface WxInfoService {

    WxInfo getByOpenid(String openId);

    void saveWxInfo(WxInfo wxInfo);

    void updateWxInfo(WxInfo wxInfo);

    WxInfo getByOwner(Integer ownerId);

    WxInfo getByMobile(String mobile);

    void deleteByOwner(Integer ownerId);

    String getAccessToken();

    Map<String,Object> getOpenId(String code) throws Exception;

    WxInfoDTO getWxInfo(String openid, String token) throws Exception;
}
