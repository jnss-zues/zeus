package com.zeus.service;

import com.zeus.domain.entity.House;

public interface HouseService {

    House getById(String id);

    House getByNo(Integer no,Integer communityId);
}
