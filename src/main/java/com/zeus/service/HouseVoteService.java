package com.zeus.service;

import com.github.pagehelper.PageInfo;
import com.zeus.domain.entity.House;
import com.zeus.domain.entity.HouseVote;
import com.zeus.domain.entity.HouseVoteIssue;
import com.zeus.domain.param.VoteParams;
import com.zeus.domain.param.VoteRecordQueryParams;

import java.util.List;
import java.util.Map;

public interface HouseVoteService {

    void insetBatch(VoteParams voteParams);

    PageInfo ListByParam(VoteRecordQueryParams voteRecordQueryParams);

    List<HouseVote> ListAllByParam(VoteRecordQueryParams voteRecordQueryParams);

    List<HouseVote> ListAllMobile(String mobile,Integer voteId);

    void update(HouseVote houseVote);

    void affirm(String mobile,Integer voteId);

    HouseVote getById(Integer id);

    List<HouseVoteIssue> listIssues(Integer HouseVoteId);

    void insertHouseVoteIssue(HouseVote houseVote ,List<HouseVoteIssue> results);

    void vote(HouseVote houseVote,List<HouseVoteIssue> results);

    void insert(House house,Integer voteId);

    HouseVote getByNo(Integer no,Integer voteId);
}
