package com.zeus.service;

import com.github.pagehelper.PageInfo;
import com.zeus.domain.dto.VoteCountInfoDTO;
import com.zeus.domain.dto.VoteDetailDTO;
import com.zeus.domain.dto.VoteRecordDTO;
import com.zeus.domain.entity.VoteIssue;
import com.zeus.domain.entity.VoteRecord;
import com.zeus.domain.param.VoteParams;
import com.zeus.domain.param.VoteRecordParams;
import com.zeus.domain.param.VoteRecordQueryParams;
import com.zeus.domain.vo.VoteInfoDTO;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

public interface VoteService {

    PageInfo<VoteInfoDTO> listVote(VoteParams voteParams);

    void create(VoteParams voteParams);

    void update(VoteParams voteParams);

    VoteDetailDTO getVoteDetail(Integer id);

    void voteRecordExport(VoteRecordQueryParams voteRecordQueryParams, HttpServletResponse response);

    String getVoteQrCode(Integer voteId);

    void getVoteQrCodeImg(Integer voteId,HttpServletResponse response);

    void createVoteRecord(VoteRecordParams voteRecordQueryParams);

    VoteCountInfoDTO countVote(Integer communityId,Integer voteId);

    PageInfo listVoteRecord(VoteRecordQueryParams voteRecordQueryParams);

    List<VoteIssue> listIssues(Integer voteId);


}
