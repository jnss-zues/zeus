package com.zeus.service.impl;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.google.common.collect.Lists;
import com.zeus.common.CSVHelper;
import com.zeus.common.StringUtil;
import com.zeus.common.utils.DateUtil;
import com.zeus.common.utils.FileUtil;
import com.zeus.dao.*;
import com.zeus.domain.*;
import com.zeus.domain.dto.*;
import com.zeus.domain.entity.*;
import com.zeus.domain.mapper.VoteMapper;
import com.zeus.domain.mapper.VoteRecordMapper;
import com.zeus.domain.param.VoteParams;
import com.zeus.domain.param.VoteRecordParams;
import com.zeus.domain.param.VoteRecordQueryParams;
import com.zeus.domain.vo.VoteInfoDTO;
import com.zeus.service.*;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.*;

@Service
public class VoteServiceImpl implements VoteService {

    private static final String fileTemp = "/usr/local/temp/";

    @Resource
    private VoteDao voteDao;

    @Resource
    private VoteRecordDao voteRecordDao;

    @Resource
    private ImgsService imgsService;

    @Resource
    private ImgsDao imgsDao;

    @Resource
    private HouseService houseService;

    @Resource
    private VoteMapper voteMapper;

    @Resource
    private VoteRecordMapper voteRecordMapper;

    @Resource
    private CommunityService communityService;

    @Resource
    private HouseVoteService houseVoteService;

    @Resource
    private UserService userService;

    @Resource
    private VoteIssueDao voteIssueDao;

    @Resource
    private HouseVoteIssueDao houseVoteIssueDao;



    @Override
    public PageInfo<VoteInfoDTO> listVote(VoteParams voteParams) {
        PageHelper.startPage(voteParams.getPageNum(), voteParams.getPageSize());
        List<Vote> votes = voteDao.listVote(voteParams);
        PageInfo pageInfo = new PageInfo(votes);
        votes = pageInfo.getList();
        List<VoteInfoDTO> voteInfoDTOS = new ArrayList<>();
        for(Vote vote : votes){
            VoteInfoDTO voteInfoDTO = new VoteInfoDTO();
            BeanUtils.copyProperties(vote,voteInfoDTO);
            voteInfoDTO.setStatusStr(VoteStatusEnum.getDesc(voteInfoDTO.getStatus()));
            voteInfoDTOS.add(voteInfoDTO);
        }
        pageInfo.setList(voteInfoDTOS);
        return pageInfo;
    }

    @Override
    @Transactional
    public void create(VoteParams voteParams) {
        voteDao.insert(voteParams);
        List<VoteIssue> issues = voteParams.getIssues();
        if(!CollectionUtils.isEmpty(issues)){
            for(VoteIssue voteIssue : issues){
                voteIssue.setVoteId(voteParams.getId());
                voteIssue.setCreateTime(new Date());
                voteIssueDao.insert(voteIssue);
            }
        }
        houseVoteService.insetBatch(voteParams);


    }

    @Override
    @Transactional
    public void update(VoteParams voteParams) {
        voteDao.update(voteParams);

        List<VoteIssue> voteIssues = voteIssueDao.listByVote(voteParams.getId());

        List<VoteIssue> updateList = new ArrayList<>();
        List<VoteIssue> addList = new ArrayList<>();

        Map<Integer,VoteIssue> voteIssueMap = new HashMap<>();
        for(VoteIssue voteIssue : voteIssues){
            voteIssueMap.put(voteIssue.getId(),voteIssue);
        }


        List<VoteIssue> newIssues = voteParams.getIssues();
        for(VoteIssue voteIssue : newIssues){
            if(voteIssue.getId() == null){
                voteIssue.setCreateTime(new Date());
                voteIssue.setVoteId(voteParams.getId());
                addList.add(voteIssue);
                continue;
            }

            if(voteIssueMap.get(voteIssue.getId()) != null){
                updateList.add(voteIssue);
                voteIssueMap.remove(voteIssue.getId());
                continue;
            }
        }
        Iterator<Map.Entry<Integer,VoteIssue>> iterator = voteIssueMap.entrySet().iterator();
        while (iterator.hasNext()){
            Map.Entry<Integer,VoteIssue> entity = iterator.next();
            VoteIssue voteIssue = entity.getValue();
            voteIssue.setDeleted(1);
            updateList.add(voteIssue);
        }

        for(VoteIssue updateIssue : updateList){
            voteIssueDao.update(updateIssue);
        }

        for(VoteIssue addIssue : addList){
            voteIssueDao.insert(addIssue);
        }
    }

    @Override
    public VoteDetailDTO getVoteDetail(Integer id) {
        Vote vote = voteDao.getVote(id);
        VoteDetailDTO voteDetailDTO = new VoteDetailDTO();
        if(vote != null){
            BeanUtils.copyProperties(vote, voteDetailDTO);
            voteDetailDTO.setIssues(voteIssueDao.listByVote(id));
            voteDetailDTO.setStatusStr(VoteStatusEnum.getDesc(voteDetailDTO.getStatus()));
            if(new Integer(0).equals(voteDetailDTO.getPublisher())){
                Community community = communityService.getById(voteDetailDTO.getCommunityId());
                if(community != null){
                    voteDetailDTO.setPublisherStr(community.getSignature());
                }
            }else{
                User user = userService.getById(voteDetailDTO.getCommunityId());
                if(user != null){
                    voteDetailDTO.setPublisherStr(user.getName());
                }
            }
            return voteDetailDTO;
        }
        return null;
    }

    @Override
    public void voteRecordExport(VoteRecordQueryParams voteRecordQueryParams, HttpServletResponse response) {
        List<HouseVote> records = houseVoteService.ListAllByParam(voteRecordQueryParams);
        List<String> title = Lists.newArrayList();
        title.add("分区");
        title.add("房号");
        title.add("投票编号");
        title.add("姓名");
        title.add("电话");
        title.add("面积");
        title.add("投票时间");
        title.add("接受状态");
        title.add("投票结果");
        List<HouseVoteIssue> houseVoteIssues = houseVoteIssueDao.listByVote(voteRecordQueryParams.getVoteId());
        Map<Integer,Map<Integer,String>> houseVoteIssuesMap = new HashMap<>();
        for(HouseVoteIssue houseVoteIssue : houseVoteIssues){
            Map<Integer,String> houseVoteIssuesTemp = houseVoteIssuesMap.get(houseVoteIssue.getHouseVoteId());
            if(CollectionUtils.isEmpty(houseVoteIssuesTemp)){
                houseVoteIssuesTemp = new HashMap<>();
            }
            houseVoteIssuesTemp.put(houseVoteIssue.getVoteIssueId(),VoteResultEnum.getDesc(houseVoteIssue.getResult()));
            houseVoteIssuesMap.put(houseVoteIssue.getHouseVoteId(),houseVoteIssuesTemp);
        }

        List<VoteIssue> voteIssues = voteIssueDao.listByVote(voteRecordQueryParams.getVoteId());
        for(VoteIssue voteIssue : voteIssues){
            title.add(voteIssue.getIssue());
        }
        List<String[]> data = new ArrayList<>();
        for (HouseVote dto :records){
            List<String> strList = new ArrayList<>();
            strList.add(dto.getDistrict() + "");
            strList.add(dto.getDistrict() + dto.getBuildingNum()+ "幢"+ dto.getUnit() +"单元"+dto.getRoomNum() +"号");
            strList.add(dto.getVoteNo());
            strList.add(dto.getNames()+ "");
            strList.add(dto.getMobiles()+ "");
            strList.add(dto.getArea()+ "");
            strList.add(dto.getStatus() == 2?DateUtil.dateToString(dto.getUpdateTime(), "yyyy-MM-dd HH:mm:dd"):"");
            strList.add(VoteRecordStatusEnum.getDesc(dto.getStatus()));
            strList.add(VoteResultEnum.getDesc(dto.getResult()));
            Map<Integer,String> houseVoteIssueMap =  houseVoteIssuesMap.get(dto.getId());
            if(houseVoteIssueMap != null){
                for(VoteIssue voteIssue : voteIssues){
                    strList.add(houseVoteIssueMap.get(voteIssue.getId()) == null?"未投票":houseVoteIssueMap.get(voteIssue.getId()));
                }
            }
            data.add(strList.toArray(new String[strList.size()]));
        }
        if(data.isEmpty()){
            data.add(new String[]{"无数据"});
        }
        String day = DateUtil.dateToString(new Date(), "yyyy-MM-dd");
        CSVHelper.export(title, data, fileTemp + day  + "投票记录");
        CSVHelper.download(response,fileTemp + day  + "投票记录" + ".csv");
    }

    @Override
    public String getVoteQrCode(Integer voteId) {
        List<Imgs> imgsList = imgsDao.listBySubjectId(voteId,SubjectTypeEnum.VOTE.getCode());
        if(!CollectionUtils.isEmpty(imgsList)){
            Imgs imgs = imgsList.get(0);
            String url = imgs.getUrl();
            byte[] data = FileUtil.getBytes(url);
            return JSON.toJSONString(data);
        }
        return "";
    }

    public void getVoteQrCodeImg(Integer voteId,HttpServletResponse response){
        String url = imgsService.createGrCode(voteId);
        BufferedImage img = new BufferedImage(300, 150, BufferedImage.TYPE_INT_RGB);
        img = FileUtil.getInputStream(url);
        if(img==null){
            throw new RuntimeException("打印图片异常：com.controller.Business_Ctrl.getImg(String, HttpServletResponse)");
        }
        if(img!=null){
            try {
                ImageIO.write(img, "JPEG", response.getOutputStream());
            } catch (IOException e) {
                e.printStackTrace();
                System.out.println("打印异常:com.controller.Business_Ctrl.getImg(String, HttpServletResponse)");
            }
        }

    }

    @Override
    public void createVoteRecord(VoteRecordParams voteRecordQueryParams) {
        House house = houseService.getById(voteRecordQueryParams.getHouseId());
        VoteRecord voteRecord = new VoteRecord();
        BeanUtils.copyProperties(voteRecordQueryParams,voteRecord);
        voteRecord.setCommunityId(house.getCommunityId());
        voteRecord.setArea(house.getArea());
        voteRecord.setHouseId(house.getId());
        voteRecordDao.insert(voteRecord);
    }

    @Override
    public VoteCountInfoDTO countVote(Integer communityId, Integer voteId) {
        VoteCountInfoDTO voteCountInfoDTO = new VoteCountInfoDTO();
        Vote vote = voteDao.getVote(voteId);
        Integer districtOwnerNum = voteMapper.countDistrictOwnerNum(vote.getVoteUnits(),communityId);
        Community community = communityService.getById(communityId);
        CountHouseDTO countHouseDTO = voteMapper.countHouse(communityId);
        countHouseDTO.setCommunityArea(Double.valueOf(community.getBuildingArea()));
        countHouseDTO.setCommunityOwnerNum(community.getHouseholdNum());
        countHouseDTO.setDistrictOwnerNum(districtOwnerNum);
        List<CountVoteDTO> voteDTO = voteMapper.countVote(voteId);
        List<CountVoteDTO> countVoteDTO = voteMapper.countVoteTwo(voteId);
        voteCountInfoDTO.setCountHouse(countHouseDTO);
        voteCountInfoDTO.setCountVote(voteDTO);
        if(new Integer(1).equals(community.getVoteType())){
            voteCountInfoDTO.setCountVoteTwo(countVoteDTO);
        }else{
            voteCountInfoDTO.setCountVoteTwo(voteDTO);
        }
        return voteCountInfoDTO;
    }

    @Override
    public PageInfo listVoteRecord(VoteRecordQueryParams voteRecordQueryParams) {
        Integer voteId = voteRecordQueryParams.getVoteId();
        //投票列表限制
        PageHelper.startPage(voteRecordQueryParams.getPageNum(), voteRecordQueryParams.getPageSize());
        List<VoteRecordDTO> result = voteRecordMapper.recordList(voteRecordQueryParams);
        if(!CollectionUtils.isEmpty(result)){
            for(VoteRecordDTO voteRecordDTO : result){
                voteRecordDTO.setResultStr(VoteResultEnum.getDesc(voteRecordDTO.getResult()));

            }
        }

        return new PageInfo(result);
    }

    @Override
    public List<VoteIssue> listIssues(Integer voteId) {
        return voteIssueDao.listByVote(voteId);
    }
}
