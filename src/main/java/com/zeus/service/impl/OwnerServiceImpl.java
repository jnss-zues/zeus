package com.zeus.service.impl;

import cn.hutool.poi.excel.ExcelReader;
import cn.hutool.poi.excel.ExcelUtil;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.zeus.common.Properties;
import com.zeus.common.StringUtil;
import com.zeus.common.utils.UUIDs;
import com.zeus.dao.OwnerDao;
import com.zeus.domain.HouseTypeEnum;
import com.zeus.domain.entity.Feedback;
import com.zeus.domain.entity.House;
import com.zeus.domain.entity.Owner;
import com.zeus.domain.mapper.FeedbackMapper;
import com.zeus.domain.mapper.HouseMapper;
import com.zeus.domain.mapper.OwnerMapper;
import com.zeus.domain.param.FeedbackHandleParams;
import com.zeus.domain.param.OwnerInfoEditParams;
import com.zeus.domain.param.OwnerInfoQueryParams;
import com.zeus.domain.vo.FeedbackDTO;
import com.zeus.domain.vo.OwnerInfoDTO;
import com.zeus.domain.vo.SimpleDTO;
import com.zeus.service.OwnerService;
import com.zeus.service.WxInfoService;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author yishan@dian.so
 * @version 1.0.0
 * @data 2019/3/3
 */
@Service
@Slf4j
public class OwnerServiceImpl implements OwnerService{

    @Resource
    private HouseMapper houseMapper;

    @Resource
    private FeedbackMapper feedbackMapper;

    @Resource
    private OwnerDao ownerDao;

    @Resource
    private OwnerMapper ownerMapper;

    @Resource
    private Properties properties;

    @Resource
    private WxInfoService wxInfoService;

    @Override
    public List<String> listDistrict(Integer communityId) {
        List<House> list = houseMapper.listByCommunityId(communityId);
        if(CollectionUtils.isEmpty(list)) {
            return Lists.newArrayList();
        }
        Set<String> set = Sets.newHashSet();
        for(House house : list) {
            String district = house.getDistrict();
            set.add(district);
        }
        List<String> result = Lists.newArrayList();
        result.addAll(set);
        Collections.sort(result);
        return result;
    }

    @Override
    public List<SimpleDTO> listBuilding(Integer communityId) {
        List<House> list = houseMapper.listByCommunityId(communityId);
        if(CollectionUtils.isEmpty(list)) {
            return Lists.newArrayList();
        }
        Set<Integer> set = Sets.newHashSet();
        List<SimpleDTO> result = Lists.newArrayList();
        for(House house : list) {
            Integer buildingNum = house.getBuildingNum();
            if(set.add(buildingNum)) {
                SimpleDTO simpleDTO = new SimpleDTO();
                simpleDTO.setId(buildingNum);
                simpleDTO.setText(buildingNum + "幢");
                result.add(simpleDTO);
            }
        }
        Collections.sort(result);
        return result;
    }

    @Override
    public List<SimpleDTO> listUnit(Integer communityId) {
        List<House> list = houseMapper.listByCommunityId(communityId);
        if(CollectionUtils.isEmpty(list)) {
            return Lists.newArrayList();
        }
        Set<Integer> set = Sets.newHashSet();
        List<SimpleDTO> result = Lists.newArrayList();
        for(House house : list) {
            Integer unit = house.getUnit();
            if(set.add(unit)) {
                SimpleDTO simpleDTO = new SimpleDTO();
                simpleDTO.setId(unit);
                simpleDTO.setText(unit + "单元");
                result.add(simpleDTO);
            }
        }
        Collections.sort(result);
        return result;
    }

    @Override
    public JSONObject listOwnerInfo(OwnerInfoQueryParams ownerInfoQuery) {
        Integer count = houseMapper.countByCondition(ownerInfoQuery.getCommunityId(),
                ownerInfoQuery.getDistrict(),
                ownerInfoQuery.getBuildingNum(),ownerInfoQuery.getUnit(),null);
        List<House> list = houseMapper.listByCondition(ownerInfoQuery.getCommunityId(),
                ownerInfoQuery.getOffset(),ownerInfoQuery.getPageSize(),ownerInfoQuery.getDistrict(),
                ownerInfoQuery.getBuildingNum(),ownerInfoQuery.getUnit(),null);
        List<OwnerInfoDTO> result = Lists.newArrayList();
        if(!CollectionUtils.isEmpty(list)) {
            list.forEach(house -> {
                result.add(house.convert());
            });
        }

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("count",ownerInfoQuery.getTotalCount(count));
        jsonObject.put("list",result);
        return jsonObject;
    }

    @Override
    @Transactional
    public String editOwnerInfo(OwnerInfoEditParams ownerInfoEditParams) {
        String houseId = ownerInfoEditParams.getHouseId();
        String names = ownerInfoEditParams.getNames();
        String mobiles = ownerInfoEditParams.getMobiles();
        if(houseId == null || StringUtil.isEmpty(names) || StringUtil.isEmpty(mobiles)) {
            return "入参为空";
        }
        House house = houseMapper.selectByPrimaryKey(houseId);
        if(house == null) {
            return "业主信息不存在";
        }
        names = names.replaceAll("；",";");
        mobiles = mobiles.replaceAll("；",";");
        //String[] arrName = names.split(";");
        //String[] arrMobile = mobiles.split(";");
        /*if(arrName.length != arrMobile.length) {
            return "导入姓名和电话必须一一对应";
        }*/

        house.setNames(names);
        house.setMobiles(mobiles);
        house.setDistrict(ownerInfoEditParams.getDistrict());
        house.setBuildingNum(ownerInfoEditParams.getBuildingNum());
        house.setUnit(ownerInfoEditParams.getUnit());
        house.setRoomNum(ownerInfoEditParams.getRoomNum());
        house.setArea(ownerInfoEditParams.getArea());
        house.setHouseType(ownerInfoEditParams.getHouseType());
        String msg = checkMobiles(mobiles);
        if(msg != null) {
            return msg;
        }
        /*String exsitMsg = exsitMobiles(mobiles,houseId);
        if(exsitMsg != null) {
            return exsitMsg;
        }*/
        houseMapper.updateByPrimaryKeySelective(house);
        /*ownerMapper.deleteByHouseId(houseId);
        List<Owner> owners = getOwners(houseId,house.getNames(),house.getMobiles());
        if(!CollectionUtils.isEmpty(owners)) {
            ownerMapper.batchInsert(owners);
        }*/
        return null;
    }

    public String exsitMobiles(String mobiles,String houseId) {
        List<String> list = Lists.newArrayList();
        String[] mobileArr = mobiles.split(";");
        for(String mobile:mobileArr) {
            list.add(mobile.trim());
        }
        Integer count = ownerMapper.getOwnerByMobile(houseId,list);
        if(count != null && count > 0) {
            return "手机号已存在";
        }
        return null;
    }

    @Override
    @Transactional
    public Boolean importOwnerInfo(Integer communityId,String fileName, MultipartFile mFile) {
        String url = "/Users/yishan/temp";
        /*String url = "/Users/yishan/Desktop";
        String fileName = "111";*/
        File file = new File(url,fileName+".xls");
        try {
            FileUtils.copyInputStreamToFile(mFile.getInputStream(), file);
        } catch (IOException e) {
            e.printStackTrace();
        }

        //2读取file
        ExcelReader excelReader = ExcelUtil.getReader(file);

        List<Map<String, Object>> list = excelReader.readAll();
        excelReader.close();

        if(CollectionUtils.isEmpty(list)) {
            return null;
        }
        //3删除原有小区数据
        houseMapper.deleteByCommunityId(communityId);

        List<House> houses = Lists.newArrayList();
        //List<Owner> owners = Lists.newArrayList();
        int index = 0;
        Map<Integer,String> map = Maps.newHashMap();
        for(Map<String, Object> stringObjectMap :list) {
            index++;
            try {
                if(stringObjectMap.get("序号") == null) {
                    map.put(index,"序号不可为空");
                    continue;
                }
                Integer no = Integer.valueOf(stringObjectMap.get("序号").toString().trim());

                if(stringObjectMap.get("区") == null) {
                    map.put(index,"区不可为空");
                    continue;
                }
                String district = stringObjectMap.get("区").toString().trim();

                if(stringObjectMap.get("幢") == null) {
                    map.put(index,"幢不可为空");
                    continue;
                }
                Integer buildingNum = Integer.valueOf(stringObjectMap.get("幢").toString().trim());

                if(stringObjectMap.get("单元") == null) {
                    map.put(index,"单元不可为空");
                    continue;
                }
                Integer unit =  Integer.valueOf(stringObjectMap.get("单元").toString().trim());

                if(stringObjectMap.get("室") == null) {
                    map.put(index,"室不可为空");
                    continue;
                }
                Integer roomNum =  Integer.valueOf(stringObjectMap.get("室").toString().trim());

                if(stringObjectMap.get("面积") == null) {
                    map.put(index,"面积不可为空");
                    continue;
                }
                Float area =  Float.valueOf(stringObjectMap.get("面积").toString().trim());

                if(stringObjectMap.get("房屋类型") == null) {
                    map.put(index,"房屋类型不可为空");
                    continue;
                }
                String houseType =  stringObjectMap.get("房屋类型").toString().trim();

                if(stringObjectMap.get("业主姓名（多个用;隔开）") == null) {
                    map.put(index,"业主姓名不可为空");
                    continue;
                }
                String names = stringObjectMap.get("业主姓名（多个用;隔开）").toString().trim();
                names = names.replaceAll("；",";");

                if(stringObjectMap.get("业主电话（多个用;隔开）") == null) {
                    map.put(index,"业主电话不可为空");
                    continue;
                }
                String mobiles = stringObjectMap.get("业主电话（多个用;隔开）").toString().trim();
                mobiles = mobiles.replaceAll("；",";");

                //String[] arrName = names.split(";");
                //String[] arrMobile = mobiles.split(";");
                /*if(arrName.length != arrMobile.length) {
                    map.put(index,"导入姓名和电话必须一一对应");
                    continue;
                }*/


               /* List<House> houseList = houseMapper.listByCondition(communityId,
                        0,1,district,
                        buildingNum,unit,roomNum);

                String msg = exsitMobiles(mobiles,CollectionUtils.isEmpty(houseList) ? null : houseList.get(0).getId());
                if(msg != null) {
                    map.put(index,msg);
                    continue;
                }*/

                House house = new House();
                house.setNo(no);
                house.setDistrict(district);
                house.setBuildingNum(buildingNum);
                house.setUnit(unit);
                house.setRoomNum(roomNum);
                house.setArea(area);
                house.setHouseType(HouseTypeEnum.getCode(houseType));
                house.setNames(names);
                house.setMobiles(mobiles);
                String checkMsg = checkMobiles(house.getMobiles());
                if(checkMsg != null) {
                    map.put(index,checkMsg);
                    continue;
                }
                house.setCommunityId(communityId);
                house.setId(UUIDs.getUUID());
                houses.add(house);
                /*House house;
                String uuid;
                if(CollectionUtils.isEmpty(houseList)) {
                    house = new House();
                    house.setDistrict(district);
                    house.setBuildingNum(buildingNum);
                    house.setUnit(unit);
                    house.setRoomNum(roomNum);
                    house.setArea(area);
                    house.setHouseType(HouseTypeEnum.getCode(houseType));
                    house.setNames(names);
                    house.setMobiles(mobiles);
                    String checkMsg = checkMobiles(house.getMobiles());
                    if(checkMsg != null) {
                        map.put(index,checkMsg);
                        continue;
                    }
                    house.setCommunityId(communityId);
                    uuid = UUIDs.getUUID();
                    house.setId(uuid);
                    houses.add(house);
                }else {
                    house = houseList.get(0);
                    uuid = house.getId();
                    house.setNames(String.format("%s;%s",house.getNames(),names));
                    house.setMobiles(String.format("%s;%s",house.getMobiles(),mobiles));
                    String checkMsg = checkMobiles(house.getMobiles());
                    if(checkMsg != null) {
                        map.put(index,checkMsg);
                        continue;
                    }
                    houseMapper.updateByPrimaryKeySelective(house);
                }
                owners.addAll(getOwners(uuid,names,mobiles));*/
            }catch (Exception e){
                map.put(index,"插入失败");
                continue;
            }
        }
        boolean result = true;
        if(!CollectionUtils.isEmpty(map)) {
            result = false;
            com.zeus.common.utils.ExcelUtil.createExcel(url,fileName,map);
        }

        if(!CollectionUtils.isEmpty(houses)) {
            houseMapper.batchInsert(getMixHouses(houses));
        }
        /*if(!CollectionUtils.isEmpty(owners)) {
            ownerMapper.batchInsert(owners);
        }*/

        return result;
    }

    public static List<House> getMixHouses(List<House> houses) {
        List<House> houseList = Lists.newArrayList();
        if(CollectionUtils.isEmpty(houses)) {
            return houseList;
        }
        Map<String,House> map = Maps.newHashMap();
        for(House house :houses) {
            String district = house.getDistrict();
            Integer buildingNum = house.getBuildingNum();
            Integer unit = house.getUnit();
            Integer roomNum = house.getRoomNum();
            String key = district.trim()+"-"+buildingNum+"-"+unit+"-"+roomNum;
            House house1 = map.get(key);
            if(house1 == null) {
                map.put(key,house);
            }else {
                house1.setNames(house1.getNames()+";"+house.getNames());
                house1.setMobiles(house1.getMobiles()+";"+house.getMobiles());
            }
        }
        for(Entry<String,House> entry :map.entrySet()) {
            houseList.add(entry.getValue());
        }
        return houseList;
    }

    private String checkMobiles(String mobiles) {
        String[] arr = mobiles.split(";");
        List<String> list = Arrays.asList(arr);
        /*Set<String> set = Sets.newHashSet();
        set.addAll(list);
        if(list.size() != set.size()) {
            return "电话号码重复";
        }*/
        for(String mobile: list) {
            if(mobile.trim().length() != 11) {
                return "电话格式不正确";
            }
        }
        return null;
    }

    private List<Owner> getOwners(String houseId, String names, String mobiles) {
        List<Owner> list = Lists.newArrayList();
        if(StringUtil.isEmpty(names) || StringUtil.isEmpty(mobiles)) {
            return list;
        }
        String[] nameArr = names.split(";");
        String[] mobileArr = mobiles.split(";");
        Integer nameArrLength = nameArr.length;
        Integer mobileArrLength = mobileArr.length;
        if(nameArrLength > mobileArrLength) {
            for(int i = 0;i < nameArrLength;i++) {
                Owner owner = new Owner();
                owner.setHouseId(houseId);
                owner.setName(nameArr[i].trim());
                if(i > mobileArrLength-1) {
                    owner.setMobile(mobileArr[mobileArrLength-1].trim());
                }else {
                    owner.setMobile(mobileArr[i].trim());
                }
                list.add(owner);
            }
        }else {
            for(int i = 0;i < mobileArrLength;i++) {
                Owner owner = new Owner();
                owner.setHouseId(houseId);
                if(i > nameArrLength-1) {
                    owner.setName("");
                }else {
                    owner.setName(nameArr[i].trim());
                }
                owner.setMobile(mobileArr[i].trim());
                list.add(owner);
            }
        }

        return list;
    }

    @Override
    public Boolean handleOwnerFeedback(FeedbackHandleParams feedbackHandleParams) {
        Feedback feedback = feedbackMapper.selectByPrimaryKey(feedbackHandleParams.getFeedbackId());
        //feedback.setStatus(feedbackHandleParams.getStatus());
        feedback.setStatus(1);
        feedback.setUpdateTime(new Date());

        feedback.setOperator(feedbackHandleParams.getUserId());
        feedback.setOperatorName(feedbackHandleParams.getUserName());
        feedbackMapper.updateByPrimaryKeySelective(feedback);
        return true;
    }

    @Override
    public JSONObject listFeedback(OwnerInfoQueryParams ownerInfoQuery) {
        List<Feedback> feedbacks = feedbackMapper.listByCondition(ownerInfoQuery.getCommunityId(),ownerInfoQuery.getOffset(),ownerInfoQuery.getPageSize(),
                ownerInfoQuery.getDistrict(),ownerInfoQuery.getBuildingNum(),ownerInfoQuery.getUnit(),ownerInfoQuery.getStatus());
        Integer count = feedbackMapper.countByCondition(ownerInfoQuery.getCommunityId(),
                ownerInfoQuery.getDistrict(),ownerInfoQuery.getBuildingNum(),ownerInfoQuery.getUnit(),ownerInfoQuery.getStatus());
        List<FeedbackDTO> list = Lists.newArrayList();
        if(!CollectionUtils.isEmpty(feedbacks)) {
            feedbacks.forEach(feedback -> {
                list.add(feedback.convert());
            });
        }
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("count",ownerInfoQuery.getTotalCount(count));
        jsonObject.put("list",list);
        return jsonObject;
    }

    @Override
    public Owner getById(Integer id) {
        return ownerDao.getById(id);
    }

    @Override
    public Boolean addOwnerFeedback(FeedbackHandleParams feedbackHandleParams) {
        /*Owner owner = ownerMapper.getByMobile(feedbackHandleParams.getMobile());
        if(owner == null) {
            return false;
        }*/
        /*List<House> houses = houseMapper.listByNamesAndMobilesLike(feedbackHandleParams.getFeedbackOwnerName(),feedbackHandleParams.getMobile());
        if(CollectionUtils.isEmpty(houses)) {
            return false;
        }*/
        //House house = houses.get(0);
        Feedback feedback = new Feedback();
        feedback.setCommunityId(feedbackHandleParams.getCommunityId());
        feedback.setDistrict(feedbackHandleParams.getDistrict());
        feedback.setBuildingNum(feedbackHandleParams.getBuildingNum());
        feedback.setUnit(feedbackHandleParams.getUnit());
        feedback.setRoomNum(feedbackHandleParams.getRoomNum());
        feedback.setFeedbackOwnerName(feedbackHandleParams.getFeedbackOwnerName());
        feedback.setMobile(feedbackHandleParams.getMobile());
        feedback.setStatus(0);
        feedback.setQuestion(feedbackHandleParams.getQuestion());
        feedback.setFeedbackTime(new Date());
        feedback.setCreateTime(new Date());
        feedbackMapper.insertSelective(feedback);
        return true;
    }

    @Override
    public House queryById(String houseId) {
        return houseMapper.selectByPrimaryKey(houseId);
    }

    @Override
    public Owner getByMobile(String mobile) {
        return ownerDao.getByMobile(mobile);
    }

    @Override
    public List<Owner> listByMobile(String mobile) {
        return ownerDao.listByMobile(mobile);
    }

    public static void main(String[] args) {
       /* List<House> list = Lists.newArrayList();
        House house = new House();
        house.setDistrict("测试区");
        house.setBuildingNum(1);
        house.setUnit(1);
        house.setRoomNum(1);
        house.setNames("aa");
        house.setMobiles("123");
        list.add(house);
        House house1 = new House();
        house1.setDistrict("测试区");
        house1.setBuildingNum(1);
        house1.setUnit(1);
        house1.setRoomNum(1);
        house1.setNames("aa");
        house1.setMobiles("123");
        list.add(house1);
        House house2 = new House();
        house2.setDistrict("测试区1");
        house2.setBuildingNum(1);
        house2.setUnit(2);
        house2.setRoomNum(2);
        house2.setNames("aa");
        house2.setMobiles("123");
        list.add(house2);

        List<House> list1 = getMixHouses(list);
        System.out.println(JSONObject.toJSONString(list1));*/

        System.out.println(String.format("%03d",12));
        System.out.println(String.format("%03d",1));

        System.out.println(String.format("%03d",122));
        System.out.println(String.format("%03d",1222));


    }
}
