package com.zeus.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.zeus.common.Properties;
import com.zeus.common.utils.HttpClientManager;
import com.zeus.dao.WxInfoDao;
import com.zeus.domain.dto.WxInfoDTO;
import com.zeus.domain.entity.WxInfo;
import com.zeus.service.WxInfoService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

@Service
public class WxInfoServiceImpl implements WxInfoService {

    @Resource
    private WxInfoDao wxInfoDao;

    @Resource
    private Properties properties;

    @Override
    public WxInfo getByOpenid(String openId) {
        WxInfo wxInfo = wxInfoDao.getByOpenId(openId);
        return wxInfo;
    }

    @Override
    public void saveWxInfo(WxInfo wxInfo) {
        wxInfoDao.insert(wxInfo);
    }

    @Override
    public void updateWxInfo(WxInfo wxInfo) {
        wxInfoDao.update(wxInfo);
    }

    @Override
    public WxInfo getByOwner(Integer ownerId) {
        return wxInfoDao.getByOwner(ownerId);
    }

    @Override
    public WxInfo getByMobile(String mobile) {
        return wxInfoDao.getByMobile(mobile);
    }

    @Override
    public void deleteByOwner(Integer ownerId) {
        wxInfoDao.deleteByOwner(ownerId);
    }

    public String getAccessToken() {
        String appId = properties.appId;
        String secret = properties.getSecret;
        String url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=" +
                appId + "&secret=" + secret;
        try {
            String data = HttpClientManager.getUrlData(url);
            JSONObject object = JSON.parseObject(data);
            if (object.get("access_token") != null) {
                String accessToken = (String) object.get("access_token");
                return accessToken;
            }
        } catch (Exception e) {

        }
        return null;
    }

    public Map<String,Object> getOpenId(String code) throws Exception {
        //微信公众号的APPID和APPSECRET
        String url="https://api.weixin.qq.com/sns/oauth2/access_token?appid=" + properties.appId+
                "&secret=" +properties.getSecret+
                "&code=" +code+
                "&grant_type=authorization_code";
        String result = HttpClientManager.getUrlData(url);
        Map<String,Object> data = JSON.parseObject(result);
        return data==null?new HashMap<>():data;
    }


    public WxInfoDTO getWxInfo(String openid, String token) throws Exception {
        //获取信息
        String infoUrl="https://api.weixin.qq.com/cgi-bin/user/info?access_token=" +token+
                "&openid=" +openid+
                "&lang=zh_CN";
        String infoResult = HttpClientManager.getUrlData(infoUrl);
        WxInfoDTO wxInfoDTO = JSON.parseObject(infoResult,WxInfoDTO.class);
        return wxInfoDTO;
    }
}
