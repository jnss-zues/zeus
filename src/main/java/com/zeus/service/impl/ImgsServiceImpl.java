package com.zeus.service.impl;

import com.google.zxing.WriterException;
import com.zeus.common.Constant;
import com.zeus.common.Properties;
import com.zeus.common.QrCodeCreateUtil;
import com.zeus.common.enums.ErrorEnum;
import com.zeus.dao.ImgsDao;
import com.zeus.domain.entity.Imgs;
import com.zeus.domain.vo.BaseVo;
import com.zeus.service.ImgsService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;

@Service
@Slf4j
public class ImgsServiceImpl implements ImgsService {

    @Resource
    private Properties properties;

    @Resource
    private ImgsDao imgsDao;

    @Override
    @Transactional
    public BaseVo uploadImgs(MultipartFile[] imgs, Integer type, String userId, String tempId) throws IOException {
        if(imgs != null){
            for(MultipartFile file : imgs){
                String uploadResult= imgUpload(file,null,userId);
                log.info("上传结果：" + uploadResult);
                if(!StringUtils.equals(uploadResult,Constant.ERROR)){
                    Imgs img = new Imgs();
                    img.setUrl(uploadResult);
                    img.setCreateTime(new Date());
                    img.setUpdateTime(new Date());
                    img.setType(type);
                    img.setTempId(tempId);
                    imgsDao.add(img);
                }else{
                    BaseVo.errorCreate(ErrorEnum.PARAMS_NULL.GOODS_UPLOAD_ERROR);
                }
            }
        }
        return BaseVo.create(null);
    }

    @Override
    public String createGrCode(Integer voteId ) {
        try {
            String content = properties.voteShareUrl + "?voteId=" + voteId;
            String url = "https://open.weixin.qq.com/connect/oauth2/authorize?" +
                    "appid=" + properties.appId +
                    "&redirect_uri="+ content +
                    "&response_type=code&scope=snsapi_base&state=STATE";
            String imgName = voteId + ".jpg";
            String fileUrl = properties.uploadPathGrcodeVote + imgName;
            File file = new File(properties.uploadPathGrcodeVote);
            // 如果文件路径所对应的文件存在，并且是一个文件，则直接删除
            if (!file.exists()) {
                file.mkdirs();
            }
            QrCodeCreateUtil.createQrCode(new FileOutputStream(new File(fileUrl)),url,1500,"JPEG");
            return fileUrl;
        } catch (WriterException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String createNoticeGrCode(Integer noticeId ) {
        try {
            String content = properties.noticeShareUrl + "?noticeId=" + noticeId;
            String imgName = noticeId + ".jpg";
            String fileUrl = properties.uploadPathGrcodeNotice + imgName;
            File file = new File(properties.uploadPathGrcodeNotice);
            // 如果文件路径所对应的文件存在，并且是一个文件，则直接删除
            if (!file.exists()) {
                file.mkdirs();
            }
            QrCodeCreateUtil.createQrCode(new FileOutputStream(new File(fileUrl)),content,1500,"JPEG");
            return fileUrl;
        } catch (WriterException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void saveGrCode(Integer voteId) {
        try {
            String content = properties.voteShareUrl + "?voteId=" + voteId;
            String imgName = voteId + ".jpg";
            String fileUrl = properties.uploadPathGrcodeVote + imgName;
            File file = new File(properties.uploadPathGrcodeVote);
            // 如果文件路径所对应的文件存在，并且是一个文件，则直接删除
            if (!file.exists()) {
                file.mkdirs();
            }
            QrCodeCreateUtil.createQrCode(new FileOutputStream(new File(fileUrl)),content,900,"JPEG");
            Imgs imgs = new Imgs();
            imgs.setUrl(fileUrl);
            imgs.setSubjectId(voteId);
            imgs.setType(2);
//            imgs.setUploadUserId();
            imgsDao.add(imgs);
        } catch (WriterException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String imgUpload(MultipartFile img , HttpServletRequest request, String userId) throws IOException {
        //如果文件不为空，写入上传路径
        if(!img.isEmpty()) {
            //上传文件路径从配置文件中读取
            String path = properties.uploadImgsPath+"/notice";
            //上传文件名
            long time = new Date().getTime();
            String filename = time + img.getOriginalFilename();
            File filepath = new File(path + File.separator + userId + "",filename);
            //判断路径是否存在，如果不存在就创建一个
            if (!filepath.getParentFile().exists()) {
                filepath.getParentFile().mkdirs();
            }
            //将上传文件保存到一个目标文件当中
            img.transferTo(new File(filepath + ""));
            return filepath + "";
        } else {
            return Constant.ERROR;
        }
    }
}
