package com.zeus.service.impl;

import com.zeus.dao.CommunityDao;
import com.zeus.domain.entity.Community;
import com.zeus.domain.mapper.CommunityMapper;
import com.zeus.domain.param.CommunityParams;
import com.zeus.domain.vo.CommunityDTO;
import com.zeus.service.CommunityService;
import java.util.Date;
import java.util.List;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;

/**
 * @author yishan@dian.so
 * @version 1.0.0
 * @data 2019/3/2
 */
@Service
public class CommunityServiceImpl implements CommunityService{

    @Resource
    private CommunityMapper communityMapper;

    @Resource
    private CommunityDao communityDao;

    @Override
    public CommunityDTO detail(Integer communityId) {
        Community community = communityMapper.selectByPrimaryKey(communityId);
        if(community == null) {
            return new CommunityDTO();
        }
        return community.convert();
    }

    @Override
    public Boolean modify(CommunityParams communityParams) {
        Community community = communityMapper.selectByPrimaryKey(communityParams.getCommunityId());
        community.setBuildingArea(communityParams.getBuildingArea());
        community.setHouseholdNum(communityParams.getHouseholdNum());
        community.setSignature(communityParams.getSignature());
        community.setVoteType(communityParams.getVoteType());
        community.setUpdateTime(new Date());
        communityMapper.updateByPrimaryKeySelective(community);
        return true;
    }

    @Override
    public Community getById(Integer id) {
        return communityMapper.selectByPrimaryKey(id);
    }

    @Override
    public List<Community> listAll() {
        return communityDao.listAll();
    }
}
