package com.zeus.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.zeus.common.Properties;
import com.zeus.common.utils.FileUtil;
import com.zeus.dao.ImgsDao;
import com.zeus.dao.NoticeDao;
import com.zeus.dao.UserDao;
import com.zeus.domain.SubjectTypeEnum;
import com.zeus.domain.dto.NoticeDetailDTO;
import com.zeus.domain.entity.Imgs;
import com.zeus.domain.entity.Notice;
import com.zeus.domain.param.NoticeParams;
import com.zeus.service.ImgsService;
import com.zeus.service.NoticeService;
import com.zeus.domain.entity.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Slf4j
public class NoticeServiceImpl implements NoticeService {

    @Resource
    private NoticeDao noticeDao;

    @Resource
    private ImgsDao imgsDao;

    @Resource
    private UserDao userDao;

    @Resource
    private Properties properties;

    @Resource
    private ImgsService imgsService;

    @Override
    @Transactional
    public void create(NoticeParams noticeParams) {
        noticeDao.insert(noticeParams);
        imgsDao.updateTemp(noticeParams.getId(),noticeParams.getTempId());
    }

    @Override
    public void update(NoticeParams noticeParams) {
        noticeDao.update(noticeParams);
        imgsDao.updateTemp(noticeParams.getId(),noticeParams.getTempId());
    }

    @Override
    public PageInfo listNotice(NoticeParams noticeParams) {
        PageHelper.startPage(noticeParams.getPageNum(), noticeParams.getPageSize());
        List<Notice> notices = noticeDao.listNotice(noticeParams);
        return new PageInfo(notices);
    }


    public List<NoticeDetailDTO> setPublisherName(List<Notice> notices,String community){
        List<Integer> userIds = notices.stream().map(Notice::getPublisher).distinct().collect(Collectors.toList());
        List<NoticeDetailDTO> dtos = new ArrayList<>();
        if(CollectionUtils.isEmpty(userIds)){
            return dtos;
        }
        List<User> users = userDao.listByIds(userIds);
        for(Notice notice : notices){
            NoticeDetailDTO noticeDetailDTO = new NoticeDetailDTO();
            BeanUtils.copyProperties(notice,noticeDetailDTO);
            Optional<User> data = users.stream()
                    .filter(d -> d.getId().equals(notice.getPublisher())).findFirst();
            if(notice.getPublisher() != 0){
                noticeDetailDTO.setPublisherName(data.isPresent()?data.get().getName():"");
            }else{
                noticeDetailDTO.setPublisherName(community);
            }

            dtos.add(noticeDetailDTO);
        }
        return dtos;

    }

    @Override
    public void getQrCodeImg(Integer noticeId, HttpServletResponse response) {
        String url = imgsService.createNoticeGrCode(noticeId);
        BufferedImage img = new BufferedImage(300, 150, BufferedImage.TYPE_INT_RGB);
        img = FileUtil.getInputStream(url);
        if(img==null){
            throw new RuntimeException("打印图片异常：com.controller.Business_Ctrl.getImg(String, HttpServletResponse)");
        }
        if(img!=null){
            try {
                ImageIO.write(img, "JPEG", response.getOutputStream());
            } catch (IOException e) {
                e.printStackTrace();
                System.out.println("打印异常:com.controller.Business_Ctrl.getImg(String, HttpServletResponse)");
            }
        }
    }

    @Override
    public void delete(NoticeParams noticeParams) {
        noticeDao.delete(noticeParams.getId());

    }

    @Override
    public NoticeDetailDTO getDetail(Integer id , HttpServletRequest request) {
        Notice notice = noticeDao.getInfo(id);
        NoticeDetailDTO noticeDetailDTO = new NoticeDetailDTO();
        BeanUtils.copyProperties(notice,noticeDetailDTO);
        if(notice.getPublisher() == 0){
            String signature = (String) request.getSession().getAttribute("signature");
            noticeDetailDTO.setPublisherName(signature);
        }else{
            User user = userDao.getById(notice.getPublisher());
            if(user != null){
                noticeDetailDTO.setPublisherName(user.getName());
            }
        }
        List<Imgs> imgs =  imgsDao.listBySubjectId(id,SubjectTypeEnum.NOTICE.getCode());
        for(Imgs img : imgs){
            img.setUrl(img.getUrl().replace(properties.uploadPath,""));
        }
        noticeDetailDTO.setImgList(imgs);
        return noticeDetailDTO;
    }
}
