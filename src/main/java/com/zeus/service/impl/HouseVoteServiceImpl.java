package com.zeus.service.impl;

import cn.hutool.log.level.WarnLog;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.zeus.dao.HouseDao;
import com.zeus.dao.HouseVoteDao;
import com.zeus.dao.HouseVoteIssueDao;
import com.zeus.domain.VoteRecordStatusEnum;
import com.zeus.domain.VoteResultEnum;
import com.zeus.domain.dto.HouseVoteDTO;
import com.zeus.domain.dto.VoteRecordDTO;
import com.zeus.domain.entity.House;
import com.zeus.domain.entity.HouseVote;
import com.zeus.domain.entity.HouseVoteIssue;
import com.zeus.domain.entity.VoteIssue;
import com.zeus.domain.param.VoteParams;
import com.zeus.domain.param.VoteRecordQueryParams;
import com.zeus.service.HouseVoteService;
import com.zeus.service.VoteService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;

@Service
public class HouseVoteServiceImpl implements HouseVoteService {

    @Resource
    private HouseVoteDao houseVoteDao;

    @Resource
    private HouseDao houseDao;

    @Resource
    private HouseVoteIssueDao houseVoteIssueDao;

    @Resource
    private VoteService voteService;

    private DecimalFormat df = new DecimalFormat("0000");

    @Override
    public void insetBatch(VoteParams voteParams) {
        String voteUnits = voteParams.getVoteUnits();
        List<House> houses = houseDao.listByDistrict(voteUnits, voteParams.getCommunityId());
        List<HouseVote> houseVotes = new ArrayList<>();
//        DecimalFormat df = new DecimalFormat("0000");
        for (House house : houses) {
            HouseVote houseVote = new HouseVote();
            BeanUtils.copyProperties(house, houseVote);
            houseVote.setId(null);
            houseVote.setVoteId(voteParams.getId());
            houseVote.setVoteNo(df.format(house.getNo()));
            houseVotes.add(houseVote);
        }
        houseVoteDao.insertBatch(houseVotes);
    }

    @Override
    public PageInfo ListByParam(VoteRecordQueryParams voteRecordQueryParams) {
        PageHelper.startPage(voteRecordQueryParams.getPageNum(), voteRecordQueryParams.getPageSize());
        List<HouseVote> result = houseVoteDao.listByParam(voteRecordQueryParams);
        if (!CollectionUtils.isEmpty(result)) {
            for (HouseVote HouseVote : result) {
                HouseVote.setResultStr(VoteResultEnum.getDesc(HouseVote.getResult()));
                HouseVote.setStatusStr(VoteRecordStatusEnum.getDesc(HouseVote.getStatus()));
            }
        }
        return new PageInfo(result);
    }

    @Override
    public List<HouseVote> ListAllByParam(VoteRecordQueryParams voteRecordQueryParams) {
        List<HouseVote> result = houseVoteDao.listByParam(voteRecordQueryParams);
        return result;
    }

    @Override
    public List<HouseVote> ListAllMobile(String mobile, Integer voteId) {
        return houseVoteDao.listByMobile(mobile, voteId);
    }

    @Override
    public void update(HouseVote houseVote) {
        houseVoteDao.update(houseVote);
    }

    @Override
    public void affirm(String mobile, Integer voteId) {
        houseVoteDao.affirm(mobile, voteId);
    }

    @Override
    public HouseVote getById(Integer id) {
        return houseVoteDao.getById(id);
    }

    @Override
    public List<HouseVoteIssue> listIssues(Integer HouseVoteId) {
        return houseVoteIssueDao.listByHouseVote(HouseVoteId);
    }

    @Override
    public void insertHouseVoteIssue(HouseVote houseVote, List<HouseVoteIssue> results) {
        Integer HouseVoteId = houseVote.getId();
        Integer voteId = houseVote.getVoteId();
        List<VoteIssue> houseVoteIssues = voteService.listIssues(voteId);
        Map<Integer, VoteIssue> houseVoteIssueMap = houseVoteIssues.stream()
                .collect(Collectors.toMap(VoteIssue::getId, n -> n, (k1, k2) -> k1));
        for (HouseVoteIssue houseVoteIssue : results) {
            VoteIssue voteIssue = houseVoteIssueMap.get(houseVoteIssue.getVoteIssueId());
            if (voteIssue == null) {
                continue;
            }
            houseVoteIssue.setCreateTime(new Date());
            houseVoteIssue.setDeleted(0);
            houseVoteIssue.setHouseVoteId(HouseVoteId);
            houseVoteIssue.setIssue(voteIssue.getIssue());
            houseVoteIssue.setVoteId(voteId);
            houseVoteIssue.setVoteTime(new Date());
            houseVoteIssueDao.insert(houseVoteIssue);
        }


    }

    @Override
    @Transactional
    public void vote(HouseVote houseVote, List<HouseVoteIssue> results) {
        this.update(houseVote);
        this.insertHouseVoteIssue(houseVote, results);

    }

    @Override
    public void insert(House house, Integer voteId) {
        HouseVote houseVote = new HouseVote();
        BeanUtils.copyProperties(house, houseVote);
        houseVote.setId(null);
        houseVote.setVoteId(voteId);
        houseVote.setVoteNo(df.format(house.getNo()));
        houseVoteDao.insert(houseVote);
    }

    @Override
    public HouseVote getByNo(Integer no, Integer voteId) {
        return houseVoteDao.getByNo(df.format(no),voteId);
    }


}
