package com.zeus.service.impl;

import com.zeus.dao.HouseDao;
import com.zeus.domain.entity.House;
import com.zeus.service.HouseService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class HouseServiceImpl implements HouseService {

    @Resource
    private HouseDao houseDao;

    @Override
    public House getById(String id) {
        return houseDao.getById(id);
    }

    @Override
    public House getByNo(Integer no, Integer communityId) {
        return houseDao.getByNo(no,communityId);
    }
}
