package com.zeus.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.google.common.collect.Lists;
import com.zeus.dao.UserDao;
import com.zeus.domain.dto.UserInfoDTO;
import com.zeus.domain.entity.Community;
import com.zeus.domain.entity.User;
import com.zeus.domain.mapper.UserMapper;
import com.zeus.domain.param.UserParams;
import com.zeus.domain.param.UserQueryParams;
import com.zeus.domain.vo.CommunityDTO;
import com.zeus.domain.vo.UserDTO;
import com.zeus.service.CommunityService;
import com.zeus.service.UserService;
import java.util.Date;
import java.util.List;
import javax.annotation.Resource;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

/**
 * @author yishan@dian.so
 * @version 1.0.0
 * @data 2019/3/2
 */
@Service
public class UserServiceImpl implements UserService{

    @Resource
    private UserMapper userMapper;

    @Resource
    private UserDao userDao;

    @Resource
    private CommunityService communityService;

    @Override
    public Boolean delete(Integer userId) {
        userMapper.deleteById(userId);
        return true;
    }

    @Override
    public String add(UserParams userParams) {
        String mobile = userParams.getMobile().trim();
        if(mobile.length() != 11) {
            return "手机格式不正确,请更改手机格式重试";
        }
        Integer count = userMapper.getByMobile(mobile,userParams.getCommunityId());
        if(count != null && count > 0) {
            return "手机号已存在,请更改手机号重试";
        }
        User user = new User();
        user.setCreateTime(new Date());
        user.setDeleted(0);
        user.setName(userParams.getName());
        user.setMobile(mobile);
        user.setRoom(userParams.getRoom());
        user.setDuty(userParams.getDuty());
        user.setCommunityId(userParams.getCommunityId());
        user.setPassword("yg"+userParams.getMobile());
        user.setLoginName(userParams.getMobile());
        userMapper.insertSelective(user);
        return null;
    }

    @Override
    public JSONObject listUser(UserQueryParams userQueryParams) {

        List<User> users = userMapper.selectByCommunityId(userQueryParams.getCommunityId(),userQueryParams.getOffset(),userQueryParams.getPageSize());
        Integer count = userMapper.countByCommunityId(userQueryParams.getCommunityId());

        List<UserDTO> voList = Lists.newArrayList();
        if(!CollectionUtils.isEmpty(users)) {
            users.forEach(user -> {
                voList.add(user.convert());
            });
        }

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("list",voList);
        jsonObject.put("count",userQueryParams.getTotalCount(count));
        return jsonObject;
    }

    @Override
    public User login(String userName, String password,Integer communityId) {
        User user = userDao.login(userName,password,communityId);
        return user;
    }

    @Override
    public User getByMobile(String mobile, Integer communityId) {
        return userDao.getByMobile(mobile,communityId);
    }

    @Override
    public User getByOpenId(String openId) {
        return userDao.getByOpenId(openId);
    }

    @Override
    public User getByOpenId(String openId, Integer communityId) {
        return userDao.getByOpenId(openId,communityId);
    }

    @Override
    public User getById(Integer id) {
        return userDao.getById(id);
    }

    @Override
    public void update(User user) {
        userDao.update(user);
    }
}
