package com.zeus.service;

import com.zeus.domain.entity.Community;
import com.zeus.domain.param.CommunityParams;
import com.zeus.domain.vo.CommunityDTO;

import java.util.List;

/**
 * @author yishan@dian.so
 * @version 1.0.0
 * @data 2019/3/2
 */
public interface CommunityService {

    CommunityDTO detail(Integer communityId);

    Boolean modify(CommunityParams communityParams);

    Community getById(Integer id);

    List<Community> listAll();
}
