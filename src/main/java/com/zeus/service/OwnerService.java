package com.zeus.service;

import com.alibaba.fastjson.JSONObject;
import com.zeus.domain.entity.House;
import com.zeus.domain.entity.Owner;
import com.zeus.domain.param.FeedbackHandleParams;
import com.zeus.domain.param.OwnerInfoEditParams;
import com.zeus.domain.param.OwnerInfoQueryParams;
import com.zeus.domain.vo.FeedbackDTO;
import com.zeus.domain.vo.SimpleDTO;
import java.util.List;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author yishan@dian.so
 * @version 1.0.0
 * @data 2019/3/3
 */
public interface OwnerService {

    List<String> listDistrict(Integer communityId);

    List<SimpleDTO> listBuilding(Integer communityId);

    List<SimpleDTO> listUnit(Integer communityId);

    JSONObject listOwnerInfo(OwnerInfoQueryParams ownerInfoQuery);

    String editOwnerInfo(OwnerInfoEditParams ownerInfoEditParams);

    Boolean importOwnerInfo(Integer communityId,String fileName, MultipartFile file);

    Boolean handleOwnerFeedback(FeedbackHandleParams feedbackHandleParams);

    JSONObject listFeedback(OwnerInfoQueryParams ownerInfoQuery);

    Owner getById(Integer id);

    Boolean addOwnerFeedback(FeedbackHandleParams feedbackHandleParams);

    House queryById(String houseId);

    Owner getByMobile(String mobile);

    List<Owner> listByMobile(String mobile);
}
