package com.zeus.service.impl;

import static org.junit.Assert.*;

import com.alibaba.fastjson.JSONObject;
import com.zeus.common.utils.RedisUtil;
import com.zeus.domain.param.UserParams;
import com.zeus.domain.param.UserQueryParams;
import com.zeus.service.UserService;
import javax.annotation.Resource;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author yishan@dian.so
 * @version 1.0.0
 * @data 2019/3/7
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class UserServiceImplTest {

    @Resource
    private UserService userService;

    @Resource
    private RedisUtil redisUtil;

    @Test
    public void delete() throws Exception {
        userService.delete(3);
    }

    @Test
    public void add() throws Exception {
        UserParams userParams = new UserParams();
        userParams.setDuty(1);
        userParams.setMobile("18842689096");
        userParams.setName("test");
        userParams.setRoom("1");
        userParams.setCommunityId(2);
        System.out.println(userService.add(userParams));
    }

    @Test
    public void listUser() throws Exception {
        UserQueryParams userQueryParams = new UserQueryParams();
        userQueryParams.setCommunityId(1);
        userQueryParams.setOffset(0);
        userQueryParams.setPageSize(10);
        JSONObject jsonObject = userService.listUser(userQueryParams);
        System.out.println(jsonObject.toString());
    }

    @Test
    public void login() throws Exception {
        redisUtil.set("a",123);
        System.out.println(redisUtil.get("a"));
        redisUtil.del("a");
        System.out.println(redisUtil.get("a"));
    }

}