package com.zeus.service.impl;


import com.alibaba.fastjson.JSONObject;
import com.zeus.domain.entity.House;
import com.zeus.domain.mapper.HouseMapper;
import com.zeus.domain.mapper.OwnerMapper;
import com.zeus.domain.mapper.UserMapper;
import com.zeus.domain.param.FeedbackHandleParams;
import com.zeus.domain.param.OwnerInfoEditParams;
import com.zeus.domain.param.OwnerInfoQueryParams;
import com.zeus.domain.vo.SimpleDTO;
import com.zeus.service.OwnerService;
import java.util.Arrays;
import java.util.List;
import javax.annotation.Resource;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author yishan@dian.so
 * @version 1.0.0
 * @data 2019/3/7
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class OwnerServiceImplTest {

    @Resource
    private OwnerService ownerServiceImpl;

    @Resource
    private HouseMapper houseMapper;

    @Test
    public void listDistrict() throws Exception {
        List<String> list = ownerServiceImpl.listDistrict(1);
        System.out.println(list.toString());
    }

    @Test
    public void listBuilding() throws Exception {
        List<SimpleDTO> list = ownerServiceImpl.listBuilding(1);
        System.out.println(list.toString());
    }

    @Test
    public void listUnit() throws Exception {
        List<SimpleDTO> list = ownerServiceImpl.listUnit(1);
        System.out.println(list.toString());
    }

    @Test
    public void listOwnerInfo() throws Exception {
        OwnerInfoQueryParams ownerInfoQueryParams = new OwnerInfoQueryParams();
        ownerInfoQueryParams.setOffset(0);
        ownerInfoQueryParams.setPageSize(10);
        ownerInfoQueryParams.setCommunityId(1);
        JSONObject jsonObject = ownerServiceImpl.listOwnerInfo(ownerInfoQueryParams);
        System.out.println(jsonObject.toString());
    }

    @Test
    public void editOwnerInfo() throws Exception {
        OwnerInfoEditParams ownerInfoEditParams = new OwnerInfoEditParams();
        ownerInfoEditParams.setHouseId("1");
        ownerInfoEditParams.setNames("测试3,测试4，测试5");
        ownerInfoEditParams.setMobiles("13903678765,13903678766,123");
        ownerServiceImpl.editOwnerInfo(ownerInfoEditParams);
    }

    @Test
    public void importOwnerInfo() throws Exception {
        //ownerServiceImpl.importOwnerInfo(1,null);
    }

    @Resource
    private OwnerMapper ownerMapper;

    @Resource
    private UserMapper userMapper;
    @Test
    public void exsitMobiles() throws Exception {
        Integer count = userMapper.getByMobile("18058750520",2);
        System.out.println(count);
    }

    @Test
    public void handleOwnerFeedback() throws Exception {
        FeedbackHandleParams feedbackHandleParams = new FeedbackHandleParams();
        feedbackHandleParams.setFeedbackId(1);
        feedbackHandleParams.setStatus(1);
        ownerServiceImpl.handleOwnerFeedback(feedbackHandleParams);
    }

    @Test
    public void listFeedback() throws Exception {
        OwnerInfoQueryParams ownerInfoQueryParams = new OwnerInfoQueryParams();
        ownerInfoQueryParams.setOffset(0);
        ownerInfoQueryParams.setPageSize(10);
        ownerInfoQueryParams.setCommunityId(1);
        JSONObject jsonObject = ownerServiceImpl.listFeedback(ownerInfoQueryParams);
        System.out.println(jsonObject.toString());
    }

    @Test
    public void getById() throws Exception {
        System.out.println(ownerServiceImpl.getById(1));
    }

    @Test
    public void addOwnerFeedback() throws Exception {

        FeedbackHandleParams feedbackHandleParams = new FeedbackHandleParams();
        feedbackHandleParams.setQuestion("一闪测试");
        feedbackHandleParams.setMobile("15658150080");
        feedbackHandleParams.setFeedbackOwnerName("aaa");
        ownerServiceImpl.addOwnerFeedback(feedbackHandleParams);
    }
    @Test
    public void test11() {
        House house = ownerServiceImpl.queryById("10");
        System.out.println(house);
    }

    @Test
    public void test12() {
        List<House> house = houseMapper.listByNamesAndMobilesLike("周锐","13429136268");
        System.out.println(house.get(0));
    }
}