package com.zeus.service.impl;

import static org.junit.Assert.*;

import com.zeus.domain.entity.Community;
import com.zeus.domain.param.CommunityParams;
import com.zeus.domain.vo.CommunityDTO;
import com.zeus.service.CommunityService;
import java.util.Date;
import javax.annotation.Resource;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author yishan@dian.so
 * @version 1.0.0
 * @data 2019/3/2
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class CommunityServiceImplTest {

    @Resource
    private CommunityService communityService;

    @Test
    public void detail() throws Exception {
        CommunityDTO detail = communityService.detail(1);
        System.out.println(detail.toString());

    }

    @Test
    public void modify() throws Exception {
        CommunityParams communityParams = new CommunityParams();
        communityParams.setCommunityId(1);
        communityParams.setBuildingArea("5000");
        communityParams.setHouseholdNum(522);
        communityParams.setSignature("年少有作为");
        communityParams.setVoteType(2);
        communityService.modify(communityParams);
    }

}